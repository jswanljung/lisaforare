+++
avsandare = ""
bilder = []
date = "2019-09-03T00:00:00+02:00"
inlaggsbild = ""
title = "Releasefest för Varken på Herrängens gård"

+++
<figure> <img src="/bilder/69957972_10158746311877678_4716693463393894400_n.jpg" alt="release" width="494"> <figcaption>Herrängens gård är perfekta partystället!  </figcaption> </figure>

Det finns bara ett sätt att beskriva min releasefest: STORSLAGEN! Det krävdes tydligen en debutroman för att jag skulle ställa till med en egofest. Till och med på mina två bröllop har jag lagat maten själv. Jag betalade bara en del själv på releasefesten  – både [LB förlag](http://lbforlag.se) och Herrängens gård var storslaget generösa. TACK!<!--more--> 

Cecilia och Sofie som rattar [Herrängens gård ](http://www.herrangensgard.se)sen årsskiftet har ryckt upp verksamheten – gå dit och luncha eller fika och upplev det själv! Risken är stor att du snubblar över mig, för jag sitter ofta där och arbetar. Luncherna är gedigna med finess och lagade med speciell kärlek till grönsaker i säsong.

Releasegästerna bjöds på buffé med dessert och vin. Petter och Claes spelade kusliga låtar till fördrinken. Jens Heimdahl ordnade en skräckinjagande installation av monsterlarver. Vad vi åt? Jag har lovat och svurit att det här inte ska bli en matblogg, men hur ska jag kunna låta bli?

Alla rätter som den fantastiske kocken Jimmy Johannesson lagade hade anknytning till Varken och de dofter och smaker som gömmer sig innanför pärmarna. Rosor, honung, lavendel, rödbetor, citronverbena och förstås – den gäckande frangipani. Jimmy har ett förflutet från bland annat Atmosfär i Malmö och är en hejare på att locka fram råvarornas själ. Dessutom har han den goda smaken att verkligen gilla skräck och oknytt. Vi hade vansinnigt kul när vi spånade fram menyn.

Jag fixade två sorters hummus. En med kikärtor och fikon som nämns i romanen, kikärtor är dessutom Saras stapelföda. Och en hummus på gula ärtor med kapris, libbsticka, mjukfräst salladslök och libbsticka. Dessutom gjorde jag min klassiska finska svampsallad med syrad gurka till buffén. Det här dukade Jimmy upp:

Saltbakad samt fryst rödbeta med lättfermenterade svarta vinbär och chokladmynta

Bakad purjolök med färskpotatis, brynt smör, krispig purjo, gräddfil och färsk stenbitsrom

Lätt torkad vildtomat med rosenpicklad fänkål och rökt mandel

Lammlägg med sotad endive, karamelliserad samt friterad jordärtsskocka och gräddad sky

Getost från Löt gårdsmejeri med honung från Laggarbacken, lavendel och rostad brioche

Torkad pannkaka med frangipane, citronverbena, rostad vit choklad och hallon

PS. Bilderna i det här inlägget är snodda här och där, de har tagits av mina gäster.  
  
<figure> <img src="/bilder/70223464_10158746311872678_7624329148181250048_n.jpg" alt="release" width="494"> <figcaption>Jag läser ur Varken.  </figcaption> </figure>

<figure> <img src="/bilder/69545719_10158746314692678_7601089127387234304_n.jpg" alt="release" width="494"> <figcaption>Cecilia och Sofie är stjärnor!</figcaption> </figure>

<figure> <img src="/bilder/69615690_10158746314707678_5287330265813745664_n.jpg" alt="release" width="494"> <figcaption>Monster in spe!</figcaption> </figure>