+++
avsandare = ""
bilder = []
date = "2019-01-16T00:00:00+01:00"
inlaggsbild = ""
title = "Bakom texten: Direktsändning"

+++
På skrivkursen jag gick för [Anders Fager](https://sv.wikipedia.org/wiki/Anders_Fager "Bra lärare") för några år sen ingick skrivövningar att plita på hemma. Mininovellen [Direktsändning](https://lisaforare.se/langsjon/direktsändning/ "Läs!") var resultatet av en av hemläxorna. Målet? Att skriva dialog med så mycket repliker och lite mellantext som möjligt. Anders Fager är av skolan som anser att så kallade ”regianvisningar” i möjligaste mån bör undvikas i text. <!--more--> Replikerna ska vara så tajta att de räcker. (Jag håller inte riktigt med, men det resonemanget ska jag utveckla en annan gång.)

Eftersom min hjärna går igång på begränsningar och svårigheter bestämde jag mig för att skriva en berättelse med enbart repliker. Valet av miljö var självklart: en lokalradiostation. I den miljön är det naturligt att fokusera på repliker och förklaringar kan läggas in i pratet utan att det känns krystat. Jag gillar verkligen att vara med i radio eftersom man kan vara nästan säker på att man inte stjäl lyssnarnas tid, utan snarare berikar den. Till och med att stryka blir uthärdligt när man lyssnar på radio, brukar jag belåtet tänka när jag radiopratar. Ett strykjärn! Så klart att ett strykjärn måste få vara med!

Nu hade jag tre komponenter klara: ett lokalradioprogram, ett strykjärn och skräck. Sen gällde det att hitta några distinkta röster som enkelt kunde särskiljas. En försagd författare, en forcerad radiopratare och en grabbig alltiallo. Det var bara att slänga in dem i studion, sen skrev de sig själva. Ibland var jag tvungen att stoppa in namn för att läsaren ska förstå vem som står för nästa replik. I en radiomiljö känns det greppet mer naturligt än i andra sammanhang.

Humor i skräck är svårt eftersom det skapar en distans, jag tycker att det funkar här eftersom huvudsyftet inte är att skrämmas utan att underhålla en smula. I en lång novell skulle det parodiska kännas tröttsamt. I mininoveller är det bara att vräka på.

Den alienliknande illustrationen av just ett strykjärn är gjord av min vän Fredric Dahlgren.

**LISAS SKRIVTIPS!** Du kan själv experimentera med att skriva en novell som enbart består av dialog. En superb träning!