+++
avsandare = ""
bilder = []
date = "2019-08-22T10:00:00+02:00"
inlaggsbild = "/68739587_10158706308707678_2578491716461920256_n.jpg"
title = "Välkommen till världen, Varken!"

+++
<figure> <img src="/bilder/68739587_10158706308707678_2578491716461920256_n.jpg" alt="BOKFAN" width="494"> <figcaption> Nyfödda! </figcaption> </figure>  
Alla är vi barn i början och det har känts lite fint att vara barn på nytt de senaste åren. Nybörjare. Ettagluttare. Fantastikförfattare in spe. Det var så länge sen jag var ung och lovande.  Jag önskar att jag visste hälften av det jag vet idag när jag började skriva Varken. Och jag hoppas att jag vet dubbelt så mycket som idag när jag är färdig med del två.<!--more--> Här är några surt förvärvade kunskaper:

[Det tar tid.](https://lisaforare.se/post/skrivkramp-del-2/) Oändliga omskrivningar. Oräkneliga turer av och an med redaktör. För att inte tala om allt prokrastinerande. Jag trodde att jag visste hur det var eftersom jag faktiskt gett ut en rad kokböcker. Jag trodde fel. Det tog ännu mer tid. Se till att du kan ta den utan att det drabbar dina nära och kära allt för hårt.

Låt pålitliga personer titta på texten tidigt så att du inte är ute och cyklar. För mig är det lättare att lämna från mig en text jag vet är halvkass och halvfärdig än en jag suttit och finslipat i evigheter. <br>Vissa håller inte alls med om det här. Men för mig var det väldigt nyttigt och tidsbesparande att i ett tidigt skede höra den där förnedrande läsarkommentaren: "Jag fattar ingenting, vad håller du på med?"

Lär dig ta kritik! Lätt att säga … Kanske kan man snarare uttrycka det: lär dig hur du reagerar på andras synpunkter, i synnerhet de negativa. Jag blir till exempel ofta rosenrasande och obstinat i tre till femton minuter, sen lugnar jag ner mig och landar i att kritikern har ganska rätt. Till slut blir jag milt förälskad i människan som läst så uppmärksamt och tagit sig tid att formulera sina tankar. Därför har jag lärt mig att be om alla synpunkter skriftligt, så att jag kan rasa för mig själv den där första kvarten. <br>Beröm? Det älskar jag i en kvart, sen tror jag sällan på det.

Hejdå, boken! Du har varit ett fint men krävande sällskap flera år. Många vaknätter har det blivit. Jag har tuktat dig, svurit över dig, slitit mitt hår över dina förbryllande tilltag och mina egna fadäser – men till slut har vi försonats. Det är mycket jag i efterhand tänker borde gjorts annorlunda, men så  är det alltid. <br>Nu får du stå på egna ben. Du är trind, knasig, pladdrig och yvig – precis som din mamma. Gudskelov betydligt snyggare och fräschare. <br>Nu måste jag berätta något jobbigt. Alla du möter kommer inte gilla dig, en hel del kommer rent av att avsky dig. Det tar vi med ro du och jag. För jag är rätt säker på att du får några vänner också. Nu måste jag syssla med annat. För om allt går enligt planerna ser din lillasyster dagens ljus om ett år.  
  
[HÄR KAN DU LÄSA FÖRSTA KAPITLET AV VARKEN.](https://lisaforare.se/langsjon/varken-sten/ "SMAKPROV!")

28:de augusti finns Varken att köpa i bokhandeln eller direkt av mig. Jag kommer dessutom att ha försäljning och signering på supertrevliga [Herrängens gård](http://www.herrangensgard.se) rätt många helger.

[Köp på BOKUS!](https://www.bokus.com/bok/9789177990864/varken/)

[Köp på ADLIBRIS!](https://www.adlibris.com/se/bok/varken-9789177990864)