+++
avsandare = ""
bilder = []
date = "2019-02-28T00:00:00+01:00"
inlaggsbild = "/yQbnmGXw.png"
title = "Skrivkramp, intro: Ett överdiagnostiserat tillstånd?"

+++
<figure> <img src="/bilder/yQbnmGXw.png" alt="Mörk musa" width="494"> <figcaption>Skrivkrampens demon, aka Qwerty.
Ill: Cristopher Anderton</figcaption> </figure>

Hur mycket påverkar vårt språk våra tankar? Styr grammatiken och ordförrådet hur vi uppfattar olika fenomen. Det är en fråga som sysselsatt filosofer som [Noam Chomsky](https://en.wikipedia.org/wiki/Noam_Chomsky) och författare som [George Orwell](https://en.wikipedia.org/wiki/Newspeak "Newspeak"). Och just nu sysselsätter frågeställningen den här lallande amatören; för jag undrar verkligen om själva termen skrivkramp  gör skrivandet svårare än det är. <!--more-->

Skrivkramp är alltså den legendomsusade åkomma som anses drabba författare, främst skönlitterära, med förfärande regelbundenhet. Texten trilskas, hjärnan kör fast – de satans bokstäverna dansar inte från fingertopparna. Det ifrågasätts sällan att fenomenet existerar och det finns en underförstådd överenskommelse att den har likartade orsaker och botemedel. Ofta beskrivs skrivkrampen som existensiell och mystisk. Kanske rent av lite nobel? Ordet skrivkramp har  ju faktiskt en inbyggd försåtlig klang av status. Det antyder att det naturliga tillståndet för den drabbade är ett flödande skrivande. Och det enda som hejdar det flödet är en onaturlig låsning, som man inte riktigt kan rå för. Det hela är ännu tydligare på engelska: _writers block_. "Författare har skrivkramp, om jag har skrivkramp är jag författare."

För dig funkar ordet kanske på ett helt annat sätt än för mig. Det står dig naturligtvis fritt att kalla dina svårigheter för skrivkramp om det får dig att känna dig bättre. Min poäng är dock: just det faktum att du känner dig bättre av begreppet kan leda till att motivationen och möjligheten att ta dig ur tillståndet minskar. Gör som du vill. Allt jag vill göra är att bjuda på mina knep för att börja skriva, fortsätta skriva och skriva klart. Kanske passar de dig, kanske inte.

Mitt första råd är alltså: sluta överdiagniostisera. Att prestationsförmågan varierar är helt normalt. Du beklagar dig sällan över matkramp när du inte lyckas åstadkomma en smarrig middag på tre dagar – eller ens tre veckor. Jobbar du med statistik och får räknekramp? Knappast. Om det ser ut som slaget vid Lützen i hemmet är städkramp en ovanlig ursäkt. Du har ju bara varit stressad, oinspirerad, trött, ängslig eller ärligt talat lite lat.

Föreställ dig att du är förstoppad i ordets konventionella mening. Det finns många huskurer som lättar på trycket för många, men hittar du orsakerna kan du förfina behandlingen och förebygga. Mental förstoppning är samma sak. Om din blanka hjärna – och dito datorskärm – beror på allmän livsstress, behövs lite andra åtgärder än om den beror på idétorka eller dåligt självförtroende. Visserligen är det oftast olika faktorer som samverkar i en ohelig allians, men vad sägs om att bena ut vilka som dominerar? Och sedan avdramatisera lite och hitta lösningar.

I en serie blogginlägg kommer jag att fortsätta berätta hur jag ser på mina problem att få texter klara och vad jag gör åt dem. Och tro mig: mina problem är monumentala!

1. LIVET: Har du så många måsten att du inte ens kommer till datorn? Sitter du och tänker på andra saker du borde göra? Är samvetet en enda stor böld? Planera innan du påbörjar ett stort bokprojekt!
2. MOTIVATION: Varför vill du skriva en roman? Har du orealistiska mål.  Önskar du att någon annan skrev din historia? Hur kan du sporra dig själv?
3. STARTMOTOR Har du ofta en hög tröskel för att sätta igång med olika uppgifter eller är det bara skrivandet? Är stora projekt svårare att påbörja än små?
4. FLÖDE Hur hittar man hyperfokus och hur stannar man där? Finns det någon del av arbetet som alltid går plättlätt?  Har du svårt att hitta tillbaka in i arbetet när du blir störd?  Får du myror i benen så att det är jobbigt att sitta stilla länge?
5. SLUTSPURT Tröttnar du halvvägs? Har du kört fast? Har du lättare att börja med nya projekt än att avsluta gamla.
6. SJÄLVFÖRTROENDE Är sannolikheten hög att ingen någonstans någonsin har skrivit en sämre roman? Ligger du vaken om nätterna och undrar vad andra ska tycka om dina ord? Vågar du inte låta andra läsa?

   ILLUSTRATION: [CRISTOPHER ANDERTON](https://www.instagram.com/chanderton/ "Geni!")