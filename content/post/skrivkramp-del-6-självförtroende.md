+++
avsandare = ""
bilder = []
date = "2019-05-30T00:00:00+02:00"
inlaggsbild = "/61831959_10158448406987678_1269776079988457472_n.jpg"
title = "Skrivkramp, del 6: SJÄLVFÖRTROENDE"

+++
<figure> <img src="/bilder/61831959_10158448406987678_1269776079988457472_n.jpg
" alt="Mörk musa" width="494"> <figcaption> Du är sämst i världen. Jag med. Ill: Cristopher Anderton</figcaption> </figure>

Varje gång telefonen ringer är jag övertygad om att det är någon som ringer för att ösa misshag över mig, berätta att jag gjort något fel eller bara kallt konstatera att jag är en loser. <!--more-->

[Imposter syndrome](https://www.fastcompany.com/40421352/the-five-types-of-impostor-syndrome-and-how-to-beat-them "Du är en bluff! Jag med!") innebär att man alltid känner sig som en bluff och inte förtjänar sin position. Det är helt normalt att känna så ibland. och det är helt okej. Värre är förstås om känslorna lägger fälleben i ens strävanden. Dags att servera ett av mina viktigaste livsmotton:

**Man ska inte bry sig så mycket om känslor, för det kommer nya hela tiden.**

Kortvariga undergångskänslor är inte farliga och man kan inte rå för dem, det är bara att vänta ut dem medan man resonerar lite med sig själv. Så här brukar jag tänka: "Kanske blir det här en askass bok som allmänheten skrattar åt ett tag. Men det kommer ändå inte hamna på topp-tjugo-listan med pissiga saker som hänt i mitt liv: fyra missfall, skilsmässa, egna och andras svek, kronisk sjukdom, vänner som dött. Jag bröt ihop, [limmade ihop skärvorna](https://makezine.com/2015/08/17/kintsugi-japanese-art-recognizing-beauty-broken-things/ "How beautiful it is and how easily it can be broken.") och gick vidare. Barnen är friska, vi kan äta oss mätta och har tak över huvudet. Min man sa senast i morse att han älskade mig. "

Farliga är känslorna först när man inte kan ta sig ur dem utan fastnar i självömkans träsk. Tycker du att jag raljerar? Så klart jag gör! Raljerande är ett effektivt motgift, man måste kunna skratta åt sig själv.

Paranoia är på sätt och vis ett extremfall av självupptagenhet. Att tro att man är universums centrum och att människor har tid och ork att hata en. [Här är ett ljuvligt tragikomiskt poem om just det.](https://poets.org/poem/we-who-are-your-closest-friends "We who are your closest friends …") De flesta andra är naturligtvis fullt upptagna med saker som är intressantare för dem – exempelvis att hata sig själva. Kanske utan anledning, kanske för att de gjort betydligt värre saker än skrivit en medioker bok. Du är inte misslyckad för att du inte skriver lika fantastiskt som [Kelly Link](https://kellylink.net "Husgudinna!") eller Kazuo Ishiguro. Inte ens de är nöjda för jämnan. Och de har också varit nybörjare.

Om bristande självförtroende förhindrar dig från att skriva vill du nog inte skriva egentligen, du vill snarare bli publicerad. Erkänd. Älskad. (Inget fel med det, men skrivande är ett ovanligt tidsödande sätt att uppnå ditt mål.) Själva skrivandet kan du dock ägna dig åt utan att en enda annan person någonsin läser. Din uselhet kan vara och förbli en hemlighet precis så länge du vill. Om du är paniskt rädd för att bli förknippad med ditt verk så kan du prova vingarna genom att lägga ut texter på en anonym blogg.

Är du din egen strängaste kritiker? Då kan du helt enkelt utveckla ett tjockare skinn genom att skriva så ofta och så mycket du kan. Ju mer du skriver, desto mindre dramatiskt är det. Varför inte ta i från tårna? Börja med att skriva en novell så hiskeligt uselt du kan – _med flit_. Gödsla med orealistiska karaktärer, yxig dialog och smeta på så många klyschor du kan. Hurra, du är ju faktiskt snudd på världsbäst på att skriva dåligt!  
Gotta dig i din uselhet ett bra tag.  
Rulla runt i gyttjan.  
Grymta.  
Grattis, du överlevde! Och nästa sak du skriver kommer att vara bättre. Värre kan det ju inte gärna bli …

Dåligt självförtroende är för övrigt betydligt mindre skadligt för skrivandet än hybris. I bästa fall grundar det sig ju i en sund respekt för läsaren – att göra anspråk på andra människors tid och uppmärksamhet är ju i ärlighetens en smula förmätet. Min största skräck är just att tråka ut läsare och stjäla deras tid. Det finns ju så mycket roligt, fint och viktigt i världen att ägna sig åt! Sen tänker jag på alla böcker jag läst, och inser att jag inte hyser något som helst agg mot författare som gjort sitt bästa utan att vara läsvärda för just mig – det är smällar man får ta som läsare. _Hit and miss._ Och det är till viss tröst att jag faktiskt läst fler pinsamt dåliga böcker än jag skrivit.

Än så länge.

ILLUSTRATION: [CRISTOPHER ANDERTON](https://www.instagram.com/chanderton/ "Geni!")