+++
avsandare = ""
bilder = []
date = "2019-01-17T23:00:00+00:00"
inlaggsbild = "/41678596_10157715724327678_7752333888160006144_n.jpg"
title = "Antagen!"

+++
Var börjar man en historia? Nästan var som helst, beroende på vem man berättar för. Jag väljer mitten. Närmare bestämt den 10:de augusti 2018. Allt före och efter kommer jag att berätta så småningom här i bloggen – lite pö om pö. Nå, den där kvalmiga torsdagen fick jag ett mejl:<!--more-->

> Hej Lisa!
>
> Hoppas du har haft, och fortfarande har, en underbar sommar!
>
> Jag har läst ditt manus som jag verkligen måste säga har bra driv och intressanta karaktärer. Du har helt enkelt skapat en spännande värld! Men precis som du själv nämner så finns det en hel del att förbättra, stryka och skriva om. Jag skulle gärna vilja träffa dig och prata igenom vad det är du vill göra med den här boken och vad jag tror skulle vara möjligt. Manuset har stor potential, men det är en hel del arbete som återstår.
>
> Hur ser det ut för dig på måndag den 20 augusti – har du möjlighet att ses då?
>
> Många hälsningar,
>
> Louise Bäckelin

Mitt manus hade jag skickat in till [LB Förlag](http://lbforlag.se "Mitt förlag!") fem veckor tidigare, precis innan hela Sverige gick på semester. Manuset var varken välslipat eller med ett tydligt slut. Men jag hoppades ändå att det var så pass bra att ett förlag skulle se just potentialen och vilja inleda ett samarbete för att få fram en riktigt bra bok. Eftersom det är min debutroman hade jag inte inbillat mig att mitt manus skulle svischa iväg till tryck med några kommatecken flyttade. Tvärtom var jag på jakt efter en riktigt rutinerad och klåfingrig redaktör. Det fick jag så småningom. Men det var den här dagen jag försiktigt började kalla mig författare. 

Med en så tjusig titel i fickan anser jag mig ha mandat att tala om för alla andra HUR MAN GÖR. Jag har ju trots allt levererat recept i tjugofem år – små texter där jag pekar med hela handen och beordrar: Skala! Hacka! Fräs! Servera!

Den här bloggen ångar på i samma anda och handlar om saker som jag kommit underfund med under arbetet på romanen. Det blir inga benhårda regler, precis som när det gäller att tillaga en kyckling finns det en massa olika metoder att skriva. Det vore vansinnigt tråkigt om alla lagade exakt samma kycklingrätt. Allt jag kan servera är det som just jag gillar.

På menyn står anrättningar som:

* Döda darlingar: En ofta onödig sysselsättning
* Om hur lättja och dåligt lokalsinne kan vändas till en fördel
* De orättvist hånade adverben
* Skrivkramp: Ett överdiagnostiserat tillstånd
* Skrivkurser (andras och mina egna)
* Intimt perspektiv (nej, det handlar inte om att sitta på huk över en spegel, det här är en annan sorts blogg)

Ständigt och stundom levereras uppdateringar om vad som händer med boken och olika event. Ni kan också förvänta er skryt om verkliga och inbillade framgångar.

![](/bilder/41678596_10157715724327678_7752333888160006144_n.jpg)