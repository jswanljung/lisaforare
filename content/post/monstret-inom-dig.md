+++
avsandare = ""
bilder = []
date = "2019-01-31T22:00:00+01:00"
inlaggsbild = "/52965269_10158189579867678_6886498928309567488_n-1.jpg"
title = "Monstret inom dig"

+++
<figure> <img src="/bilder/IMG_1909 2.jpg" alt="monster" width="494"> <figcaption>Illustration: Titti Winbladh</figcaption> </figure>

Du kanske inte visste det, men området kring Långsjön är befolkat av en hel del monster. Några av dem fick flytta in i min roman Varken. Monster har jag älskat sen barnsben, så det var bara en tidsfråga till det att jag började skriva om dem och skapa mitt eget bestiarium. Hur skriver man fram ett monster? <!--more-->

Ett säkert kort är att bara låta monstret anas. Biotittarnas fantasier har alltid klått gummidräkter och aldrig så lyckade datoranimeringar. Det suddiga som rör sig snabbt i skuggorna är nästan alltid det läbbigaste. Men räkna med att många läsare och tittare blir purkna om de inte får the full monty.

Ett smart drag när du väljer att låta läsarna bekanta sig mer intimt med ditt monster är att ge det en agenda. "Äta små möra barn" är visserligen klatschigt credo och ett säkert kort – men förtjänar inte dina läsare ett mer komplext monster? I och för sig är en del monster mest själlösa redskap för mer utstuderade och smarta styggingar, men monster kan vara så mycket mer. Ser de sig ens själva som monster. Kanske inte mer än en ordinär mamma med kort stubin gör …

Gastar, mylingar, tentakelgrisar, vampyrer et cetera behöver faktiskt inte bete sig helt rationellt och begripligt (det gör faktiskt inte människor heller). Däremot måste de ha hyfsat begripliga drivkrafter för att vara engagerande. Läsaren behöver förstås inte veta särskilt mycket om själslivet hos den namnlösa fasan i skogen eller barnätande blobben under badkaret. Men författaren bör ha koll på sina kritter. Och när man känner sitt monster på djupet blir det lättare att hitta på vad det tar sig för. Vi vet ju att rädsla gör människor farliga. Ett monster med rädsla kan paradoxalt nog vara skrämmande.

De monster som berör mig mest är en kombination av mänskliga och omänskliga, både möjliga att förstå och gåtfulla. Frankensteins monster är förstås ett sådant mycket mänskligt monster. Zombies är i regel trista eftersom de sällan har någon övergripande strategi, de raglar mest runt och tuggar på folk lite planlöst och lämnar en del att önska vad gäller personlig hygien. Vampyrer lider av en viss brist på självdistans och är rätt uttjatade. Dessutom är det omöjligt att göra det bättre än mästerverket Låt den rätte komma in.

Påfallande och pinsamt ofta förälskar sig författaren huvudlöst i sina monster, just alla överromantiska och sackarinsöta vampyrskildringar är paradexempel på detta. Personligen tycker jag sällan om de böcker där seriemördare och sadistiska vampyrer är de enda karaktärerna författarna verkar intressera sig för, vilket märks på att de försetts med tröttsamma egenskaper som onaturlig intelligens, perfekt gehör, fotografiskt minne, toppkockstalanger i köket et cetera. Den multikompetenta renässansmördaren som häller upp chianti i välpolerade kristallglas till tonerna av Vivaldi har blivit en klyscha. Det var skojigt att bekanta sig med Hannibal Lecter och Lestat en gång i tiden, deras arméer av kloner får mig att gäspa. Jag rekommenderar att du har en hälsosam ambivalens till samtliga karaktärer du skriver.

Det säkraste sättet att bli nöjd är att skräddarsy sina egna monster. Vrid och vräng på gamla myter eller uppfinn dina kritter från scratch. Mina kritter är av båda sorterna. Och jag älskar dem alla lite lagom.

NÅGRA BOKMONSTER  
[Lamia, Tristan Travis](https://www.goodreads.com/book/show/6635346-lamia) (1984) En ormdemon som gör slarvsylta av våldtäktsmän. Underskattad bok med ett trevligt monster som faktiskt inspirerade till en sidoplått i min roman. Extraplus för att en könssjukdom som luktar [gefilte fish ](https://en.wikipedia.org/wiki/Gefilte_fish "Tveksamt kulinariskt värde")har en bärande roll i handlingen.

Alla monster [Kelly Link ](http://kellylink.net)skrivit om. Av det enkla skälet att hon alltid skapar magi. (Ja, jag är också distanslös ibland.)

[Svenska kulter av Anders Fager](https://sv.wikipedia.org/wiki/Svenska_kulter) (2010) Genom att placera snillrikt modifierade Lovecraftska monster mitt ibland oss uppstår slemmig poesi. Vad gör de? Hur mår de? Vad eller vem äter de? Vad eller vem knullar de?

[The snake, John Godey](https://www.goodreads.com/book/show/410636.The_Snake) (1978) Älskar en grej med den här ganska mediokra men fartiga boken: idén att låta vissa scener skildras mer eller mindre skickligt ur en svart mambas perspektiv.

[Carmilla, Le Fanu](https://en.wikipedia.org/wiki/Carmilla) (1872) Varför inte ge dig på en gammal goding? Dessutom så kort att du klämmer den på någon timme. Klassisk utan att vara mossig och stor inspirationskälla till Bram Stokers Dracula. Fortfarande en av de sexigaste vampyrskildringarna.

NÅGRA FILMFAVORITER  
På film är det inte lika viktigt med att lära känna monstrets själ, stämning och engagemang skapas på andra sätt.

The thing (1982) var en tidig kärlek och [den magnifika slutscenen ](https://www.youtube.com/watch?v=GA4Ozqt7338)har jag sett oändligt många gånger. Paranoia och smygande skräck i pittoresk arktisk miljö.

[Alien ](https://www.youtube.com/watch?v=LjLamj-b0I8)(1979) Inte så originellt att hylla en klassiker, men det går inte att komma undan henne. Bokstavligt talat. Klaustrofobisk och banbrytande.

[Train to Busan](https://www.youtube.com/watch?v=pyWuHv2-Abk) (2016) Zombier som är riktigt uthärdliga tack vare några fräscha grepp och magisk cinematografi. Rasande tempo och rajraj deluxe.

[Medusa.](https://www.youtube.com/watch?v=_htpiyRkooY) En orm är en för många. Ett knippe ormar på någons huvud är fanimig oslagbart. En film med en Medusa är lite som lasagne: även när den är dålig är den ganska bra. History Channel har gjort [en bra dokumentär](https://www.youtube.com/watch?v=9wePgwCojNI) om den tragiska bakgrunden. Jag hejar alltid på Medusa.

**LISAS SKRIVTIPS!** Skriv om en berömd skräckscen från en film ur monstrets perspektiv. Välj en grad av medvetenhet, intelligens och intellektuell reflektion du tycker passar. Använda första person (jagform) eller för all del plural (viform) om du skriver om Sharknado och vill ta i från tårna. Är The Thing superskrajset under den berömda [blodtestscenen](https://www.youtube.com/watch?v=m6shktwDwzM)? Hitta detaljer som ger närvaro. Alien kanske har fått rymdloppor? Ha kul! Eller skräm skiten ur dig själv.

_Det blir mer om monster på bloggen. Mycket mer. Det här var en ytlig exposé, jag nördar ner mig mer senare och presenterar fler favoritmonster – och mina egna!_