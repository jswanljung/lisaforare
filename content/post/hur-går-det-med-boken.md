+++
avsandare = ""
bilder = []
date = ""
draft = true
inlaggsbild = ""
title = "Hur går det med boken?"

+++
<figure> <img src="/bilder/P4039418.jpg" alt="monster" width="494"> <figcaption>Fotograf: Severus Tenenbaum</figcaption> </figure>

Tackar som frågar! Jag tror det går okej. Just nu trycks boken. Och vi har ett utgivningsdatum: 28 augusti 2019. En hejdundrande releasefest planeras som bäst.

Min gamle vän Niklas Lindblad på [Mystical Garden Design](https://www.mgd.nu "Bra skit!") har gjort det fina omslaget. Och romanfan går att bevaka på [Adlibris](https://www.adlibris.com/se/bok/varken-9789177990864 "Adlibris"). På Bokus finns dessutom möjlighet att förbeställa en signerad upplaga.

Det hela känns lite mysko, förstås. Att ge ut en roman var inte min dröm. Jag kommer nog aldrig att se mig själv som författare. I synnerhet inte med stort F. Plötsligt snubblade jag på en historia värd att berätta. Det är ju inte direkt så att jag siktar på Augustpris, men jag hoppas verkligen att mina läsare har i alla fall hälften så roligt när de läser boken som jag hade när jag skrev den. Det är min första skönlitterära bok och jag lärde mig liksom skriva på nytt under arbetets gång. Nästa bok blir mycket bättre, fast det säger man ju alltid. Den är faktiskt redan på god väg att ta form och om gud vill och hängslena håller kommer tvåan ett år efter debuten.

Jag har alltså bestämt mig för att ha en riktig releasefest. Runt åttio personer bjuds på vin, buffé, dessert och kaffe på löjligt pittoreska [Herrängens gård](http://www.herrangensgard.se "Kanonställe!"). Jag har aldrig haft en fin fest för mig själv där jag inte lagat maten själv. Inte ens på mina två bröllopsfester var jag köksledig. När jag fyllde femtio var jag för sjuk för att ha fest alls. Fyrtioårsdagen? Minns jag inte. Så visst var det på tiden? Dessutom måste jag ha ett extra mingel, för eftersom man måste bjuda en massa branschfolk etc har inte alla vänner jag vill bjuda fått plats.

De nya ägarna på Herrängens gård, Cecilia och Sofie, är råproffsiga. Kocken Jimmy är en klippa och har särskilt fin hand med grönsaker. Och de gav mig ett superfint erbjudande eftersom vi ska samarbeta på olika sätt. Jag kommer så småningom att skriva ett blogginlägg om maten i min bok och andra böcker. Samt lägga ut några recept, förstås.