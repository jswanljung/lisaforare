+++
avsandare = ""
bilder = []
date = "2019-04-05T00:00:00+02:00"
inlaggsbild = "/53028713_312272079492058_100357494328524800_n.png"
title = "Skrivkramp, del 3: Startmotor"

+++
<figure> <img src="/bilder/53028713_312272079492058_100357494328524800_n.png" alt="skrivkramp" width="494"> <figcaption>Du vet att du borde skriva! Ill: Cristopher Anderton</figcaption> </figure>

I modern barnuppfostran förkastas de beprövade knepen hot och mutor. I stället anser experterna att barn ska drivas av inre motivation, trots att de själva knappast skulle gå till jobbet om de inte fick lön varje månad. Jag vet inte hur det är med dig, men personligen lider jag av en osedvanligt svag startmotor. <!--more--> Ett fel i den grundläggande konstruktionen. För att jag ska ta itu med skrivande krävs mer än förhoppning om framtida tillfredsställelse. Jag måste lura mig själv genom att …

SKAPA EN DEADLINE  
Största problemet för mig är att jag inte klarar av att skriva utan deadline. Inte ens då går det särskilt bra. Jag hetsar mina uppdragsgivare att ge mig korta deadlines, att beskriva fasansfulla följder av mitt eventuella svek. Helt utan deadline vandrar jag som en osalig ande mellan tvätthögar, disk och andra distraktioner i hemmet. Om du är en bättre och mer karaktärsfast än jag räcker det kanske med att avge ett löfte dig själv att skriva 5 000 tecken per dag. Väldigt ofta använder jag [pomodoro-metoden](https://en.wikipedia.org/wiki/Pomodoro_Technique "Tomat, tomat!"). Det vill säga: jag sätter en äggklocka på 25 minuter. Så pass mycket självaktning har jag att jag sätter i gång då. För nästan alla kan stå ut med att göra något oöverstigligt i futtiga 25 minuter. Enligt pomodoro-metoden _måste_ du avbryta efter 25 minuter hur mycket flyt du än har, det brukar inte jag göra eftersom jag ofta hamnar i hyperfokus. Om du precis som jag är en undermålig arbetsledare kan du även …

REKRYTERA EN EXTERN DIKTATOR  
Lova att skicka 5 000 tecken till din älskling, en önskad älskling (som du vill förtrolla med dina bokstäver) alternativt din dödsfiende (som kommer att håna dig om du inte levererar). Det är dock inte säkert att någon av dem vill vara din förmyndare. Ett mer realistiskt alternativ är en skrivgrupp, som dock kan innehålla någon av de tre övriga förslagen. Eller skrivpartner. Är du mer teoretiskt lagd kan du …

SKAFFA EN GURU  
Det finns dröser av självförbättringsböcker som lovar att avslöja hemligheten om hur du blir rikare och lyckligare genom ökad effektivitet. Så klart att du köper en bok! Du läser. Nickar och hummar instämmande. Sen kan du köpa och installera en app baserad på bokens metod. Organisera din tillvaro genom att mata in uppgifter ett par timmar.  Tjattra om frälsaren och hens evangelium på olika forum. Råka i luven på idioter. Snart få korn på en ny guru som är strået vassare än din gamla. Du inser att du bara måste köpa hens bok. Och så upprepas hela processen från början igen. Utan att klämma ur dig ett ord på din bok kan du vara strängt sysselsatt med att effektivisera skrivarbetet.

När jag har det extra körigt använder jag faktiskt en app som heter [Things ](https://culturedcode.com/things/ "Inte pjåkig!")som är löst baserade på metoden [GTD, getting things done](https://en.wikipedia.org/wiki/Getting_Things_Done).  Där matar man in ärenden, uppdrag, projekt et cetera. Och prickar av dem allt eftersom du uträttar dem. Fördelen är att du slipper hålla så mycket i huvudet där de parasiterar på din energi. Du kan också ange deadline, märka upp olika uppgifter med plats och grad av obehag du förväntar dig. Om du sitter lägligt till kan du till exempel klicka på taggen _telefon_ och få en lista på samtal du borde ringa. Varje morgon får du upp dagens göromål. Det är lite som att ha en chef. Tyvärr inte tillräckligt elak, men lite hjälper det. Egentligen handlar det om något så grundläggande som att …

DELA UPP EN STOR UPPGIFT I FLERA SMÅ  
Om du sätter dig ner och tänker "nu ska jag skriva en roman, helst en som vinner Bookerpriset" finns risken att hjärnan går i baklås. Allt blir högtidligt. Överväldigande. Punktlistor fungerar för vissa, annars kan du Du kan också tagga ner genom att  …

INLEDA MED UPPVÄRMNING  
Skriv på om vad fan som helst. Hur en av karaktärerna i din bok reagerar  på dagens toppnyhet. Om hur hjältinnans fru klär på sig på morgonen. Låt fingrarna flyga! Nästan alltid blir det en fin rad att spara och kila in någonstans i boken. Så där, fingrarna funkar! Det blev 1 399 tecken, inte i riktigt rätt ordning. Ett annat, mer pretentiöst knep är förstås att blogga om sitt skrivande. För vissa fungerar det som uppvärmning, för andra som prokrastinerande. Min hjärna vill nästan alltid …

SKRIVA NÅGOT HELT ANNAT  
Trots att jag behöver hårda tyglar vad gäller tidsramen har jag svårt att skriva det som förväntas. ( Därav mitt ledord [Lita på det oväntade](https://lisaforare.se/post/skrivkramp-3-varför-vill-du-skriva/ "Mer om motivation")). Paradoxalt nog vill jag ha detaljerade riktlinjer. Ge mig regler som jag antingen bryter mot, omtolkar eller slingrar mig undan, då skriver jag som besatt. När jag gick skrivkurs fick vi i uppgift om att skriva om ett riktigt skrämmande rum. Eftersom jag är livrädd för klyschor skrev jag om ett rum fullt av rosa plyschkuddar och affischer på enhörningar – men med ett hot lurande mellan raderna. Sen kan man förstås  ge sig själv friheten att helt lämna sitt stora skrivprojekt. [Författaren Tim Clare leverera en tiominuters workout](http://www.timclarepoet.co.uk/tim-clares-weekly-writing-workout/) varje vecka om du vill ha hjälp på traven. Om inget av ovanstående hjälper brukar jag sätta mig och …

LÄSA NÅGOT  
Att läsa andra författare brukar fylla mig med avund över deras överlägsenhet eller ilska över deras uselhet. Jag har aldrig sagt att jag är en ädel person. Båda dessa tarvliga känslor kanske kan utgöra eldfängt bränsle även för din startmotor, beroende på vilken läggning du har. Poesi funkar bäst för mig, där finns koncentration och precision som skärper min koncentration. Jag sparar ord och fraser att knåda om till eget material. Inte minst poesi sätter igång tankearbetet och ger mig lust att formulera något eget. När jag ska skriva en vinterscen googlar jag fram dikter om is, kyla och snö. I min roman har jag hämtat mycket inspiration från Karin Boye, gamla skillingtryck, David Bowie, Edith Södergran.

DIVERSE SMÅKNEP  
En del författare avbryter skrivandet mitt i en spännande scen för att underlätta igångsättandet nästa gång. Testa gärna det! Jag tycker snarare att det är väldigt förvirrande. Dessutom är det frustrerande att avbryta mitt i flytet. Däremot fungerar det för mig att avsluta varje skrivpass med att planera nästa. Antingen leta upp en skrivövning att inleda med eller att göra en punktlista på saker som måste göras. Minibelöningar som en kopp kaffe, en snus, en chokladbit eller en kvarts surfande efter söta kattungebilder kan sprängas in under dagen.