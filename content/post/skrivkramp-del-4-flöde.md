+++
avsandare = ""
bilder = []
date = "2019-05-30T21:00:00+02:00"
inlaggsbild = "/68817344_10158660966342678_9119005800440791040_n.jpg"
title = "Skrivkramp, del 4: FLÖDE"

+++
<figure> <img src="/bilder/68817344_10158660966342678_9119005800440791040_n.jpg" alt="monster" width="494"> <figcaption>Illustration: Cristopher Anderton</figcaption> </figure>

Du är äntligen i gång. Fingertoppar mot tangenter. Bokstav för bokstav. Kanske kommer orden sakta och orytmiskt som de första tvekande dropparna i ett sommarregn. Men du skriver! Snart knattrar du som besatt. I bästa fall. <!--more-->

Jag har en mild ADD med nyckfull och svag startmotor, lyckligtvis innebär det också att jag kan hamna i hyperfokus. Hyperfokus är inte alltid bra, det är också en del av de koncentrations- och uppmärksamhetsstörningar som kännetecknar ADD. Och en anledning till att personer med ADHD och ADD, inte svarar på tilltal och har svårt att avbryta när de gör något som intresserar dem. Men just när det gäller skrivande kan hyperfokus vara en tillgång.  <br> Alltså glömmer jag tid rum, till och med mat när jag är inne i skrivandet. Och andra sysslor som omgivningen tycker är viktiga. <br>  Ibland är det lika svårt att sluta som att börja. Men bara om det är kul och stimulerande. När det är tråkigt krävs otroliga mängder knep och knåp för att hålla ångan uppe.

Beroende på vem du är och hur du tycker om att jobba passar olika tekniker för att inte tappa styrfarten. Vissa följer troget ett synopsis och planerar varje skrivpass i detalj. De behöver en grund i form av ramar och struktur och använder kanske olika typer av kreativitet vid planering och skrivande. Andra låter tankarna flyga fritt, fångar orden som vildfåglar ute i luften, låter sig ledas av instinkt och skapar (i bästa fall) kosmos av kaos i efterhand. Det finns inget rätt eller fel och många pendlar mellan olika strategier ungefär som en musiker spelar efter noter ibland och jammar hejvilt vid andra tillfällen.

Själv kan jag sällan skriva strukturerat utan går på inspiration och association. Sen pusslar jag samman fragmenten till en dramaturgi, ofta formas en del av den under flödeskrivandet. Ibland måste jag stuva om helt.  <br> Jag skulle aldrig blivit skribent utan ordbehandlare som gör det lätt att klippa, klistra och flytta runt. Min metod är tidskrävande men ger roliga och överraskande texter.

Fasan är förstås att [så kallad skrivkramp ](https://lisaforare.se/post/skrivkramp-ett-överdiagnostiserat-tillstånd/ "Skrivkramp – finns den?")kommer som en blixt från klar himmel. Mitt i en mening stumnar fingrarna och du sitter och fånstirrar på skärmen. Hur stannar man kvar i flödet? Och hur tar man sig tillbaka när man kastats ur sadeln?  <br> Vissa trivs med att ta schemalagda pauser. Anta unnar sig en paus efter att ha klarat av ett specifikt moment. Se [inlägget om startmotor](https://lisaforare.se/post/skrivkramp-del-3-startmotor/) för mer idéer.

En del använder hypnostekniker, meditation och andra knep för att hamna i flow eller olika kreativa tillstånd. Men jag är inte expert på det och därför håller jag klaffen.

UNDVIK DISTRAKTIONER Jag har som regel telefonen avstängd när jag skriver. Att börja prata om oväntade saker gör att jag tappar tråden. Dessutom gillar jag inte att prata i telefon och blir ofelbart på dåligt humör av det. Däremot har jag inga problem att ta en liten tankepaus genom att socialisera på Facebook eller läsa nyheter på Twitter.  <br> Undersök vad som funkar för just dig!

SKAPA IDÉDOKUMENT Ofta när jag skriver på en historia poppar det upp idéer eller fyndiga formuleringar formuleringar som absolut inte passar in i manuset just då. Därför har jag ett dokument i anteckningar öppet så att jag snabbt och enkelt kan dumpa material där. Sen kan jag i lugn och ro kika på det vid ett annat tillfälle. Har jag tur är de i alla fall hälften så genialiska som jag tyckte när jag klottrade ner dem.  En del av dem har blivit eller kommer att bli noveller. <br> Om jag inte skriver ner infallen blir de till flugpapper för uppmärksamheten, eftersom utforskandet av nya möjligheter är mer lockande än något man harvat med länge.

OM DU KÖR FAST … När det står helt stilla kan det vara fiffigt att skriva på något annat ett tag. Eftersom jag försörjer mig på skrivande har jag alltid flera skrivprojekt på gång. Någon artikel, en krönika och ett bokprojekt.  <br> Ett sätt att hålla flödet konstant är att ha en skrivdagbok där du skriver om ditt skrivande. En halvhemlig blogg eller en oldschoolig dagbok på papper där du sammanfattar vad du gjort och vad du ska göra härnäst. En annan metod är att gå tillbaka och redigera det du skrivit tidigare. I de textmassorna kan små frön till framtida skrivande gömma sig.

SKRIV DÅLIGT Fortsätt att skriva fast det blir pinsamt dåligt, bara baxa historien framåt utan krav på finess eller realism. Så länge fingrarna rör sig och handlingen kravlar sig framåt är halva jobbet gjort. <br>Andas. <br>Skratta åt din uselhet. Och skriv om och bättre senare.

ORDLEKEN Ta fram en ordbok, eller för all del vilken bok som helst … Blunda, slå upp boken och sätt på måfå ett finger på sidan. Ordet du pekar på måste du ha med i nästa mening/stycke du skriver. Plötsligt blir det roligt att skriva igen!

PROBLEMLÖSNING Flödet kan sina när du skrivit in dig i ett hörn och inte vet hur du ska ta dig ut. Har hjälten trillat ner i en lejongrop? Har du råkat knyckla ihop tidslinjen?  Mitt bästa knep för att lösa problem i manus är att formulera utförliga frågor till min redaktör eller en testläsare.  <br> Vanligtvis hittar jag svaret själv när jag väl renodlat frågeställningen. Bara i enstaka fall blir jag tvungen att skicka frågorna. Att förklara svårigheter för andra kräver nämligen att man är mycket exakt och detaljerad – och med ens ökar ens egen förståelse och nya möjligheter. utkristalliseras.  <br> Om svaret inte uppenbarar sig under formulerandet av frågan kan det mycket väl göra det när jag ställer upp olika lösningsalternativ. Jag är en person som tänker genom att skriva, jag knattrar fram stigar genom mörka snårskogar av fakta tills jag hittar en ljus glänta. Andra personer tänker bäst innan de driver.