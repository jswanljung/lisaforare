+++
avsandare = ""
bilder = []
date = "2019-08-21T00:00:00+02:00"
inlaggsbild = "/LindaLinda.jpg"
title = "Bisous! Doften av en historia eller \"sälj grej med sprej\""

+++
<figure> <img src="/bilder/LindaLinda.jpg" alt="Linda" width="494"> <figcaption>Foto: Pär Olsson </figcaption> </figure>

Jag är matskribent och har klafsat runt i gastronomiträsket i en bra bit över trettio år. När jag byggde min Varken-värld, med bokstäver som kitt, var dofter och smaker naturligtvis viktiga byggstenar. Vissa författare har ett soundtrack till sina böcker, en låtlista som hjälper dem att komma i stämning under skrivandet. Eftersom jag är snudd på tondöv hade jag i stället en uppsättning parfymer att sniffa på för att få upp vittringen på historien. <!--more--> Närmare bestämt tre parfymer av parfymmakaren och doftgeniet [Linda Landenberg](http://www.lindalandenberg.com/about.html): Bisous, Lavender & Wool och Trembling rose.

Jag måste erkänna att jag förälskade mig i Linda Landenberg långt innan jag föll för hennes parfymer. Men vi tar det från början, eftersom det är ett så praktiskt ställe att börja på.

För snart fem år sedan råkade jag mer eller mindre av en slump bli vän med [Kicki Norman](https://www.daisybeauty.com/bloggare/kicki/ "The one and only") och [Linnea Öst](https://www.daisybeauty.com/bloggare/arga-tanten/ "Arga tanten"), Sveriges kanske kunnigaste skönhetskribenter. Till min förvåning och skräckblandade förtjusning blev jag sen bjuden på Daisy Awards Gala, där priser delades ut till årets bästa produkter. <br>Jag stövlade in direkt från en matfotografering: osminkad i säckliknande grå linneklänning med foträta skor och utan att känna just någon. Naturligtvis hamnade jag vid ett bord med några festens vackraste varelser, bland annat Linda Landenberg – tjusigast av dem alla. Tillika grönsaksleverantör åt några av Stockholms yppersta krogar.

Jag var milt skräckslagen, det måste erkännas. Skönhetssfären i Sverige är dock oerhört seriös, trevlig, varm och inkluderande, så till min stora förvåning toktrivdes jag. Lindas parfym [Lavendel & Wool ]()sopade senare hem pris som årets parfym. Ändå tog det lång tid innan jag bekantade mig med den.

För något år sedan hade jag kört fast lite i romanskrivandet. Historien löpte på, men jag behövde gjuta mer liv i flödet och karaktärerna.<br>I stället för att skriva satt jag och slögooglade, och eftersom Linda var en trollbindande person kollade jag upp henne och hennes parfymer. Vad gör man inte för att slippa skriva? <br>Linda är något av en legend: personen som låg bakom det nyskapande upplägget på NK:s parfymavdelning. Sin första parfym Bisous satte hon samman för att återskapa sin upplevelse av den sanslöst väldoftande frangipanin, en vit blomma vars naturliga doft inte kan fångas på flaska utan måste byggas från grunden av andra aromatiska element. Ni som har läst Varken förstår varför detta faktum fascinerade mig så.

Efter som tanke och beslut är ett (ibland noll komma fem) hos mig kontaktade jag Linda, som försåg mig med prover. Jag insöp och hänfördes. Och tre av parfymerna hjälpte mig att bygga vidare på karaktärerna. <br>Sara, Gabriella och Louise fick varsin signaturdoft, dofter som sa något om dem – men också förklädnader de kunde gömma sig bakom. Och hux flux gjorde dessutom en helt ny karaktär entré i boken för att introducera parfymerna.

[Bisous](http://www.lindalandenberg.com/bisous-eu.html), som närmast kan översättas till småpussar, är en intrikat och subtil komposition av frisk petitgrain, jasmin, pikant ingefära, kryddig tolubalsam, honungssötma och en gnutta smeksam choklad. Inte en doft jag skulle gett chansen om jag inte läst på innan[ – för då hade jag bara uppfattat uppenbara charme](https://www.fragrantica.com/news/Jewels-from-Sweden-Linda-Landenberg-s-Tender-Scents-10088.html)n, inte intelligensen som dolde sig bakom. Bisous kan nämligen tyckas vara en smula anspråkslös vid första sniffen, men för den som lägger huvudet på sned, blundar och känner efter är det ett enastående litet hantverk.<br>Jag älskar sen tidigare både mat och litteratur som kräver eftertanke, så det var enkelt att flytta över tankesättet till parfymer.

[Trembling Rose ](http://www.lindalandenberg.com/a-trembling-rose.html)initierade en oväntad vurm för rosenparfymer, något jag haft svårt för tidigare. Men Linda har balanserat olika rosenaromer med vad jag uppfattar som nyplockade blad från en svart vinbärsbuske. En lättälskad  och omtumlande parfym som utgör en perfekt påminnelse om att Varken utspelar sig i ett villaområde.

Lavender & Wool är den allra mest originella av de tre. En ytterst återhållsam provokation som på en och samma gång är ett betryggande barndomsminne och sofistikerad mognad. Parfymen luktar som den heter med ett tillägg av bland annat varm choklad. Kanske är det en parfym som kräver visst självförtroende, eller så är det en doft som ingjuter självförtroende. Jag vet bara att den gör mig lycklig. I romanens första kapitel, [som du kan läsa här](https://lisaforare.se/langsjon/varken-sten/), förekommer parfymen.

Nå, hur som helst. Jag vill bjuda på en förhöjd läsupplevelse. Därför kommer jag snart att lotta ut en flaska parfym till en entusiastisk Varken-läsare. [Håll utkik i Facebookgruppen!](https://www.facebook.com/lisaforareskriver/) Flaskan kostar runt en tusenlapp i butik. Jag är inte sponsrad av något företag utan betalar (ett kompispris) för parfymen, så det här inlägget är ingen reklam utan en kärleksförklaring.

Om man köper boken direkt av mig kommer man dessutom kunna köpa små rör av de tre parfymerna till självkostnadspris. För att få veta när och var ni kan hitta mig och Lindas väldoft får ni också hålla utkik på Facebook.

Att dela det här inlägget lär ge bra karma inför utlottningen. Men det är absolut inget krav för att delta. Visserligen ökar antalet tävlande om du delar, men om jag och Linda är generösa kan du också vara det!

Min roman VARKEN kan du redan nu köpa på nätet. Bättre är det förstås om du stöttar din lokala bokhandel där den dyker upp 28:de augusti. Bäst och billigast är det att köpa den hos mig, för då kan du dessutom köpa tre parfymprover för självkostnadspriset tjugo kronor. Jag är nästan alltid hemma: Tulpanvägen 4 i Älvsjö, farligt nära Långsjön. Skicka ett meddelande på Facebooks messenger innan om du vill vara helt säker på att jag är hemma.  
  
[. Här kan du läsa första kapitlet av romanen.](https://lisaforare.se/langsjon/varken-sten/)

[ADLIBRIS ](https://www.adlibris.com/se/bok/varken-9789177990864)  
[BOKUS](https://www.bokus.com/bok/9789177990864/varken/)

![](/bilder/bisous2-crop-u5479.png)