+++
avsandare = ""
bilder = []
date = "2019-03-21T16:03:42+01:00"
inlaggsbild = "/skarseldChanderton.jpg"
title = "Skrivkramp, del 2: MOTIVATION"

+++
<figure> <img src="/bilder/skarseldChanderton.jpg" alt="bible-hell" width="494"> <figcaption>Skärselden aka skrivkramp de luxe. Ill: Cristopher Anderton</figcaption> </figure>

En vanlig anledning till att man inte får något skrivet är att man helt enkelt inte vill skriva en bok. I alla fall inte mer än man vill plantera tomatplantor, städa ur köksskåpen, skaffa hund eller dra på picknick. I det här avsnittet tänker jag diskutera varför jag överhuvudtaget beslutade mig för att skriva en bok. Och varför boken måste bli vad den blev.<!--more-->

Det finns en liten grupp som hellre vill vara författare än att skriva själva böckerna. Eller komma med idéer och låta någon sköta tangentknattrandet. Det är lätt att göra sig lustig över dem, som om man vore lite förmer. Ja, rentav ett högre väsen, nämligen Konstnär besatt av själva Skapandet. Idag handlar författaryrket om så mycket mer än tangenthamrande. Historien är faktiskt mer avgörande för framgång än stilistiken. Mediaframträdanden, föreläsningar och signeringar är också viktiga att behärska och genomföra med entusiasm. Jag önskar att jag slapp större delen av kringjobbet och är bättre på stilistik än story. Vill man bli proffs och försörja sig på att kränga bokstäver så måste man dock behärska rubbet. Av lojalitet mot sina läsare om inte annat. Att drömma om att bli författare eftersom det delvis är ett glassigt yrke, kan både piska på och förlama. Men underskatta aldrig kraften i avundsjuka, äregirighet och exhibitionism. Den kan hjälpa dig långt. Eftersom jag redan publicerat mig i de flesta stora tidningar, [apat mig i radio](https://sverigesradio.se/koksradion "Köksradion"), nött tevesoffor och skrivit några [hyfsat kända matböcker](https://www.bokus.com/bok/9789174751031/matmolekyler-kokbok-for-nyfikna/ "Matnörderi på hög nivå") hjälper de inte just mig. Tyvärr. Det kortet är utspelat. Skälet till att det tagit så pass lång tid at skriva klart beror faktiskt på att min drivkraft inte varit tillräckligt stark.

Så varför ville jag skriva en roman? Mest för att jag är lite trött på att skriva mat. Det går trögt. Och man måste producera text i förfärande hastighet för att försörja sig på matskrivande. Arvodena har stått stilla i tjugo år. Inte för att romanskrivande är mer lukrativt, men det har nyhetens behag. Det är roligt på jobbet igen. Att få berätta historier, leka med orden är inte nödvändigtvis det finaste jobbet men ett av de roligaste. Förhoppningsvis kommer det in lite kulor för skrivcoachande och föreläsningar. Skriva är tyvärr det enda jag kan, så det är värt ett försök.

Varför ska just du skriva just din bok? Först måste du besvara en viktig fråga: Vem är du egentligen? För många, många år sen gick jag på en dagskurs i mediaträning. En av uppgifterna vara att välja ut tre ledmotiv. Ord eller fraser som beskrev ens verksamhet. Och nu kommer det svåra – kombinationen måste vara unik. Det dög alltså inte med kreativ, snygg och kompetent. Ungefär som att en programförklaring för en krog inte kan vara "vällagad, god mat på de bästa råvarorna. Mina tre punkter var: Lita på det oväntade, I.E.O.A.S, och B.T.A.

Vad förkortningarna betyder? Vet du inte det? Den första betyder _instant expert on any subject_, vilket är ett raljerande med min fäbless för nördig research. Den andra hade min farfar på sitt visitkort från 40-talet och betyder _been to America_, vilket var häpnadsväckande på den tiden. Ett paradexempel på hur mitt trepunktsprograms manifesterades i text var det, i mina ögon högst relevanta stycke om den amerikanska porrstjärnan Linda Lovelace jag lyckades kila in i en artikel om sichuanesisk mat. Av någon obegriplig anledning redigerades det bort av nitiska redaktörer.

Nu infinner sig förstås frågan om vem jag är som skönlitterär författare. Jag jobbar fortfarande på svarets exakta formulering. Men det har förstås att göra med tre saker. Det faktum att jag är matskribent vilket gör att jag har mångårig träning i att få texten att dofta, smaka och glöda av färger. Jag har ett driv i språket som jag av nöden utvecklat för att kunna baxa en läsare genom tjugotusen tecken om kålrötter. Min milda ADD gör att mitt skrivande på gott och ont blir _extra allt_, eftersom jag inte klarar av att ha tråkigt. Lösningen är övernaturligt rajraj, eftersom verkligheten inte räcker till. Mitt lokalsinne är så uselt att jag går vilse i en skokartong, vilket har lett till att jag lättjefullt placerat det mesta av handlingen vid Långsjön i Älvsjö, ett par hundra meter bort. Det blev visst fyra saker … extra allt, som sagt. Just nu kan jag inte skriva en annan bok än den jag skriver: en mörk, myllrande, ordrik saga om det hemliga livet vid Långsjön.

Det finns en massa olika skäl till att skriva en bok. Och det hjälper en hel del att du hittar dina unika. Visst är det vägen som gör mödan värd, men om du inte har ett mål kan du komma att irra runt på villovägar i evigheter. Du kan skriva ditt liv som terapi, forska i din släkts förflutna eller dina egna perversioner. Kanske älskar du också att leka med ord? Får du en kick av att en läsares hjärta att dunkar hårt och snabbt av kättja eller fasa? Var ärlig! En bekant skrev en roman för att inte bli alkoholist efter sin skilsmässa. Är det så att kilometerlång signeringskö på bokmässan är en viktig del av motivationen ska du inte hymla om det. I alla fall inte för dig själv, utåt kan du förstås vara klädsamt blygsam. _Whatever peels your potato!_

Det hela kan sammanfattas med att om du verkligen vill ha yrket författare (eller i alla fall ha det som seriös sidoverksamhet) och känner att det finns en bok som ingen annan kan skriva – då har du motivationen klar. En sak till. Du behöver en god portion storhetsvansinne också, den kan jag tyvärr inte hjälpa dig med. Jag har ingen aning om var mitt kom ifrån.

**LISAS SKRIVTIPS!**  
Sammanfatta dig själv och dina drivkrafter i tre punkter som får plats på baksidan av ett visitkort. Ett miniporträtt av dig som författare.

**FLER DELAR I SERIEN OM SKRIVKRAMP**  
[Skrivkramp, intro: Ett överdiagnostiserat tillstånd?](https://lisaforare.se/post/skrivkramp-ett-överdiagnostiserat-tillstånd/)  
[Skrivkramp, del 1: LIVET](https://lisaforare.se/post/skrivkramp-del-2/ "Det som pågår medan du skriver.")

ILLUSTRATION: [CRISTOPHER ANDERTON](https://www.instagram.com/chanderton/ "Geni!")