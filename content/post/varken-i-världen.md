+++
avsandare = ""
bilder = []
date = 2020-02-08T09:06:41Z
inlaggsbild = ""
title = "Varken i världen"

+++
<figure> <img src="/bilder/79282957_443276489936998_8970503277502267392_n.png" a_rk musa" width="494"> <figcaption> HURRA! Ill: Cristopher Anderton</figcaption> </figure>

Precis när Varken släpptes var jag ytterst osugen på att prata om boken. Jag var ju helt inne i del två. Sen fick jag problem att koncentrera mig på uppföljaren eftersom mottagandet av ettan pockade på uppmärksamhet stup i kvarten.

Hur ska man summera Varken resa ut i världen? Jo, tackar som frågar det går verkligen inte att klaga. <!--more--> Bättre än jag vågade hoppas. Förutom på det faktum att jag tycker att Varken kunde sälja ännu bättre, med tanke på hur hiskligt lång tid den tog att skriva.

De fotbollstokiga nornorna har vunnit flest hjärtan. Men även maran har fått sig en och annan släng av kärlekssleven. Många av de här recensionerna går i många tidningar, så spridningen är stor. Vilket gör det orättvist för de många fabulösa  fantastikböcker som inte blir recenserade alls.

[DN:s Jonas Thente](https://www.dn.se/kultur-noje/bokrecensioner/tat-saga-fran-morkaste-fruangen/ "DN"), som möjligen är Sveriges mest kritiska kritiker, menar att boken är härligt halsbrytande. Och bedömer att jag grejar de många perspektivvskiftena som "ett fullfjädrat proffs".

En stort underhållande och imponerande debut, summerar [Malin Krutmeijer i Aftonbladet,](https://www.aftonbladet.se/kultur/bokrecensioner/a/1n8rOe/forortsfantasy-som-imponerar "Aftonbladet")  även om hon tycker att det blir lite overload mot slutet. Det är en fin balansgång som författare: skräckälskare kräver mer rajraj och vill inte ha allt skrivet på näsan. Deckarälskare vill ha allt prydligt uppbenat och mindre kritter. Krutmeijer är särskilt förtjust i gestaltningen och alldeles särskilt humorn, vilket gör mig extra glad. Alla tycker inte att skräck och humor hör ihop.

[Anders K Gustafsson kulturredaktör  på Dalarnas tidningar](https://www.vlt.se/logga-in/bland-maror-och-tankelasare-i-fororten-sprittande-romandebut-ger-mersmak) tycker att det är en bok man vill sträckläsa och skriver rätt och slätt: "Varken är en fantastisk bok"

[I Sydsvenskan kallas boken "bitvis briljant"](https://www.sydsvenskan.se/2019-10-17/lisa-forare-har-sin-karriar-som-matskribent-att-tacka-for?fbclid=IwAR35qw3L5-Rans-ObbHpp7Wd9Wb49e8fHLC7oY2aSIOTZFg_JyqoGyMQ_VY) i en recension där jag faktiskt inte riktigt förstår vad den lätt förvirrade skribenten menar. Eventuellt för att det var den enda recensionen som inte var väldigt positiv. Och jag hör så dåligt på det örat.

[Bella Stenberg på Borås tidning](https://www.bt.se/kultur/vantan-pa-nasta-del-kanns-redan-for-lang/) längtar redan otåligt på nästa bok och utnämner [i en annan artikel](https://www.bt.se/kultur/arets-basta-spanningsromaner/?fbclid=IwAR3fB6EouIcI2-c_FxFDY8T9163ecs5ckOgpjXXXitC_YRi9lKeLB73po2A) Varken till en av årets tre bästa spänningsböcker.

I övrigt har bokbloggare överlag varit mycket entusiastiska. En och annan har varit oförstående, men så är det. En roman som passar alla existerar inte. Och Varken är ärligt talat rätt knasig. De blogginlägg jag gillar bäst är de som både ger beröm och intressant kritik. [Som Anders Kapps omsorgsfulla läsning](https://www.kapprakt.se/lisa-forare-varken/).

På instagram har jag blivit särskilt glad över hyllningar från [Simona Arnstedt](https://www.instagram.com/p/B65EyPSpsQe/) (vars son tog över och sträckläste bioken) Och [hemskaste Helena Dahlgren](https://www.instagram.com/p/B8NjBCdpiHY/), förstås.

Jag har fått flera förtjusande mejl. Extra glad blev jag av bibliotekarien som skrev att hon bara tänkt nosa på boken vid läggdags men sträckläste till halv fem på morgonen. För att inte tala om den nittiofyraåriga man som är en mycket kritisk läsare men skrev ett berömmande brev till mina föräldrar. Och fick mig att känna mig som en trettonåring som vunnit en uppsatstävling.

[Twittrarna ](https://mobile.twitter.com/Birgitta_L/status/1185293961713344514)har [kvittrat ](https://mobile.twitter.com/Arkelsten/status/1177860198536482816)[översvallande ](https://twitter.com/Terejs/status/1168267286035275776?fbclid=IwAR2Bi43a_UGvmSPj3XWCJHwcfdPPtE7x7iZTixFYtvBFZDQq4S1f5g5n2kE)b[eröm](https://twitter.com/MichisLeo/status/1168904118628495371?fbclid=IwAR253r5PLY-AAorvcvHhLO_lwO1KTHxIIgwi4ltYvzRu9eeuvbt4qg9NkAc). För övrigt har några glada läsare startat en bokcirkel på Twitter.

Jag hamnade på förstasidorna på [Mitti](https://mitti.se/noje/skrivit-fantasy-fullvuxna/?omrade=liljeholmenalvsjo) och [Älvsjö Direkt](https://www.stockholmdirekt.se/nyheter/lisa-forares-skrackroman-lockar-folk-till-langsjon/repsjC!hXTQV34RFpN5kT4b8CJpHg/). Två ovanligt bra journalister skrev fina artiklar.

Mer då? Fenomenala skönhetsredaktören Jessica Blockström tipsade lite oväntat om Varken på sin beauty-sida i Femina. Och jag blev månadens författare i M-magasin.

Jag har säkert glömt något, men det här är ju alldeles tillräckligt för att ge mig högfärdshosta.