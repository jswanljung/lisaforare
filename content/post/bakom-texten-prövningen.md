+++
avsandare = ""
bilder = []
date = "2019-02-05T12:58:32+01:00"
inlaggsbild = ""
title = "Bakom texten: Prövningen"

+++
Skriv om det du kan, brukar experterna säga. Jag kan förstås inte ett smack om hur det är att ha horn, men jag vet [hur det är att älska ett barn och känna avgrundsdjup oro](https://taffel.se/blog/tio-fingrar-tio-tar "Gammelmamma!"). Och skammen över den egna otillräckligheten lurar alltid i ens inre. Alla känslorna i [novellen jag skickade in till Skrivas novelltävling ](https://tidningenskriva.se/tavlingar-och-event/tavlingar/skrivas-stora-hosttavling-2018/provningen/?fbclid=IwAR0wpKbV7g_y7ArZfqp8QNzEQ4TkQvzTE2YcIoXCnpVWltcu0y3Mp6RGvS4 "Jag vann!")är mina, även om mamman i novellen är av en annan sort än jag.<!--more-->

Ett skäl till att jag älskar fantastik så mycket är att övernaturliga element ger en möjlighet att ställa en situation på sin spets genom att dramatiken skruvas upp. Det okända ställs mot det välkända och får läsaren att lystra lite extra eftersom inget kan tas för givet. Jag tror att alla människor känner sig lite som ufon i vissa väldigt vardagliga situationer – och vi tror gärna att vi är de enda som känner så.

I en kort novell är det viktigt att inte slösa på tid och ord utan kasta läsaren direkt in i handlingen. Stark närvaro redan i de första raderna är ett läckert recept på framgång. Oron får växa långsamt fram till kulmen av ren och skär fasa – prövningen.

Jag är lite konservativ och gillar noveller som är lite mer än bara ett utsnitt av en verklighet. Gärna en eller ett par överraskande vändningar – helst ska läsaren kippa en aning efter andan. Vändningen behöver absolut inte ligga i slutet. Ett starkt driv framåt är också viktigt. Stilistiskt handlar det mycket om att en mening ska leda till nästa genom rytm och innehåll. Det första ordet och det sista i meningarna ska harmoniera eller skava på rätt sätt. I Prövningen använder jag inte bara starka känslor utan också gåtfullhet för att skapa driv, läsaren får information om den mystiska familjen genom detaljer utportionerade i förbifarten. Det viktiga för huvudpersonen ska vara det viktiga för läsaren. Jag försöker undvika för många objektiva meningar som enbart har till syfte att informera läsaren. Vad lägger huvudpersonen märke till och varför?

Ett knep är att låta huvudpersonen förhålla sig till detaljer, så att de känns motiverade att ha med. Eller att låta detaljerna påverka handlingen, på så sätt puffar de på kreativiteten. Exempel: "När jag går tycks golvet svikta under mig. Stegen blir lätta som i lövskog." Här förstår man var mamman trivs och hör hemma, lite så där i förbifarten. Egentligen hade det räckt långt men vad mamman tänker på för att lugna ner sig själv under prövningen ger också information: "Tänker på nattsvala dungar med saftig grönska, den månglittrande sjön och alla dess vänliga små ljud. Sommarstugan i Roslagen där vi alla kan springa fritt. Vår lilla flock." Men informationen filtreras genom huvudpersonen och besjälas av känslor. Väldigt mycket av trixandet för att ge liv åt information gör jag instinktivt – på grund av att det sitter i märgen efter tjugofem år av fackskrivande. Jag kommer att skriva mer om knepen här i bloggen.

Samtidigt behövs tomrum där läsarens fantasi får spela fritt. När man skriver noveller kan man lämna mycket mer luft än när man skriver romaner. Det är därför novellformen passar så fint för fantastik och skräck: små berättelser som skimrande magiska såpbubblor. De flesta läsare orkar inte sväva i ovisshet på samma sätt under läsningen av en lång roman. Fantastikromaner är därför svårare att lyckas med, det är lätt att mystiken försvinner när för mycket beskrivs och förklaras.

I novellen har jag använt ett ganska neutralt och försiktigt språk för att förstärka det vardagliga hos huvudpersonen. Tack vare de bisarra inslagen och lite absurd humor stör det inte så mycket att novellen har rejält sentimentala inslag. Tvärtom behövs de nästan för balansens skull. Hur farlig monstermamman är får inte läsaren veta, monstrositet ligger i betraktarens öga. Kanske äter hon upp Millan vid nästa fullmåne. Vem vet? I[ sina egna ögon är hon bara en vanlig bristfällig mamma](https://lisaforare.se/post/monstret-inom-dig/) som älskar sin unge till månen och tillbaka.

![](/bilder/51405189_10158124607837678_1468476887897473024_n.jpg)

Foto: Fredric Dahlgren