+++
avsandare = ""
bilder = []
date = ""
draft = true
inlaggsbild = ""
title = "Lisa Förare – en annorlunda skrivcoach"

+++
Jag älskar att hjälpa andra att skriva bättre, roligare och mer njutbart. Älskar. Efter att ha provat mig fram med några försökskaniner vet jag vad jag är bra på och vad jag inte ska ägna mig åt. Som skrivcoach vänder jag mig därför främst till dem som redan kan skriva hyfsat, men står och stampar i utvecklingen. Jag kommer helst in tidigt i utvecklingen och kikar på ett halvfärdigt manus, på så sätt slipper du  riskera att skriva dig in i en återvändsgränd.

Ja, du kan kalla det lektörsläsning – men det är lite mindre och mycket mer än så. Jag är varken redaktör eller språkgranskare, min idé är mer djupgående: att identifiera dina problemområden och styrkor och därefter trimma och peppa dig som stilist och berättare. Visst, du får ett ordentligt utlåtande om ditt manus, men framför allt får du utförliga, roliga, peppande och pedagogiska råd och ny kunskap. Du och din förmåga är i fokus. Jag vill inte bara göra det specifika manuset bättre, jag vill hjälpa dig att bli en bättre författare.

> SAGT OM MIG:  
> "Lisa har varit min favoritskribent i över femton år, och när jag fick möjlighet att anlita henne som skrivcoach nappade jag förstås direkt. Jag blev inte besviken. Lisa har en fantastisk känsla för språk, stil och rytm, men också förmågan att förmedla denna kunskap på ett ärligt, tydligt och ofta roligt sätt, och det kändes som att hon var verkligt intresserad av att hjälpa mig. Mina texter blev oerhört mycket bättre efter Lisas granskning och jag kan varmt rekommendera henne som skrivcoach."  
> _– Anna Söderqvist
>
> _”Lisa Förare Winbladh har ett vältränat öga både för detaljer och för helheten i en text. Hon läser strängt men är samtidigt en påhejare av rang eftersom hon har förmågan att se möjligheter och utvecklingspotential i ett ofärdigt manus.”  
> _– Maria Francke, journalist och författare
>
> _Magisk feedback ju! Har inte kollat riktigt allt än men det här är verkligen det bästa jag hade kunnat hoppas på. Konstruktivt, smart och inspirerande på ett sätt som gör att jag verkligen får lust att dona med detta, trots att jag nyligen var rätt trött på manuset och kände att tanken på att pyssla med det tog emot. Tycker att du har riktigt bra poänger. Och ser för övrigt mycket lite jag inte håller med om. DU ÄR BÄST!
>
> _– Olle Söderström, författare_
>
> "Hon är den svenska matjournalistikens Mike Tyson. Oslagbar."  
> _– Bo Madestrand,_ [_DN_](https://www.dn.se/pa-stan/ata-ute/dom-kallar-oss-foodies/ "Dom kallar oss foodies")

SMAKPROV PÅ MITT EGET SKRIVANDE:  
Man behöver inte alls vara hyllad eller ens publicerade författare för att vara en bra skrivcoach. Men det ger ändå viss garanti för att hen vet vad hen sysslar med.  
[Prövningen, ](https://tidningenskriva.se/tavlingar-och-event/tavlingar/skrivas-stora-hosttavling-2018/provningen/?fbclid=IwAR0DX8Rj8BYlLCr3YSU-_hI-OYvyKpFb2fxtn1wWO8Cf1jGzUwgx3l1jUvg "Inte jätteläskigt!")vinnare i Tidningen Skrivas stora novelltävling.  
Första kapitlet av min debutroman Varken.  
Recensioner i bland annat Aftonbladet, VLT och DN

Så här går det till:  
1\. Du skickar mig ditt manus – i vilket utvecklingsstadium du vill: larv, puppa, imago, fjäril, stendöd, förruttnad … Fackbok eller roman. Efter att ha skummat lite i det avgör jag om just jag är rätt person att hjälpa just dig. Texten måste ha potential för utgivning på ett traditionellt förlag.  Om du skriver artiklar kan du skicka ett par av dem i stället. Både sådana du vill tapetsera stan med och sådana som får skammens rodnad att blossa på kinderna.

2\. Vi kommer överens om samarbete och vad du ska få hjälp med. Sätter deadline. Och bestämmer priset efter min prislista. Du antyder att du vill ha en liten rabatt, det kan hända att du får det eftersom du till exempel är student. Eller pank, så att du måste betala senare. Om du smickrar mig tillräckligt kan det gå vägen.

3\. Jag skickar en beställningsbekräftelse, du skickar manus i Word.

4\. Du får utförlig, pedagogisk och rolig respons i den form du beställt. Inte bara på texten utan på hur du kan utvecklas som skribent. Lite som en riktigt lång privatlektion. Och alltid en skopa extra på köpet, för att jag har så svårt att hejda mig.

5\. Du är nöjd. Jag mejlar en faktura. Du betalar. Om du av någon konstig anledning inte är helt nöjd betalar du inget alls.

6\.  Kanske kommer vi överens om en uppföljning.

**PRISLISTA:**

FÖRFATTAROMDÖME  
• Genomläsning av manus cirka 300 sidor, en del kommentarer i texten.  
• En analys på minst 2 000 ord (10 000 tecken) med personlig respons. Jag fokuserar främst på språk, metaforer, gestaltning, idéer, detaljer, psykologi och fantasi. Men givetvis levereras även en del guldkorn om handlingen och dramaturgin. Utförliga och pedagogiska förklaringar till varför du ska göra på ett visst sätt. Tips på omarbetning för att möjliggöra utgivning.  
Dessutom:  
• En handfull utvalda färdiga men opublicerade texter om skrivande i allmänhet som jag skrivit. Knep och knåp. Stilistik.  
• Några roliga skrivövningar som du kan göra på egen hand.  
• Tips på romaner som kan inspirera dig.  
• Länkar till artiklar på nätet du kan ha nytta av.

• Svar på dina frågor, så klart!  
PAKETPRIS: 5 000:- + moms

DJUPANALYS

Ett oslagbart sätt att identifiera problem, förbättra självkänslan och   Jag sysslar inte med korrläsning och grammatikrättning, du får hjälp att hitta din egen röst, utrota klyschor och blåsa liv i håglösa fraser.  
• Vi borrar ner oss i kulturlagren! Djupläsning av en lång novell eller ett par kapitel, minst 4 000 ord (cirka 20 000 tkn). Rikligt med löpande kommentarer i dokumentet.

• Initierad text med personlig respons om dina styrkor och utvecklingsområden.

• En handfull utvalda färdiga texter om skrivande i allmänhet. Knep och knåp. Stilistik.

• Svar på alla dina frågor så klart!

PAKETPRIS: 3 000 + moms

STORA COACHPAKETET

• Textomdöme (se ovan)

• Stilanalys (se ovan)  
• 45 minuter Skype- eller telefonsamtal. Eller en timmes chatt.  
• Uppföljande chatt i senare skede enligt behov. Omläsning av problemkapitel inom rimliga gränser.

MEGAPAKETPRIS: 8 000:- + moms

**PREMIÄRPRIS FÖR DE FEM FÖRSTA ADEPTERNA!  
5 500:- + moms för hela klabbet. Skynda att fynda!**