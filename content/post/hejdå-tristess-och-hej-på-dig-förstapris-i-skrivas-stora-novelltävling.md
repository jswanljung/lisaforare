+++
avsandare = ""
bilder = []
date = "2019-02-05T10:13:12+01:00"
inlaggsbild = "/Skärmavbild 2019-02-05 kl. 09.57.09.png"
title = "Hejdå tristess! Och hej på dig förstapris i Skrivas stora novelltävling …"

+++
![](/bilder/Skärmavbild 2019-02-05 kl. 09.57.09.png)Min vanligaste känsla när jag skriver roman är: ”Oj, vad skojigt!” Min näst vanligaste? ”Ingen kommer att vilja läsa den här smörjan.” De lyckliga timmar allt flyter hinner jag inte tänka just någonting. <!--more-->

Mitt skrivande drivs väldigt lite av kreativ ångest, mer av en gnagande oro för att tråka ut folk. Att stjäla andras tid genom att ta den i anspråk med sina bokstäver är ett tungt ansvar. Jag är nämligen själv allergisk mot att ha tråkigt, vilket beror på medfödda koncentrationsvårigheter som diagnostiserats som en mild ADD. Att tänka på fem saker samtidigt är mitt normala tillstånd. Därför präglas mitt skrivande av krusiduller, metaforer, associationer, parallellismer och alliterationer. Kort sagt: extra allt. Det finns oändligt många sätt att skriva, det här råkar vara mitt. Det gäller bara att göra det tillräckligt bra.

För oss med bristande fokuseringsförmåga, vek startmotor och noll disciplin är det väldigt svårt att sätta sig ner och skriva. Oavsett hur mycket vi älskar det när vi väl kommit igång. Deadlines och tydliga ramar underlättar. Att bryta mot regler – oskrivna eller präntade med blod – ger också en kick.

Men allra bäst skriver jag när jag egentligen borde skriva något helt annat. Som en dag i december då jag verkligen måste få ur mig en krönika till IcaKuriren. Eftersom prokrastinering är min favoritsysselsättning bläddrade jag i den utmärkta [tidningen Skriva](https://tidningenskriva.se "Bra tidning med god smak"). Oj, en novelltävling med tema RESPEKT! Att skriva en novell tycktes i det ögonblicket oändligt mycket mer lockande än att skriva en krönika. Det tog inte många timmar innan jag hade ett färdigt utkast på de tiotusen tecken som krävdes. Dagen efter skickade jag in novellen och gjorde de sista redigeringarna direkt i formuläret på Skrivas hemsida.

Jag vann!

Tiotusen kronor och ett frikort till alla seminarier på bokmässan – värt över tretusen kronor. Finemang för en blivande författare med blygsamma inkomster. Eftersom novellen utspelar sig i samma universum som min roman var det dessutom lite av ett kvitto på att jag inte är helt ute och cyklar.

Krönikan då? Den skrev jag i stället för att städa två dagar senare. Plötsligt var den texten hur kul som helst att komponera. Att dammråttorna fortfarande frodas under soffan kan inte hjälpas.

**LISAS SKRIVTIPS**

Bejaka kreativ prokrastinering. Pusha dig själv att skriva genom att tänka på tråkigare saker du borde göra.