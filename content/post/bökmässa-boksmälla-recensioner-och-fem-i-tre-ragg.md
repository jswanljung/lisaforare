+++
avsandare = ""
bilder = []
date = "2019-09-27T00:00:00+02:00"
inlaggsbild = ""
title = "Bökmässa, boksmälla, recensioner och fem-i-tre-ragg"

+++
<figure> <img src="/bilder/71278864_10158819003147678_2007907471556870144_n.jpg

" alt="_" width="494"> <figcaption>Writers for future </figcaption> </figure>

Att skriva en roman är en av de mest klimatsmarta strapatser du kan ge dig ut på. Lite googlande, några utskrifter – utsläppen är minimala. Och för merparten av alla författare är inkomsterna så låga att ett spartanskt liv är nödvändigt. Och efter att ha plitat på bokfan i ett par år är du så glåmig, trist och trind att risken att du bidrar till överbefolkningen försumbar. <!--more-->Så till vida du inte far till bokmässan och sammanstrålar med likasinnade och fördunklar ditt eventuella omdöme med ljummet lådvin.

Bokmässan är himmel och helvete i samma svettiga paket. En myriad författare går med samma tankar i skallen: Varför pratar inte folk mer om min bok. Varför förstår ingen mitt geni? Var är vinet?

Lyckligtvis fick jag i morse ett uppmuntrande meddelandet att en mycket vis man ansåg min roman vara ett mästerverk och … Vem det var? Det spelar väl ingen roll. Kräver du att få veta? Okejrå. Min pappa. En helt opartisk bedömning, givetvis. <br>Nå, hittills har Varken fått ett väldigt fint mottagande.<br> En stark fyra och en femma från bokbloggare. <br>[Entusiastiska twittrare](https://twitter.com/isobelsverkstad/status/1176756837372354560). [Euforiska twittrare.](https://twitter.com/MichisLeo/status/1168904118628495371) Den[ första ](https://twitter.com/Terejs/status/1168267286035275776?fbclid=IwAR3JlXW9vNKsOjPwzLHEN_A_NS7QfRsO6lsObfY-qDfilo3iUCESvy9M_OI)som twittrade fick mig nästan att bör gråta. <br>En [galet positiv recension ](https://www.vlt.se/artikel/bland-maror-och-tankelasare-i-fororten-sprittande-romandebut-ger-mersmak)från DT:s kulturredaktör, Anders K Gustafsson, som gått i många av Mittmedias tidningar.

_"Men Lisa Förare faller inte i några klichéer. ”Varken” är en fantastisk bok, som spritter av berättarglädje och kreativ uppfinningsrikedom. \[...\] Perspektiven skiftar och karaktärerna är fint skildrade  \[...\] Svensk fantastik mår uppenbarligen mycket bra just nu."_

Extra glad blev jag när jag upptäckte att geniet Johanna Frid sagt fina saker om boken i [dansk media](https://www.midtjyllandsavis.dk/artikel/10eaf6ff-f581-45db-8ef2-b927bc095467/).<br>_"Lige nu læser jeg spændings/skræk/myteromanen »**Varken**« af min kloge veninde **Lisa Förare** - pragtfuld."_

Man kan få högfärdshosta för mindre.  
Första kvällen bjöds jag på flådig middag av omslagsmakaren Niklas Lindblad på Mystical Garden. Han och hans fotograffru är inte bara ruskigt begåvande och trevliga, Niklas var dessutom min klasskamrat i nio år. Tyvärr krävdes det ansenliga mängder vin för att dränka en del barndomsminnen, så idag var det uppförslut, så att säga.

I år erbjöds faktiskt en respit från egotrippen i form av det fina initiativet Writers For Future. Vi var ett gäng som stod utanför mässan med små gröna skyltar för att visa vårt stöd för ungdomsprotesten världen över. nästan bara kvinnor – och vilka kvinnor! Varma roliga och beslutsamma.

Tidigare bokmässor har jag mest hållit till i matdelen, nu är jag nykläckt romanförfattare. Och jag är överväldigad av hur snabbt jag fått nya generösa författarvänner. Elisabeth Östnäs, som jag bara beundrat på håll. Malin Steen, Frida Andersson Johansson och flera finfina förlagskollegor. Hela gänget i Vagina Dentata med eminenta skräckförfattaren [Madeleine Bäck ](https://www.instagram.com/p/B2Gf8Y6iRhC/?fbclid=IwAR0tvXuXKqUmvBCFvvD2qgDy3M1QC6-XNplaABvhFELvpN7n6xhaaretQew)i spetsen.

Närmaste året kommer jag att vara författare på halvtid och matmänniska på halvtid. Det blir knapert, men måste gå! För om ett år kommer del två i Långsjö-serien och jag har redan klarat av första mötet med min nya redaktör Lena Sanfridsson, som verkar vara en riktig klippa. Amanda Setterwall som gjorde ett fantastiskt jobb med Varken kommer att vara med på ett litet hörn men hon ska skriva sin master i litteraturvetenskap. <br>Jag ska snart skriva mer om samarbete med redaktör, förläggare och testläsare. En roman är till viss del ett grupparbete.

Jag har två framträdanden på bokmässan, de ligger sist på SÖNDAGEN. Jag är alltså mässans motsvarighet till fem-i-tre-ragg. Kom gärna dit!

15:00-15:20 Pratar jag med bl a Johannes Pinter på Crime Time om skillnaden mellan spänning och skräck  
16:00-16:20 Står jag på Fantastikscenen och tjoar om den livsfarliga kanelbullen och hur det stora hemska kan gömma sig i det lilla vardagliga.