+++
avsandare = ""
bilder = []
date = "2019-10-21T00:00:00+02:00"
draft = true
inlaggsbild = ""
title = "Skrivtips #1 MILJÖER: Atmosfär eller på-näsan-skrivande?"

+++
> > Det finns skrivtips som är så snofsigt formulerade att en liten låga flammar upp i mitt bröst. Jag vill rama in dem, tonsätta dem, rulla dem i pärlsocker och sluka dem.  Ändå visar de sig vara värdelösa när jag väl satt mig vid datorn. De kan mycket väl funka för dig, men här presenterar jag bara råd som förändrat mitt skrivande.  
> >   
> > Först ut är Hillary Mantel. Jag låter henne säga det själv, av naturliga skäl.   
> >   
> > "Description must work for its place. It can't be simply ornamental. It ­usually works best if it has a human element; it is more effective if it comes from an implied viewpoint, rather than from the eye of God. If description is coloured by the viewpoint of the character who is doing the noticing, it becomes, in effect, part of character definition and part of the action."

En vanlig sak, jag vill inte säga fel, är att författare går över till sin egen röst och rent av sitt eget perspektiv när de beskriver ett rum, ett landskap eller en tunnelbanestation. Det funkar helt okej ibland. Men ofta blir det en infodump i stel och obekväm språkdräkt. Om du i stället filtrerar informationen och färgar den genom perspektivhållarens begär, behov och språk blir varje beskrivning en del av gestaltningen.   
  
En bra början är att tänka på vad PH lägger märke till och vilka känslor det väcker. En ornitolog, en häxa och ett barn kan gå ut samma skog och komma hem med tre mycket olika beskrivningar av den. PHs  tillfälliga sinnesstämning och syftet med promenaden påverkar också.  
  
Det svåraste med beskrivningar är inte att beskriva. Det är att låta bli. Om du verkligen vill att din läsare ska bli intim med din PH är det viktigt vad du utesluter. Det gäller särskilt när HP kommer in i sitt hem, eller utför sysslor som är en rutin. En person tänker nämligen inte särskilt mycket på det vanliga och vardagliga. Beskriver du i detalj det som PH tar för givet tar du två steg bakåt och berättar eller rent vi instruerar. Om du gör det snyggt, kort och sällan är det inget problem. Men du måste vara medveten om priset du betalar i distans. Du kan förstås vara ute efter distans, men i min fantastik är det otroligt viktigt att PH och läsaren bondar. Jag kallar mitt sätt att skriva för intimt perspektiv, ett uttryck som jag säkert snott av någon. Har jag sagt att författare är skator?  
  
 Nej, det vi naturligt lägger märke till är nya miljöer och avvikelser. Du kan lösa det genom att låta den PH som är obekant med en miljö beskriva den.  Det funkar förstås bara om du har flera PH. Ett annat knep är att låta PH beskriva eller prata om en miljö får någon som inte sett den. En guidad tur av en oerfaren stadsmänniska är förstås en tänkbar lösning om du vill beskriva en skog. Men det är lätt att du förvandlas till en serieinfodumpare när du lägger informativa repliker i en kunnig PHs mun. Om enda syftet med en dialog är att ge kunskap blir det sällan en medryckande passage i din text. Att tråka ut läsaren är som bekant en dödssynd. Lite hjälper det att göra dina samtalspartners arga, avundsjuka eller kåta på varandra –känslor som kan ge intressanta underströmmar i dialogen. Vad säger de mellan raderna?   
  
 I min roman varken låter jag Mellansyster skryta om sin garnbutik i ett brev. Ett av skälen till att Lillasyster får ordet i bok två är att hon ska få beskriva tvinnarnas liv och butik i ett nytt perspektiv, vilket förhoppningsvis engagerar dem som läst del ett samtidigt som helt nya läsare får nödvändig information.  

Men det bästa knepet är att skildra miljöer genom att skriva vad som avviker från det förväntade. 