+++
date = "2018-09-28T07:38:00+00:00"
title = "Vem är Lisa Förare?"

+++
**Vem är Lisa Förare och hur skiljer hon sig från Lisa Förare Winbladh?** Först en kortversion: Lisa Förare Winbladh är matskribent sen tjugofem år och matälskare sen mor fick Nils. Lisa Förare är författare sen nyss.

Lisa föddes 1966 och växte upp i en excentrisk och mycket kärleksfull familj i Äppelviken. Eftersom mamma var konstnär och pappa forskare drömde hon alltid om maten de inte riktigt hade råd med. I sjuårsåldern gjorde hon en tidning om mat och skräck som kallades Husmorden. Framtidsdrömmar? Hon ville bli mikrobiolog som Margareta, hennes fasters coola flickvän. Gymnasievalet var självklart: fyraårig teknisk med biokemisk inriktning.

Efter sommarpraktik på labb fick hon till sin stora överraskning erbjudande om anställning som assistent.

”Åh, ni är alltså nöjda med mig?”

”Du är den sämsta lab-ass vi haft men du är rolig och bakar jättegoda kakor.”

Lisa är nämligen patologiskt förvirrad och rätt slarvig.

Hon gav därför upp mikrobiologin, läste idéhistoria ett år började laga mat på allvar eftersom tillvaron i köket handlar om nävar och nypor snarare än mikrogram. Efter en dietistutbildning och diverse irrturer i krogvärlden insåg hon, vid knappt trettio års ålder, att det var ännu roligare att skriva om mat än att laga den.

Sen följde en lång rad roliga uppdrag och hyss. Redaktör på Gourmet, [matspanare på DN](https://www.dn.se/arkiv/pa-stan/kockarnas-kemilada/ "Artikel om kemikalier i maten"), krönikör [på SvD](https://www.svd.se/av/lisa-forare-winbladh "Ett helt skafferi med krönikor"), fast matskribent på Sydsvenskan, sommarvärdskap på P1 och mycket annat. Hon skrev kokböcker, nördade ner sig i [matkemi](http://taffel.se/matmolekyler "Matmolekyler med Malin") och vann några fina priser. Hon har bott i Uppsala, Lund och Malmö. I Malmö startade hon succén [Matkaravan](http://matkaravan.se "Min bebis!") som hon drev i drygt två år. Tillsammans med sin man, Johan Swanljung, startade hon matsajten [Taffel.se](https://taffel.se "Törnrosa-sajt"). Just nu är Lisa krönikör på [Allt om mat](https://alltommat.se/kronika-lisa-forare-winblad-drommar-om-praktiga-skordar/ "Inte direkt gröna fingrar, nej.") och [Ica Kuriren.](https://www.hemtrevligt.se/icakuriren/tagg/lisa-forare-winbladh "Läs mina krönikor!")

För några år sedan, efter att hon [lite oväntat fått en son ](https://taffel.se/blog/tio-fingrar-tio-tar "Moderslycka!")och varit mammaledig, kom Lisa in i en skrivsvacka. När man är inne på sin tionde krönika om koriander börjar orden kännas slitna. En kurs i att [skriva skräck på Folkuniversitetet](http://www.folkuniversitetet.se/Skolor/skrivarakademin/Skrivarakademin-i-Stockholm/larare/Anders-Fager/ "Gå kurs du också!"), piggar alltid upp, tänkte hon. Utan andra planer än att ha lite rajraj en termin anmälde hon sig. En tid efter kursen fick hon förfrågan om att skriva en novell som skulle användas för att göra reklam för kursen. Den första meningen måste vara: ”Jenny hänger i persiennsnöret.”  Novellen kom att bli första byggstenen i den värld som ryms i romanen Varken.

Varken är en vindlande mörk förortssaga som utspelar sig i Lisas hemtrakter: Fruängen, Långbro och området runt [Långsjön](https://sv.wikipedia.org/wiki/Långsjön,_Älvsjö "Vissa tror att sjön bara är tre meter djup …"). Valet av miljö beror främst på att Lisa är notoriskt lat och praktiskt taget saknar lokalsinne. Romanen kommer ut hösten 2019 på [Louise Bäckelin Förlag](http://lbforlag.se).

Du kan anlita Lisa Förare för:

* Författarföredrag och föreläsningar om skrivande
* Skrivcoachning och workshopar
* Noveller eller artiklar om fantastik, skräck och skrivande

Du kan anlita Lisa Förare Winbladh för:

* Texter om mat till kokböcker, tidningar och broschyrer
* Matlagningskurser inriktade på asiatiska smaker, fegvego eller vegetariskt
* Smakspektakel: shower och föreläsningar om mattrender, matens kemi, hur du trollar med grundsmakerna och konsten att kombinera ingredienser

  Kontakt: forare/på/gmail/punkt/com