+++
avsandare = "Fredric Dahlgren"
date = "2019-01-27"
layout = "single"
title = "Fotograf Fredric Dahlgren"

+++
Jag fick min första kamera för snart 48 år sedan på min 10:e födelsedag. En svart skönhet vid namn Kodak Instamatic. Nu många år senare är hon fortfarande ett kärt minne och min första bekantskap med bilden som uttrycksmedel. Sedan dess har en mängd analoga och digitala kameror passerat genom mina händer.

Min största inspiration är det lilla, det till synes obetydliga som är så lätt att missa. Jag vill visa, och ibland även transformera det till något helt annat, mer abstrakt. Motivet är inte längre det viktiga – utan den känsla bilden kan väcka hos betraktaren.

Det finns så mycket att se alldeles helt nära inpå. Det fantastiska lilla finns överallt ibland oss.

Fredric Dahlgren

Västergård Södertälje i oktober 2018  
  
[Hemsida hittar du här!](https://www.fdahlgrenphotography.com/ "Bilder, bilder, bilder!")