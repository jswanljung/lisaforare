+++
avsandare = ""
bilder = []
date = ""
draft = true
inlaggsbild = ""
title = "Jakten"

+++

Pigeon
			Children’s Song
			(From Arabic)
			I’m a pigeon
			I ran and think,
			My dad cauterized me
			My mom grilled me
			My dear sister
			Collected my bones
			And a dog beside me
			Licking my blood!
            

Ljud likt flaxande vingar, duvor som tar till flykt. Nakna fotsulor smattrar över den regnglänsande asfalten. Ett felsteg, en ojämnhet i marken och det är slut. Marius försöker att inte tänka – inte på fall, inte på något. Andas. Inte för snabbt. Långa, jämna tag. Måste vara lätt, sväva. Genom mörker, genom ljuskäglor från enstaka ljusstolpar. Mot containrarna, bort från den tomma parkeringens öppna yta. Natten är augustihet, nästan stjärnklar och för en gångs skull regnar det inte. Det är de nätterna som är farligast.
Marius vet inte exakt hur många barn som försvunnit senaste året, bara att de är många. Några av dem hade varit hans vänner: Jacob, Ricardo, Weng med den långa flätan. Hade de också sprungit längs de övergivna kajerna? Tyst, hopplöst, för sina liv. Utan någon att ropa till. Till sist hade de tagit Kheira. Hans syster.
* * *
Springa kan de, svartingarna. Det måste man ge dem. Till och med tjockisen hade gett honom en match. Men vad kan de mer? Äta, skita överallt, föröka sig som råttor, tigga vid tunnelbanan. Ohyra.
Jacke vet: Kommandot gör bara det alla svennar vill göra men inte vågar. Håller rent. Ingen saknar några papperslösa. Tjugotre har Kommandot tagit ner bara i år. Och hur många har anmälts? Två kanske. Ingen bryr sig.
Jacke tar ut stegen. De nya kängorna blänker av fiskarsmörja. Skavsår i morgon men så värt. Killen framför hoppar upp på en blå sandlåda. Han är så mager att det knappt hörs. Bara en lätt ihålig duns innan han svingar sig vidare över det låga planket till containerområdet.
Javisst, hoppa kan de också, de jävla apekatterna.
* * *
Kheira! Hon hade sagt att de alltid skulle vara tillsammans. Alltid. Ändå övergav hon Marius. Hon som alla andra. Lät sig tas. Hon hade börjat hosta, hostade i flera veckor. Blev långsam. Man får inte bli långsam.
Först när Marius är nästan framme vid de smutsgula containrarna hör han de mumlande rösterna. De är fler. De skalliga försöker lura honom in i en fälla. Maury ändrar riktning, förbi containrarna och upp mot sopstationen. Sötsakerna skaver ovant i hans mage. Sockrigt brännande stiger upp i halsen, svider, tvingar honom att svälja hårt. Inte tappa andan. Kheira! Han borde ha lyssnat på henne: Inte prata med vuxna, inte ens när man är hungrig. I synnerhet inte då. Man glömmer sig, blir oförsiktig. Ena foten framför den andra. Benen har börjat värka nu. Stumnar. Ingen idé att skrika på hjälp, varje andetag behövs. Även om någon hört hade de inte brytt sig. De hade tittat ner, bort, överallt utom på honom. De är så svenskarna. Undanglidande. Bleka av regn. Svullna av kött, smör och sött vitt bröd. De ser inte sådana som Marius. Bara De skalliga ser.
* * *
Adrenalinet har Jacke i sitt grepp. Kramar hans inälvor, får hjärtat att dunka så att revbenen skälver. Jacke tar inte cola eller e längre. Dricker inte ens öl. Bara lite röka dagen efter för att varva ner. Inget kan ändå mäta sig mot det här. Jakten. Isen i bröstet. Hettan i skrevet.
Jacke är egentligen för tung för att jaga. Om det inte varit för begäret hade han inte klarat att springa så här snabbt, så länge. Öka när det inte längre borde finnas något kvar. Och prickskyttet ligger inte för honom. Han provade en kväll, men Ungen sprang – pow! –ungen låg i gruset. Det hade inte gett mycket mer än en kväll med x-boxen. Jacke vill se dem först. Nära. Locka fram dem. Godis, frukt, mackor. Ögonen är alltid mörka, alltid blanka av hunger i ansikten gråa av smuts och skräck. Och sen det speciella ögonblick när förhoppningen slår om till rädsla när han långsamt tar av sig mössan. Ögonblicket när jakten börjar. Jacke gillar fair play, ger dem alltid ett ordentligt försprång.
Ungen börjar sacka nu. Jacke slår av på takten lite, som om han också vore trött. Låter svartingen hoppas. Drar ut på det. Upphetsningen får blodet att svirra och susa i tinningarna. Viskar att Jacke är kung, att den som har en kniv i hamnen är kung.
* * *
Tårar, svett och snor rinner nerför Marius ansikte. Det hugger i sidan för varje andetag. Stanken känns allt starkare. Han hatar att andas in den men han måste för att få krafter. Snart är han framme vid platsen dit han släpade Kheira när de skjutit henne. I ljuset av den ensamma strålkastaren vid ingången till soptippen. Stället där hon viskade hans namn medan hennes blod flöt mörkt över gruset. Ingen har sagt hans namn sen dess. Hon hade bett honom att springa därifrån, gömma sig. Till slut hade hon blivit arg, nästan skrikit på honom med en röst som bröts. Inte förrän han hörde de skalligas steg och mörka skratt hade han sprungit. När han vågat sig fram i det tidiga morgonljuset var hon borta. Bara den mörka klibbiga fläcken var kvar. Nu är den bortregnad sen länge. Inget syns i ljuset mer än skräp och grus. Och stängslet som tornar sig upp högt framför honom. Det finns ingenstans att ta vägen. Varför hade han inte sprungit mot containrarna i stället? Kheira, var är du?
* * *
Idiotungen springer rakt mot stängslet. Vinglar, har svårt att hålla kursen. Inte mycket kvar nu. Dags att avsluta. Jacke trevar efter kniven vid sidan. Snart. Månen har gått i moln.
Shit! Ungen klättrar upp på stängslet, trasslar sig på något vis över taggtråden på krönet och tumlar ner i mörkret med ett kvidande. Snacka om apa!
Besvikelsen dånar i Jacke. Han sprintar, kängar rakt in i stängslet som skälver. En gång till – allt ursinne i en enda spark. Och porten, den som alltid är låst, glider upp ett par decimeter. På marken bredvid ligger övervakningskameran som en nerskjuten fågel. Bara att öppna och promenera in. Jacke Olsson har tur i natt.
* * *
Marius tumlar runt, lyckas ta sig på fötter, men vänsterbenet viker sig. Han faller nästan. Det rinner vått och varmt nerför axeln nerför armen. Linkar vidare. Måste bara in i mörkret, mörkret som skyddar. Vila. När han hör det vill han inte tro det: porten, den som borde vara låst, svängs öppen med ett gällt gnisslande, de tunga springande stegen. Molnen lättar och månens sken lyser upp bergen av sopor. Det finns ingenstans att gömma sig i närheten. Om han bara lägger sig ner nu och blundar kan han vila en sekund. En sekund kan vara värd en evighet innan man dör. Då ser han henne.
* * *
Jacke försöker andas genom munnen men stanken tar sig upp i näsan bakvägen. Vilket jävla ställe! Det var hit de tog ungarna. Städpatrullen. Nybörjarna som fick göra skitjobbet när de äldre i Kommandot gjort sitt. Passande kyrkogård åt apejävlarna. Skit ska skit ha. Ett svagt rasslande far genom sopbergen. Regnar det igen? Nej. Måste vara vinden. Men ljuden är rytmiska som … andetag? Marken rör sig under hans fötter, böljande. Luften, tät av stank, skälver av pustarna. När Jacke får syn på flickan glömmer han allt annat. Månen har brutit fram ur molnen och han ser henne tydligt. Hon står helt stilla mellan två sopberg. Konstiga kläder, ser ut som om hon rullat sig i soporna. Ansiktet nerböjt, i skugga. Killen är nästan framme vid henne, kravlar nästan de sista metrarna. För bra för att vara sant. Ta två, betala för en. Var fan kom hon från?
* * *
Kheira. Marius har inte ork kvar att ropa hennes namn, men han vet att det når henne ändå. Hon breder ut armarna, sveper upp honom i sin famn som när han var liten. Så springer de. Tillsammans nu. De är ett och hon viskar hans namn om och om igen. Hennes hår kittlar hans ansikte, det är strävare än han minns, som visset gräs. Foten gör inte ont, ingenting gör ont längre och hon skrattar när hon lyfter honom uppåt. Rör han ens vid marken?
* * *
Jacke tar fram kniven. Fumlar. Trampar på något halt, kanske rutten frukt, faller på knä men hejdar fallet med vänsterhanden. Fuck! Knogarna svider. Kängorna, de alldeles nya och svindyra, kommer bli ett helvete att få rena. Upp igen! Han klarar dem, hur lätt som helst … Ungarna är nästan uppe på en av sophögarna nu. Hur fan kom de upp? Så vänder sig flickan om. Ser ner på honom medan hon klättrar. Och han ser att hon är äldre än han först trott. Nästan vuxen trots den späda kroppen. Håret lyser ljust, nästan vitt i lampornas sken. Inte förrän nu ser han att tjejen inte är mörk utan ljusare än han själv. En albino? En av de ryska hororna? Hennes ögon är stora, för stora, och vitorna glimmar till i mörkret. Jacke kollar uppåt, bakåt. Men där finns ingen ljuskälla utom den urvattnat bleka månen. Tjejens ögon lyser nu. Vitblått.

Och när han ofrivilligt håller andan hör han dem för första gången. Ljuden. Steg. Hastiga. Inte av fötter, något annat. Ett dött fuktigt klattrande. Ljuden av många små som är en enda jättelik. Snabb blick bakåt: Skuggliknande gestalter sammansatta av tomma mjölkkartonger, trasor, blöjor, ruttnade matrester, skit och hat. Han kan känna deras hat, det som håller samman dem och glöder vitt i ögonen. Jackes kropp vet innan han förstår. Knäna mjuknar, blåsan töms och pisset sprider sig varmt längs låren. Kniven faller ur handen. På något sätt får han krafter att springa. Klumpigt, utan mål bara bort och vidare medan de som är en enda hörs allt tydligare. Ljuden tycks komma från överallt, omringar honom: torra gläfsanden, flämtningar uppblandade med klara ljusa skratt. Barn som leker.
