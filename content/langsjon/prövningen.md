+++
avsandare = ""
bilder = []
date = "2019-07-30T00:00:00+02:00"
inlaggsbild = "/51405189_10158124607837678_1468476887897473024_n.jpg"
title = "Prövningen"

+++
”Det kommer att göra lite ont”, säger Hanen. Han säger det varje gång. Som om jag inte redan visste. Först sekatören som är slipad till rakbladsskärpa. Sen skalpellen för att putsa. Det blöder inte särskilt mycket. <br> ”Jag är rädd”, säger jag. Mycket tyst så att inte Hanen ska höra.

När det är färdigt gnuggar han sin kind mot min. Vi möter varandras blick i spegeln. Präglingen är så stark att vi känns som en enda. Ett enda.<!--more--> <br> ”Plåster eller hatten?” frågar Hanen. <br> ”Det räcker med basker. Om jag drar ner den.” <br> Jag hatar att ha saker på huvudet. Varenda sekund jag har baskern på vill jag rista med huvudet, kasta den av mig med en frustning.

”Du kanske ska ta med dem till apoteket den här gången”, säger Hanen. ”Lite extra pengar skadar inte. Elräkningen var helt galen förra månaden.”

Jag nickar med en klump i halsen. Hanen plockar upp de avklippta hornstumparna från golvet och lägger i en ziplockpåse. När han går till köket för att lägga påsen i kylen plockar jag upp en av de största flisorna från golvet och stoppar i förklädesfickan.<br>  Bara keratin, inte mer magiskt än naglar. Men apotekare Ask, min chef, har fått för sig att horn ökar potensen. Oavsett vilket betalar de japanska internetkunderna fantasisummor för hornpiller. Näthandeln går strålande.

”Du är alltid vackrast, med eller utan”, säger Hanen.<br> Det är sådant män säger. Men Hanen menar det. Han har aldrig sett åt andra honor. En kort stund innan jag går står vi näsa mot näsa och andas in varandra. Jag samlar kraft. <br> Det är min tredje prövning, precis som innan de andra gångerna är jag övertygad om att jag kommer att dö. Men jag är en överlevare, på många sätt starkare än Hanen. Det är därför det alltid är jag som går.

Hanen drar upp baskerns kant och slickar mina sår. Hans viskning kittlar mitt öra: <br> ”Vad gör vi om du blir utvald?”<br> ”Vi klarar det. Tänk på allt vi redan klarat tillsammans.”

Det är bara en kort bit att gå. I dagsljus är det en rätt harmlös byggnad i rött tegel. I mörkret tycks den större. Hotfull. Kurande. När den tunga porten slår igen bakom mig grips jag av samma förtvivlan som alltid. Byggnaden har svalt mig, den kommer aldrig släppa ut mig igen. Bara spotta ut mina rengnagda ben med en mätt rapning. <br> Jag drömde mardrömmar i veckor efter senaste prövningen. Att den fortsatte i en evighet, att jag hölls fången i det kalla ljuset. Fastnaglad av de andras ögon. De kommer inte att välja mig, tänker jag, det finns så många andra som är bättre lämpade.

Vi är kanske tjugofem i salen. Många pratar med varandra. Jag nickar åt några jag känner igen lite. Andas genom munnen som vanligt, för om jag känner stanken av deras illvilja vet jag inte om jag förmår stanna. <br> Kvinnan som stiger fram framför oss är klädd i svart och blått. Hon är silverhårig och mycket späd, handlederna tunna som ett barns. Men när hon talar lyser hennes inre kraft genom, hennes röst är mäktig och genomträngande som en stålklinga.  <br> ”Vad roligt att så många kunde komma! Vi har haft en underbar start på vårterminen. 1 D är en härlig klass att arbeta med.”

Jag rättar till solglasögonen, men fingrarna skakar så att jag råkar knuffa dem på sned. Ljuset bländar, klöser illa och bitter vätska sipprar ner för kinderna. Snabbt gnuggar jag bort den, hoppas att ingen hann lägga märke till färgen. <br> Det är lättare på sommaren då ingen reagerar på om man har solglasögon. De andra föräldrarna tror säkert att Josefs mamma är galen. Andas. Andas lugnt med mun torr av skräck.

En mamma med blond hästsvans och mjuk ljusblå ylletröja stirrar på mig. Jag är väl förberedd:<br> ”Ursäkta, jag har ögoninflammation.”

Hennes blick mildras. Hur jag ska klara av att hitta undanflykter nästa vinter vet jag inte. Man kan nog inte ha ögoninflammation flera år i rad. Jag försöker minnas olika sjukdomar som ger ljuskänslighet men tankarna är tröga som snokar i höstkyla.

Mamman som sitter bredvid i dubbelbänken är en av de äldre. Som så många andra har hon försökt måla ett yngre ansikte på sitt gamla. Hon vänder sig mot mig, lägger sin hand på min och viskar. <br> ”Jag är ledsen för det där som hände i torsdags. Millan är ju inte som andra. Hon ska börja med medicin.”

Ett ögonblick är det som allt står stilla. Jag sniffar ytterst försiktigt i luften. Nej, det finns inte ett spår av flock i hennes lukt. Men inte något skrämmande heller. Bara oro och nederlag. <br> Så märkligt. <br> Jag andas genom munnen igen.<br> ”Medicin?” frågar jag.<br> ”Medikinet, lägsta dosen.”<br>Mamman ler, men inte med ögonen.<br> Jag blundar, bläddrar i minnet. Får fram Fass. Medikinet är mot adhd.<br> ”Jag hoppas att medicinen hjälper.”<br> ”Jag borde kanske inte fråga, men funderar ni på att få Josef utredd? Han och Millan är ju lite lika på många sätt. Humöret och så.” <br>Josef och Millan är inte ett dugg lika. Det finns stunder att jag önskar att de vore det. Att Josef vore ännu vanligare, ännu mindre som vi.<br> ”Det är okej att du frågar. Josef medicinerar redan.”<br> Vi utbyter ett leende som hon tror är samförstånd.

Jag är ensammast i världen. Längtan efter Hanen och hemmets dunkel och dofter svider i mig. Bara en kvart kvar. Andas.<br>  Fröken Karin berättar om hemläxor. Sen kommer det:
<br> ”Vi måste utse nya klassföräldrar till den här terminen. Agneta och Peter erbjöd sig självmant förra terminen.”

Mitt hjärta bankar så högt att det måste höras i hela klassrummet.<br> ”Här i skolan brukar vi göra så att vi tar första och sista efternamnet på klasslistan och sen går vi vidare i bokstavsordning”, säger Fröken Karin.

Vi borde bytt namn när vi fick höra ryktet om hur det brukade gå till. Det borde vi. Men hur ska man kunna gardera sig mot allt? Att bokstäver ska spela så stor roll. Det är illa nog att man måste lära sig dem.<br> ”Bermann?” <br> En lång, rödskäggig man reser sig.<br> ”Öberg?” <br> Mina ben skakar nu. Jag måste resa mig. Måste. <br> För Josef. <br> Jag samlar allt det mod jag har, säger till mig själv att jag är alla ängars väktare, alla småkryps beskyddare. Så jag ställer mig upp. Vacklar lite, men står kvar. <br> ”Eller har vi några frivilliga?” frågar fröken Karin.

Då händer det. Två kvinnor, ger varandra en blick. Nickar. Reser sig snabbt. Står stramt som i givakt.
<br> ”Vi vill, vi har faktiskt lite idéer om …” <br>Resten hör jag inte för blodet brusar i mina öron och hjärtat slår en lättnadens trumvirvel i bröstet, driver upp små flämtningar ur strupen. <br> Jag sjunker ner på stolen, djupandas genom näsan i en minut. Tänker på nattsvala dungar med saftig grönska, den månglittrande sjön och alla dess vänliga små ljud. Sommarstugan i Roslagen där vi alla kan springa fritt. <br> Vår lilla flock.

Darrningarna avtar och ett stort lugn kommer över mig. En klarhet. Och det är då jag känner den. Vittringen av alla de andra – en och samma men med otaliga nyanser. Jag vidgar näsborrarna och för första gången drar jag utan förbehåll in föräldramötets alla lukter. Låter dem brisera inom mig. Vått läder, kaffe, ylle, parfym, kroppars utsöndringar. Det är bara tjugofem föräldrar, ändå finns där tusen och åter tusen ängslor. <br> Med ens inser jag att det varit så vid de tidigare prövningarna också. Luften har alltid varit tät av oro så lik min egen. <br> Ett gapskratt väller upp. Pappan bredvid famlar i fickan, får fram en servett. <br> ”Prosit”, viskar han. ”Lena är också snuvig.”

Fröken Karin tackar för sig. Allt är över. Det är fullbordat. Jag har stått upp för min unge. Nu klarar jag allt. Vad som helst.<br> När jag går tycks golvet svikta under mig. Stegen blir lätta som i lövskog.

En av papporna skyndar efter mig. Han har skägg. Alla dessa skägg! Man vänder världen ryggen i mindre än ett årtionde och när man tittar in igen har varenda man över sjutton helskägg. <br> ”Olle och Josef leker jättebra på fritids, Josef kanske har berättat?” <br> Ungen har inte pratat om något annat. Olle hit, Olle dit. Olle som bygger torn av kaplastavar till månen. <br>Pappan fortsätter: <br> ”Olle vill jättegärna att Josef kommer hem och leker efter skolan. Passar det på torsdag?” <br> Jag tvekar, så klart att jag tvekar. För jag tänker på de miljoner saker som kan gå fel. Om ungen är för trött. Eller för hungrig. Tänk om han blir arg … Men han har tjatat i två veckor om att få gå hem till Olle, alla andra barn har lekdejter.<br> ”Jag har förstått att Josef är allergisk mot mycket. Vill han ta med sig egen mat som i skolan? Eller om du skickar en lista, så försöker jag få till något. Jag gillar utmaningar i köket.”<br> Pappan ler självsäkert. Jag tror inte att han skulle tycka om vår sorts utmaningar.<br> ”Jag kollar med min man om torsdag. Ge Josef lite mjölk bara, han kan få middag hemma sen.”

”Det var inte så farligt”, säger jag när jag kommer hem. <br> Den här gången ljuger jag inte. Jag nämner inget om lekdejten med Olle, det får vänta till i morgon. Hanen kommer att säga nej först. Jag kommer att framhärda. Till slut blir den av, om inte den här veckan så nästa.<br> ”Ungen har feber” säger hanen när han omfamnar mig och gnider sin näsa mot min. ”Tror du att …”<br> ”Det är för tidigt”, säger jag. ”Han är bara åtta.”<br> Ändå sjunker mitt hjärta, för visst har vår unges lukt fått en ny skiftning senaste dagarna. Av svart lerjord och mogna äpplen.

Jag sätter mig på huk vid min sons rede. Plockar barr och boss från hans hår. Så sätter jag tungan mot hans panna, han är verkligen het. Inte bara förkylningsfeber. Skälvande söker jag med tungspetsen mot hans tinningar. Känner de små knopparna och ögonen flödar över. <br> Vi trodde, vi hade hoppats … Nästan inga får dem längre. Hanens syns knappt. Ungen brås på mig.<br> Hur många honor innan mig har suttit så här och gråtit med tungan mot sin unges panna? I vissheten om att allt kommer att bli så mycket svårare. Jag tänker att orostårarna inte bara är mina egna, att vi mammor lånar dem av varandra.

Min son rör sig i sömnen. Känner min närhet och mumlar ett ”mammis” innan han glider tillbaka ner i trög söt sömn. Snabbt lägger jag min kind mot ungens brännande och förnimmer hans drömmar: stilla gryning, daggigt gräs under trampdynorna och feta svartglänsande insekter.

Jag behöver inte säga något till Hanen när jag kommer ut i köket. Han ser mitt ansikte och vet.<br> ”Man tassar på”, säger han. ”Man tassar på en dag i taget. Det är allt man kan göra.”

\-

\-

\-

\** GILLADE DU HISTORIEN? Dela den till alla du gillar. <br> INTE? Dela den till alla du ogillar, så slösar de sin dyrbara tid på goja.

HÄR FINNS [ALLA BERÄTTELSER FRÅN LÅNGSJÖN](https://lisaforare.se/langsjon/ "KLABBET!").

Min roman Varken (LB Förlag) kommer ut 26 augusti 2019. Den utspelar sig i samma universum som novellen. [Du kan förbeställa eller beställa här.](https://www.bokus.com/bok/9789177990864/varken/)

VILL DU LÄSA FLER BERÄTTELSER HÄR? SPONSRA MITT SKRIVSNUS – OCH ETT LEGOBYGGE TILL MIN SON SÅ ATT JAG FÅR SKRIVRO!  
0709-733 331 **