+++
avsandare = ""
bilder = []
date = "2019-09-13T00:00:00+02:00"
inlaggsbild = ""
title = "Älsklingsleken"

+++
<figure> <img src="/bilder/71010736_1064375740432358_7834327334986448896_n.jpg" alt="löv" width="494">  </figure>

Visst ser vi änglalika ut? Bilden är från Husmodern, förmodligen runt 1970. Ungefär tre minuter efter att den togs bet jag den långhåriga tjejen för att hon hade snyggare strumpbyxor. Eller rättare sagt kalasbyxor, för de kallades så då. <!--more-->Flickan och hennes mamma hade kommit först till studion. Jag hade inte en chans att få de bästa kalasbyxorna. <br>Se på dem! Beige med spetsmönster. Klart att jag bet henne! Om hon haft lockigt hår och jag varit starkare hade risken varit stor att jag slagit ihjäl henne. Jag var inget lätt barn, även om den milda blicken kan få det att verka så.

<figure> <img src="/bilder/10615578_10153098314672678_1894236752660924153_n-1.jpg
" alt="husmodern" width="494">  </figure>

Det är inte kameran jag ler mot på bilden, utan den mörkhåriga flickan som just kommit in i studion och satt sig snett bakom fotografen. Hon hade en lejongul teddyjacka och väntade på att fotograferas. <br>Jag har inga bilder på henne. Allt jag har kvar är mitt eget leende på fotot av mig. En förtrollads leende.<br>”Det är snart din tur, Evelina”, sade fotografen.

Evelina.

Ett namn för änglar, prinsessor med korkskruvslockar i sidenklänningar eller möjligen riktiga fotomodeller. Ett namn som klingade som spröda silverklockor. Vad kalasbyxflickan hette har jag glömt för länge sen.

Det var förstås Evelina som hamnade på omslaget. Även om jag hade haft det kvar skulle jag inte lagt ut någon bild av det här. Försiktighet är något nytt för mig, den kom in i mitt liv den där tidiga morgonen i somras – [då när jag såg ett rosa hus i min trädgård och ännu en gång hörde musiken som inte är musik](https://lisaforare.se/langsjon/sommaren-på-siestan/ "En annan historia!"). <br>Nu går jag genom mina gamla foton och urklipp. Sen lägger jag varsamt pussel av mina minnesspillror. Jag lyfter upp fram skärva efter skärva, blåser bort dammet, andas varmt på dem och polerar tills de glänser av klarhet igen. Som om allt som hänt skedde igår. Bilden som växer fram blir allt mer obehaglig för varje bit som faller på plats.

Evelina. En av de vassaste skärvorna.

Hon tycktes betydligt äldre än jag, kanske sju. Ansiktet var smalt och huden hade samma nyans som honungskarameller. Ögonen var mörkt gråblå och näsan perfekt rak. Ett oroväckande utseende, barn ska inte vara så vackra. Inte heller ska de leka sådana lekar som Evelina lekte, men dem visste jag inget om då.

Barn brukar kunna lukta sig till andra ungars rang och jag var ett argsint barn utan vänner – därför förväntade jag mig inte att Evelina skulle le tillbaka. Men det gjorde hon. En nästan omärklig vinkling av mungiporna och ett blänk i blicken som sa att vi var sammansvurna. <br> Och efteråt i studions kök, när kalasbyxflickan skrek på mig för att jag bitit henne, kom Evelina farande och gav henne serie snabba slag i magen. <br>Skithårda. Poff, poff, poff. <br>Kalasbyxflickan böjde sig framåt med ett kvidande och började snyfta högljutt. Besegrad.

Evelina och jag bytte blickar. Hon hade så perfekta ögonbryn att stråna såg ut att vara ditmålade med mammas tunnaste och dyraste pensel. De tre mammorna, som druckit kaffe, kom rusande och gjorde en liten ursäktande charad för varandra. Ojade sig ett tag. Allt medan jag och Evelina såg på varandra och försökte hålla oss för fniss. <br>Jag tänkte förstås på att hon inte var ett dugg lik sin mamma: en rödbrusig kraftig kvinna med hårda blonderade lockar och tjusig benvit byxdress.

Mamma lyfte upp mig, bar mig till dörren och rafsade ihop våra ytterkläder. Jag var rasande för att jag inte ens fått säga hejdå till min nyfunna vän.<br>När vi kom ut på gatan var mamma förebrående. Hon var mer stolt än hon ville erkänna över att hennes vän, journalisten Kicki, så ofta gav just mig modelluppdrag. Jag var långt i från det sötaste barnet i vänkretsen, men jag var späd och såg mycket yngre ut – och därför ett praktiskt val. Dessutom brukade vi få lite pengar och det hände att vi fick behålla kläder eller leksaker. Eftersom mamma var konstnär och pappa forskade behövdes vartenda tillskott.

Just när mamma skulle börja förmana mig hördes ett rop bakom oss. Evelinas mamma stod i porten och viftade med mammas glömda Palestinasjal. <br>Vi vände om. Mamma tackade och <br> Evelina kom ut på gatan, nu var hon klädd i en tunn gyllengul spetsklänning som såg dyr ut. Jag hade en orange klänning som såg hemsydd ut, eftersom den var det.<br> Jo, jag vet att jag är påbyltad tjocka vinterkläder på bilden, men på den tiden hade veckotidningar evighetslång pressläggning. Det här var nog i augusti.

Evelinas mamma presenterade sig som Majken och undrade om vi också skulle till parkeringshuset. Det skulle vi. <br> På vägen dit började mammorna prata. Verkligen prata. Om månlandningen, Vietnam, kalasbyxor och kvinnosaken – ja, allt! Majken var imponerad över att mamma var konstnär och mamma uttryckte sin beundran över att Majken var ensamstående och nöjd med det. När mamma på sitt vanliga vis frågade rakt ut, berättade Majken att hon adopterat Evelina. <br>”Jag hade tur … som fick henne”, sa Majken. <br> Hennes röst blev väldigt tunn. Mamma lade armen om Majken och sa något om att Evelina minsann också hade haft tur. <br>I samma ögonblick tog Evelina min hand, kramade den hårt i sin. Trots att det gjorde lite ont, kändes det som om mitt hjärta knappt rymdes i bröstkorgen.

Vid vår buckliga Saab blev mammorna stående och fortsatte diskutera. Under tiden började jag och Evelina leka något slags kurragömmalek bland bilarna i det bensinluktande parkeringshuset. På en gång förstod jag att Evelina var överjävligt bra på att leka. Inte för att lekarna var så avancerade men Evelina gjorde monsterminer så att mörkret tycktes krypa fram under bilarna. Sen rörde hon sig som en astronaut och vips blev parkeringsgolvet till månens yta, översållad av livsfarliga kratrar. <br>Jag skrattade så högt att mamma vände sig om och såg på mig och hennes ansikte var liksom vidöppet, som om hon inte visste om hon skulle skratta själv eller brista i gråt.<br>Evelina och jag! <br>Vi fortsatte leka så att tiden stod stilla medan våra mammor pratade och pratade. <br>”Nu är vi längst inne i djungeln”, viskade Evelina.<br>Och smög efter mig, så panterlikt att lyckobubblor killade i hela kroppen och parkeringshuset fylldes av slingrande lianer, orkidéer och palmsus.

Till slut, men alldeles för tidigt, ropade mamma att vi måste hem och laga middag. Evelina och jag stod mittemot varandra. Och det var dags att komma på något fantastiskt att säga till Evelina så att hon aldrig skulle glömma mig. Men jag var bara expert på att säga taskiga saker, så vad skulle jag säga? <br>”Djungelleken är min nya älsklingslek” sa jag. <br>Evelina gav ifrån sig ett högt djungeldjurljud och slog armarna runt mig. Sen klamrade hon sig fast vid mig och jag vid henne. Och vi bölade och snorade tills vi inte visste vems tårar som var vems.

Det slutade med att vi skildes med löften om att ses nästa helg, trots att vi bodde i Bromma och de i Älvsjö. Och trots att Majken hade en bil som nog kostat tre fyra gånger så mycket som vår och min proggmamma i vanliga fall skulle kallat henne för moderatbracka. Nu i efterhand förstår jag att mamma ansträngde sig eftersom jag var så ensam.

Jag var trött och frös i bilen på väg hem trots hettan. Ändå kunde jag inte sluta prata om Evelina. Allt hon sagt, allt hon gjort.<br>På kvällen vägrade jag äta gratinerad falukorvsring eftersom halsen var svullen och ömmade. Efter ett tag kom en dånande huvudvärk och frossa. Mellan feberdröm och vakenhet letade jag efter Evelina bland oändligt långa rader av bilar men jag såg aldrig mer än hennes skugga. <br>Jag visste det inte då, men de närmaste femton åren skulle jag få halsfluss flera gånger om året.

Det tog flera veckor innan febern gav med sig helt. Och det första jag gjorde var att kräva att vi skulle åka och hälsa på Evelina. Mamma ringde numret hon fått, det gick till en mäklarbyrå i Älvsjö där ingen hört talas om Majken och Evelina. <br>När mamma ringde fotografen berättade han att Majken och Evelina bara dykt upp på fotograferingen, han hade trott att de var vänner till oss. kanske hade Majken sagt det, han var inte säker. Sen bad han mamma höra av sig om hon fick tag på Majken, för utan efternamnet och adressen kunde han inte betala ut arvodet.

I flera månader höll jag andan varje gång telefonen ringde. Men det var aldrig Majken. Det fanns bara en sak att göra: förhandla med ödet.<br>Om jag klarade att gå på alla K-brunnar på väg till skolan måste hon väl ändå ringa? <br>Om jag simmade tjugofem simtag under klorvattnet på Åkeshov skulle vi träffa dem på stan. <br>Inget fungerade. <br>Under flera år sörjde jag Evelina som den bästa vän jag aldrig fått. Jag fick nya framtänder som var stora och sneda, kinderna blev för magra. Modellförfrågningarna slutade komma.

Sommaren jag var sju – eller var det kanske åtta? – kom en överraskande inbjudan till Evelinas födelsedagskalas. Ett glansigt kort med färgglada ballonger. Telefonnumret stod längst ner, sista siffran skilde det från numret vi fått: en sjua i stället för en etta. <br>Mamma var skeptisk, det kändes märkligt att få en inbjudan av nästan okända. Jag tjatade mig blå tills hon ringde och tackade ja. Tydligen hade Majken fått vår adress av fotografen, varför det tagit flera år frågade mamma inte.

Se på bilden igen! Lägg extra noga märke till mina bens märkliga böjning. Jag är överrörlig. Eller vig som det brukar kallas. <br>Idag vet man att det inte finns något egenvärde i att kunna gå ner i brygga, gå ner i spagat eller lägga fötterna bakom huvudet. Att vara vig är en defekt och som många överrörliga lider jag av obestämbara ledsmärtor. Många morgnar när jag vaknar i smärta förbannar jag mina gummimjuka leder. <br>Ändå var det förmodligen de som en gång räddade livet på mig.

Natten innan födelsedagskalaset var det omöjligt att sova och på morgonen gick jag upp första av alla. Trettiotvå gånger hann jag skriva ”Evelina” i ritblocket, med alla färger som fanns i pennlådan. Medan mina förädrar och syskon sov vidare ritade jag en teckning som födelsedagspresent: två pantrar som skuttade över långa rader av bilar. Sen tränade jag på att busvissla, min syster hade precis lärt sig av pappa och en snygg busvissling skulle verkligen imponera på Evelina. Det gick uselt, troligen på grund av mina jättetänder. <br>Frukosten smakade sågspån och jag bytte kläder så många gånger att mamma höll på att bli galen.

Till slut var det dags att åka. Nog var det juli? Högsommarsolen gassade i alla fall, bilen hade förvandlats till bastu och det beiga konstlädret brände mina lår genom sommarbyxornas bomull.

Det röda tegelhuset låg i korsningen vid Mickelsbergsvägens slut, hukande bland lummig grönska. <br> Majken mötte oss vid grinden i blommigt förkläde. Hon såg äldre och tröttare ut än jag mindes. Håret var nästan helt grått. <br> ”Min man är bortrest. Det blir lite ensamt för oss, särskilt Evelina.”<br>Majken hade alltså skaffat en man? Födelsedagsteckningen var skrynklig i min svettiga hand, Evelina syntes ingenstans.<br>”Hon sover. Hon har varit sjuk ofta senaste åren, precis som du. Ni kanske smittade varandra.”<br>Smittat varandra – det är väl en som smittar den andra? Och hur visste Majken att jag varit sjuk?

Ett festbord stod uppdukat i bersån: kaffekoppar och kanna, en tillbringare, ett fat med bullar. Tårta, naturligtvis. Ballerinakex åt barnen och två arraksbollar till de vuxna. Två glas. Mitt och Evelinas? <br>Bara två barn kunde knappast kallas kalas, fast det här var bättre. Om Evelina var lika ensam som jag var chansen större att bli hennes bästis, fast hon var äldre.

Jag lade teckningen under det ena glaset och sköt fram det andra mot Majken. Hennes händer darrade så att isbitarna i tillbringaren klirrade och hon spillde ganska mycket saft på duken. <br>”Flädersaft. Tycker du om det?”<br>”Jättemycket”, sa jag fast jag inte hade en aning.<br>Saften smakade parfym och var så sockrad att munnen klibbade. Jag drack små klunkar och undrade om Evelina ångrat sig och inte ville träffa mig.

Länge, länge väntade jag. Mamma och Majken var djupt försjunkna i samtal, en bra sak med min mamma är att hon kan prata med vem som helst hur länge som helst. <br>Ingen Evelina.<br>Efter ett tag hämtade Evelinas mamma ett block och pennor. Jag ritade med hårda fingrar och heta ögon. En djungel. En prinsessa. Sen en blombukett. Alla saker jag kunde rita, men jag skrev inte Evelinas namn.

Andra glaset saft.<br>Varför kunde Majken inte bara gå och hämta Evelina? Askkoppen fylldes av fimpar. Mammas hemrullade John Silver och Majkens Blå Blend med filter. Till slut frågade mamma om jag fick gå och väcka Evelina. Men nej, det gick inte. <br>”Hon kan bli väldigt arg om man väcker henne.”

När tredje glaset saft var slut, tittade mamma på klockan, drog efter andan och började säga något.<br>I samma ögonblick dök Evelina upp. Som från ingenstans, plötsligt stod hon mitt på grusgången. Hon hade krans i håret, guldgulrutig klänning, röda ballerinaskor och omaka kortstrumpor. Utstyrseln var så fantastisk att det konstigaste inte sjönk in förrän efter flera sekunder: Evelina såg ut att vara exakt lika gammal som sist vi setts. Ungefär i min ålder. Enda skillnaden var att hon såg sjuklig ut. Håret hängde glanslöst, huden under ögonen var blåaktig.

Ett kort tag stod hon helt stilla och jag blev lika stilla för jag var så rädd att hon glömt vem jag var. Sekunden efter slängde hon sig mot mig. Kramade så hårt att det gjorde lite ont, hon var till och med magrare än jag. Hennes kind var sval mot min. <br>”Kom”, sa Evelina. <br>”Håll er på tomten!” ropade Majken efter oss. ”Du har lovat, älskling.”

Så lekte vi. Som vi lekte!<br>Med Evelina kunde man leka precis alla lekar fast man bara var två. Till och med röda och vita rosen. Jag fångade in henne med ett hopprep, gjorde tusen nålar på henne och stoppade kottar under hennes klänning för att få henne att avslöja alla sina hemligaste hemligheter. Fast jag gjorde de tusen nålarna svinhårt sa hon inte ett pip. När hon snavade på grusgången och slog upp knäet riktigt illa fortsatte hon att leka – utan att röra en min, med blodet rinnande nerför smalbenet. <br>Djungelleken igen. Vildare den här gången. Evelina var jägare, jag var tiger, elefant och krokodil. Det var lite läskigt och väldigt underbart.<br>Till slut slängde vi oss i en fnittrande hög på gräsmattan.<br>”Min älsklingslek”, sa jag. <br>Jag inte kunde knappt andas in eftersom inget mer fick plats i mig förutom all lycka.<br>”Det är min näst bästa.”<br>”Vilket är den bästa?”<br>”Min allra alla bästa?”<br>Evelina lutade huvudet mot min axel och skojmorrade.<br>”Ja?”<br>”Den kan man inte leka här. Bara där.”<br>Hon pekade på den tjocka buxbomshäcken mot grannträdgården. Jag tittade mot mamma och Majken i bersån.<br>”Vi måste ju stanna i er trädgård, din mamma …”<br>Jag såg mot grannhuset. Det var fyrkantigt och ljust rosa. Av någon anledning tyckte jag inte om det.

”Vem bor där?”<br>”Ett spöke. Ett stökigt rökigt spöke som är min riktiga mamma. Hon har varit död i tusen år. Hundratusen! Och mina hundra syskon.”<br>Evelina vindade med ögonen medan hon hittade på. Ingen gjorde grimaser som hon. Men de här var nya. Skevare.<br>”Vi kan leka spöken här. Buhuu”, försökte jag.<br>”Kom nu! Är du en bebis, eller?”<br>Evelina pekade på ett hål i häcken, till hälften dolt bakom ett stort ormbunksstånd. Just då var det viktigast i världen att min nya bästis inte tyckte att jag var en bebis. Därför kröp jag genom häcken. Evelina följde tätt efter.

Trädgården på andra sidan liknade ingen annan jag sett. Träden var översållade av stora vita vissnande blommor och silverskimrande lavar som liksom dröp från grenarna. Gräset kunde bara anas under heltäckningsmattan av nedfallna blomblad och löv. Gråa och brunaktiga höstlöv, fast det var mitt i sommaren. Lövmattan var genomstungen av små gråaktiga blommor som stretade mot solen En konstig sötma steg upp från marken. Allt tycktes liksom dämpat. Inga ljud från insekter eller fåglar hördes. Någonstans långt borta svischade en sprinkler i en sövande rytm.<br>”Det kan vara vår djungel!” ropade jag. <br>”Var inte så barnslig.” <br>Evelinas nya tonfall sved i mig.

Vi lekte oss sakta fram mot huset. Ett slags följa John, där jag snabbt skulle apa efter Evelinas oförutsägbara rörelser och belönades med att få ta steg framåt. <br>Små skeva tallar omgav grusstigen som ledde mot betongtrappan till farstubron. När jag kom nära dem tycktes grenarna dra sig undan. De var täckta av spiralformade barr och små blåpudriga kottar. Luften hade blivit konstig, tunn och tjock på en gång. Nästan som att andas filmjölk. <br>”Jag har fått håll”, sa jag. <br>”Bara lite till.”<br>Hållet högg i sidan, värre för varje trögt andetag. Jag tänkte: kan man drunkna i luft?

Evelinas rördes sig på ett nytt sätt i grannträdgården. Säkrare och mer kontrollerade. Hon hade – jag vet inte hur jag ska beskriva det – helt olika kroppar för olika lekar. Hon kunde vara ett djungeldjur bland låtsaslianerna. En stram soldat i röda och vita rosen. Men hela tiden ändå ett barn, i den här leken var hon något annat. Något okänt och vuxnare än vuxet. <br>”Lägg dig och vila ett tag”, sa hon och pekade på marken.<br>”Det är blött.”<br>Solen hade gått i moln, i alla fall försvunnit bakom ett dis. Allt det gröna i trädgården grånade. Tycktes dammigt. Stumt.<br>”Vi kan väl gå tillbaka? Vi kan leka djungel igen. Du får vara jägare.”Så pinsamt ynklig jag lät.<br>Evelina höjde sina penslade ögonbryn, blundade och suckade.<br> ”Snälla, Evelina …”<br>”Vi ska leka nu, på riktigt. Vi leker att du är sjuk.”<br>”Vi kan …”

Knuffen kom så snabbt och var så hård att den slog andan ur mig. Jag föll handlöst bakåt, landade på rygg. Sprattlade som en skalbagge. <br>”Du är sjuk, du är mycket sjuk”, sa Evelina entonigt.<br>”Jag vill inte.”<br>Vad var det jag inte ville?<br>Evelina sjönk ner på huk bredvid mig.<br>”Vi leker att du inte kan röra dig”, sa hon.<br>Nu var rösten så mjuk att den smet rätt in i min mage. Löstes upp där. Lent och svalt rann vidare ut i kroppen. Armar och ben. Fötter och tår. Först var det nästan skönt. Sen började jag frysa, marken under mig var våt och kall mot min rygg. <br>Sprinklerns strilande ljud hade förändrats: starkare och mer porlande. Det tycktes komma från överallt, till och med underifrån. Det kändes som om jag sjönk, som om marken gav efter under min rygg.

Evelina grävde. Formade händerna till en skopa och öste fuktiga löv över mig. Egendomligt tunga löv. Evelinas blick var fäst på något som tycktes långt borta och helt nära på en och samma gång. När hon lutade sig över mig såg jag att blommorna i hennes krans var samma gråa sort som på marken.<br>”Evelina?”<br>Hon svarade inte. Men hon log. För första gången log hon på riktigt, med skinande ögon.<br>

Och jag förstod att jag varit dum.<br>Djungelleken var inget Evelina gillade att leka. Hon hade bara låtsats tycka om den för att få leka på riktigt. Leka det här.

Älsklingsleken.

Hon böjde sig över mig och slöt mina ögon, viskade:<br>”Blunda, det går snabbare då.”<br> Hennes andedräkt mot min kind – den var kall. Hur kunde den vara så kall? Jag försökte kravla mig ur lövhögen. Kroppen ville inte lyda.<br>”Jag vill gå, det här är ingen kul lek.”<br>”De andra väntar på oss. De vill också leka. Snälla, snälla?”<br>Orden kom tyst och andfått. Och för första gången sen vi kröp genom häcken lät Evelina som ett barn igen.

Mer löv. Det var verkligen löv, men kändes det som cement hälldes över min kropp. Iskall cement som stelnade snabbt och fixerade mig vid jorden, tryckte mig ner i porlande väta. Små spetsiga föremål pressade mot min rygg. Först ett par, sen allt fler. <br>De krafsade. <br>Sökte grepp.<br>Fingrar. Många, många små fingrar. <br>

Allt som gick att tänka var: nejnejnejnej.

Ögonblicket när Evelina såg ner mot löven på marken, skruvade jag snabbt min överrörliga kropp, vred vänsteraxeln ner mot marken. Försökte att inte tänka på den beniga hand som slöts om överarmen nere i jorden. <br>Så vrängde jag upp min högerarm i en omöjlig vinkel och fick upp fingrarna till munnen. Tummen i ena mungipan, pekfingret i den andra. <br>Och busvisslade.<br>Skärande högt och rent – för första och sista gången.

Evelina frös mitt i en rörelse, gav ifrån sig ett gällt gläfsande läte och kastade en blick i den riktning mamma och Majken satt. Sen fortsatte hon fast ännu snabbare.<br>Löv, löv över. Under: fingrar. Som ursinnigt klöste mig neråt.<br>Nu kunde jag verkligen inte röra mig. Inte ens ett lillfinger, inte ens en lilltå. Tårarna frös i fast i vidgade ögon. Tyngden av löven blev ett tryck. Min högerarm pressades mot bröstkorgen. <br>Hårdare. <br>Till slut kom det spröda knastrande, som när isbitar spricker i saft. Smärta. <br> Läpparna frös runt skriket, kvävde det. Osynliga band stramade över bröstkorgen, pressade undan andetagen.  
Ljud i marken, långt ner: rasslet. Viskande, fnissande barn.

Andas. Jag måste andas.<br>a n  d<br>a<br>s

”Lisa?” <br>Mammas röst. Bara några tiotal meter bort. I ett annat universum, på andra sidan häcken.<br>”Lisa, var är du?”<br>Ljud av kvistar som knäcktes. Svordomar.

Myllret mot min rygg stannade av.<br>”LISA!”<br>Med Majken i släptåg trängde sig mamma genom buxbomsbuskarna.<br>”Vad håller ni på med?” vrålade hon. <br>et undan Evelina, föll på knä bredvid mig och föste bort löv och mer löv – fjäderlätta nu. Några virvlade iväg i vinden som maskrosfjun.

”Vi leker bara”, sa Evelina frånvarande. Hon satt på marken där hon fallit.<br> Mamma drog upp mig på fötter, fortsatte att borsta av mig löv och boss. Hon hade djupa blödande rivsår på underarmarna från buxbomen, men tycktes inte märka det.<br>”Min arm, mamma, den …” Jag hejdade mig mitt i viskningen. Min blick hade fastnat på marken där jag legat. Fuktig mylla och …<br>Jag tror inte mamma såg det som låg där, det kan hon inte ha gjort.<br>”Jag tror min arm är bruten, mamma”, sa jag lite högre.<br>Men jag tittade inte på henne och armen kändes knappt, för allt som gick att tänka på var det som låg i leran där jag legat.<br>Alla de sköra små benen. Lösa fingrar, halva små händer. <br>Det lilla kraniet till hälften täckt av våt lera. <br>Grådaskiga trasor av tyg som en gång kanske varit guldgult.

”Vi ska åka nu”, sa mamma nu med blicken fäst på Majken. Majken sa något, men jag lyssnade inte. Smärtan i armen strålade ut i kroppen, det var omöjligt att säga något.

Mamma och jag satt tysta i bilen, hon sa inte ens något om att sätet blev alldeles lerigt. Trots att armen gjorde fruktansvärt ont grät jag knappt. Vi åkte direkt till akuten. När vi anmält oss drog mamma in mig på toaletten och tvättade bort leran så gott det gick. Hon höll mig tätt intill sig medan vi väntade och lovade gång på gång att jag skulle få en Pippi-pappersdocka nästa gång vi åkte till stan. En sådan jag önskat mig så länge.<br>Min arm röntgades och gipsades och jag fick dricka en tjock sötbitter vätska som fyllde huvudet med bomull så att all smärta trubbades av.

När vi kommit halvvägs hem från sjukhuset började min hals svullna, huvudet blev tungt och jag dåsade bort i feberdrömmar om trädgårdsdjungler, pantersmyg och löv som var tyngre än stenar.

Sent följande kväll, när jag halvsov i sängen med katten i famnen, ringde dörrklockan. Majken. Som talade forcerat och så högt att en del hördes ända till mitt sovrum:<br>”Evelina är jätteledsen. … menade inte …olyckshändelse … Evelina har lite problem.”<br>Sen följde en stunds mumlande där jag inte kunde urskilja orden jag måste höra.

Jag gick upp tyst, trevade mig fram genom korridorens mörker upp med katten i famnen och hjärtat skenande i bröstet. Tryckte mig mot väggen bakom dörren till hallen. <br>Mamma svarade Majken att det inte var någon fara, att Lisa tålde en hel del och sen:<br>”Det går nog över med åldern.”<br> Jag kramade katten hårdare. För fast jag var ett barn visste jag att Evelinas problem inte skulle gå över. Inte förrän hela Evelina gick över.

Majken sa något om att träffas, leka igen under uppsikt. Det fanns något desperat i rösten. Mamma svarade att vi skulle höra av oss. Inte närmaste veckorna, men sen – absolut skulle vi höra av oss. <br>Hon lät glättig, talade långsamt och tydligt men orden kom liksom hackigt. Jag tänkte: nu vet jag hur mamma låter när hon ljuger.<br>Så smög jag tillbaka till sängen, drog täcket över huvudet. Gjorde ett bo där de enda ljud som rymdes var mina flämtande andetag och kattens låga spinnande.<br>Äntligen slog dörren igen och snart startade en bil på gatan utanför.

En stund senare kom mamma in i mitt rum, satte sig på min sängkant och lade ett guldglansigt paket på täcket. Först satt hon bara helt tyst med händerna i knäet. Efter ett tag började hon gnola och smeka mitt hår bakom öronen, så där som jag älskade. Hennes fingrar luktade thinner, trygghet, linolja och cigarettrök.<br>Hon räckte mig paketet, jag visste vad det var redan när jag klämde på det mjuka innehållet. En hård klump skavde i halsen. Ögonen hettade när jag öppnade paketet, det tog tid eftersom det tunga gipset på högerhanden gjorde mig fumlig. <br>Till slut vecklade jag upp kalasbyxorna i beige spets.<br>”Vi lägger undan de här, älskling”, sa mamma. ”Lite senare kanske.”<br>Hennes röst var oändligt len, men det fanns en skarp kant i den.

Vi var en familj som pratade om precis allt, men när det kom till Evelina och Majken var det som om det fanns en tyst överenskommelse. Varken jag eller mamma nämnde dem igen.<br>Strumpyxorna låg oanvända i min byrålåda tills de blev för små och försvann i någon utrensning.

***

Läs mer om saker som händer runt Långsjön i min nyutkomna roman Varken (LB Förlag).<br>Du kan[ förbeställa den här](https://www.adlibris.com/se/bok/varken-9789177990864) på Adlibris.  
[Du kan förbeställa den här](https://www.bokus.com/bok/9789177990864/varken/) på Bokus.<br> Pilska nornor, schlagerälskande maror och små smutsiga älvor. Köper du min bok har jag råd att skriva en uppföljare. Det ska jag väl?

Berättelsen ovan är min egen och jag tänker fortsätta på den här på hemsidan, i Varken är det däremot helt andra individer som fått ordet. Personer, om man nu kan kalla det personer, som vistas runt Långsjön. Främst Sara, som jag en gång träffade under märkliga omständigheter.

GILLADE DU HISTORIEN? Dela den till alla du gillar. INTE? Dela den till alla du ogillar, så slösar de sin dyrbara tid på goja.

HÄR FINNS [ALLA BERÄTTELSER FRÅN LÅNGSJÖN](https://lisaforare.se/langsjon/ "KLABBET!").

VILL DU LÄSA FLER BERÄTTELSER HÄR? SPONSRA MITT SKRIVSNUS – OCH ETT LEGOBYGGE TILL MIN SON SÅ ATT JAG FÅR LUGN OCH RO.  
0709-733 331