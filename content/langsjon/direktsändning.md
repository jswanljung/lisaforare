+++
avsandare = ""
bilder = []
date = "2018-12-15T14:10:29+00:00"
inlaggsbild = "/strykjarn.jpg"
title = "Direktsändning"

+++
”… och så får vi höra vad Anna Palling åt till frukost?”

”Åt till frukost?”

”Patrik vill bara testa ljudet. Du kan säga vad du vill så länge du pratar på.”

”Åh! Jag åt … vänta nu! Det vanliga: kaffe latte, ett löskokt ägg, lättyoghurt, två smörg …”

”Tack det räcker! Lollo, kan du flytta fram Annas mick lite?”

”Så här? Hur låter jag då?”

”Sammetslen och småkåt. Som alltid, Lollo. Som alltid.” <!--more-->

”Kör du eller jag nyheterna sen?”

”Jag kan ta dem. Kör så det ryker nu, tjejer!”

”Hur känns det, Anna? Nervös? Du har aldrig varit med i radio förut va? Vi pratar lite om din kommande bok. Du läser några rader. Biten om seansen, tyckte din redaktör. Jag fick bara de tre första kapitlen av henne, men de är otroligt bra. Vågade inte ens gå upp och släppa in katten i natt.”

”Tack, jag uppsk…”

”När röda lampan lyser är vi i sändning. Annars är det bara att luta dig tillbaka så kör jag. Blir det bra?”

”Jättebra.”

”Gooood natt alla nattugglor! Det här är Radio Älvsjö och _Lollo mitt i natten_ med Lollo Lundgren. Vi har den skönaste musiken, de skönaste gästerna och framför allt – de skönaste lyssnarna. Kvällens gäst är Anna Palling som ska berätta om sin kommande bok _Dödstrummorna_ . En riktig, riktig rysare. Patrik Johansson, vår tekniker och flygande nattreporter sitter i slussen och väntar på att just du ska ringa. Ring in på 08-239049.”

<hr>

”Det där gick ju bra, Anna.”

”Gjorde det?”

”Gör så här under låtarna: Ena hörluren på örat, andra av. Då hör du om Patrik i slussen vill nåt.”

”Sjunger du alltid med?”

”Bara när det är nåt jag gillar. Det är en kille på huvudkontoret i City som väljer musiken.”

”Lollo, Patte här. Det är nåt jävligt mysko med frekvenserna.”

”Frekvenserna? Lät jag konstig?”

”Det är typ tio personer som ringt in och sagt att det låter som om det är fler i studion. En käring var vettskrämd och insisterade på att hon hörde sin döde man som försökte lägga sig i. Hon är helt säker på att det var han för han kallade dig radiofittan, som han alltid gjorde. Han har varit död i tre år. Lika bra det, verkade det som på käringen. En man säger att han hörde sin farmor sjunga inne i köksfläkten. Nån unge har ringt säkert 18 gånger och bara grinar i luren. ”

”Du skämtar.”

”Dagsens sanning.”

”Nån måste hällt nåt kuckelimuck i vattnet. Vad hörde du?”

”Totalos normalos. Annars hade jag ju brutit. Hade ju hoppats på lite mer action med två fräscha brudar i studion, men man …. ”

”Kan vi få hit en av dem till studion till efter nyheterna? Så kör vi några telefonare så länge.”

”Jag kan försöka få tjejen i Långbro. Maria Ga… Ja, Maria nåt spanskt. Hon verkade mest samlad. Hörde sin syster inifrån diskmaskinen .”

”Maria Garcia da Silva?”

”Ja, just det.”

”Henne känner jag. Gick i parallellklass. Jag minns hennes syster som försvann, jättegullig unge. Kolla om du kan få tag på taxi. Om hon inte kan köra själv, alltså. Tänk inte på budgeten.”

”Det går på ditt ansvar.”

”Patrik, det här kan bli hur stort som helst. Riks, säkert. Jag tar dig med hela vägen. Lovar.”

”Vad gör vi med Palling?”

”Anna stannar kvar, fast som expert. Hon har ju skrivit en bok om övernaturligheter. Är det ok med dig, Anna? ”

”Jo, men …”

”Fatta hur bra det här är för din bok. Super. Du kan få läsa mer ur den senare i sändningen. Lite längre. Läs det där stycket med flygplanssexet så blir Patrik glad. Du vet: tentaklerna och det.”

”Är de arga på mig?”

”På dig? Det är ju mitt program. Det är alltid nån lyssnare som flippar. Det räcker med att man läser vädret. De tror att det är jag personligen som sett till att det inte regnat på hela sommaren.”

”Jag menar inte lyssnarna. De andra.”

”Vilka andra? Hjärnspökena? Vad spelar det för roll?”

”Jag visste inte att de skulle bli arga.”

”Patrik, har du fått tag på Maria?”

”Taxin är skickad. Hon vägrade köra fast hon bara druckit ett par glas vin. Vi har en telefonare på ingång också.”

”Ibland tror jag att jag älskar dig.”

”Upp till bevis. Du får en Lina Sörman i Långbro. Lite upprörd. Hon ringde på mobilen men jag har fått över henne på fast lina nu. Lina på linan, haha. Låten är slut om tjugo. Jag släpper in henne direkt.”

_– Vindarna vänder, drömmen består. Vi håller varandra när stormarna yr, finns här intill dej när morgonen gryr. –_

”Det var powerballaden _Allt som jag känner_ av [Norell Olsson Bard](https://sv.wikipedia.org/wiki/Norell_Oson_Bard) med Tommy Nilsson och Tone Norum. Du lyssnar på Radio Älvsjö och _Lollo mitt i natten_ med Lollo Lundgren. Vi har fått rapporter av hundratals lyssnare som ringt in och rapporterat om oförklarliga röster som stört sändningen. Har vi en spökinvasion på gång? Med oss i studion har vi Anna Palling, blivande succéförfattare och välkänd expert på övernaturliga fenomen. På telefon, direkt från har vi Lina Sörman. Hallå Lina, hur står det till i Långbro?”

”Hej Lollo. Måste bara få säga att jag alltid lyssnar på dina program. Varje natt.”

”Tack finaste! Lina, du stod och strök i tvättstugan alldeles nyss när nåt konstigt hände. Berätta! Ta det lugnt.”

”Jag trodde det var teven först. Men det var strykjärnet. Nån som pratade långt inne i strykjärnet. Först hörde jag inte vem det var. Sen började den prata om trupprörelser i Sudeten. Jag är nästan säker på att det var min pappa, han var besatt av andra världskriget.”

”Sa rösten nåt mer?”

”Att jag … att jag fått fet häck. Sen skrek den. Bara skrek …”

”Linavännen, allt är okej, gråt inte. Jag skickar styrkekramar här från studion. Vad skrek rösten?”

”Den skrek: hälsa henne att vi inte luktar illa. Och att vi inte har hesa röster.’”

”Vad hände sen, vännen? Orkar du berätta? Tala nära telefonen, det hörs det bättre då.”

”Det … det sprakade till i strykjärnet. Jag fick en stöt och släppte det. Sen började det glöda, plasthandtaget bara smälte och det brände en stor fläck på strykbrädan. Sen slutade allt bara. Jag har inte vågat gå in i tvättstugan sen dess.”

”Stort tack för att du delat med dig. Det är lyssnare som du som gör _Lollo mitt i natten_ till vad det är. Vi skickar en liten present till dig som tack.”

”Kan man få ett nytt strykjärn?”

”Vi ska se vad vi kan göra, Lina. Det ska vi verkligen.”  
  
  
\----------------------  
Min roman Varken (LB Förlag) kommer ut 26 augusti 2019. Den utspelar sig i samma universum som novellen. [Du kan förbeställa eller beställa här.](https://www.bokus.com/bok/9789177990864/varken/)