+++
avsandare = ""
bilder = []
date = 2019-08-26T22:00:00Z
inlaggsbild = ""
title = "VARKEN: Sten, sax, påse"

+++
<figure> <img src="/bilder/10685376_10152349640351556_5109932060922540869_n.jpg" alt="tunnelbana" width="494">  </figure>

_Lördag._ Visst är det lördag? Det känns som lördag. Att det är tidigt på morgonen är ett faktum. Att huvudet exploderar nästa gång tunnelbanan kränger till är en gissning. Längst ner i väskan finns två Alvedon, som alltid. Första fucking hjälpen. <br> Sara sköljer ner tabletterna med den sista ljumna colan i plastflaskan och kräks nästan. Andas, överlev. Ingen dör av att vara bakis. Det känns så, men det går över. Allt går över. <!--more--> Mobilen har tre procent batteri kvar. Det är visst fredag. Halv elva, den tjugonde november. I morgon är det tjugotre månader sen Gabriella försvann.

_Fyra stationer kvar._ Vagnen är knappt halvfull. Ändå är luften tung av allt som är november. Fuktigt ylle, gummisulor repade av gatusand, mandarinskal och vintertrötthet. Ibland är det intressant, eller i alla fall praktiskt, att ha extremt skarpt luktsinne. Oftast är det störande. Som nu: otvättat hår, stressvett och billig deodorant. Lukten av saknad finns överallt och hela tiden eftersom den är hennes egen. På vissa platser, de med minnen, blir den outhärdlig. Kanske hade det hjälpt att gråta, det är vad folk gör, men Sara har aldrig varit som folk.

_Tre stationer kvar._ Natten har varit så lik många andra nätter att den saknar konturer. Livet på repeat. Carmen, Debaser, The Hoe och den namnlösa svartbaren vid Hornstulls strand när allt annat var stängt. Glas efter glas med billigt rödvin. Utspillt vitt pulver i toaletthandfatet som får skrapas upp med pekfingernageln. För fuktigt. Gnugga in i tandköttet i stället, tills smaken av tvål och smuts ger hulkningar. <br>Mer dricka: tequila, lakritsshots – vad spelar ingen roll så länge någon bjuder. Till slut lindas verkligheten in i bomull och slutar angå. <br> Det fortsätter, det finns alltid en fortsättning: i smutsiga eller Rut-kliniska lägenheter där hon vilar på persiska mattor eller i nedsuttna Ikea-soffor. Händer på hennes kropp och bebisblå piller som smakar Vim ger ett sorl i kroppen, ett svagt eko av något hon behöver men inte har ord för. Nätter som aldrig tar slut, ibland drar hon till jobbet på Coop utan att byta kläder. <br> Sara går på timmar, håller avstånd, tömmer diskmaskinen i personalrummet innan någon ber om det. Packar upp back efter back i mejerihyllan tills axlarna värker. Butikschefen pratar inte längre om en plats i kassan.

Skulle någon ens sakna henne om hon också försvann? Knappast någon av dem som lever där Sara lever – lite vid sidan av. Vänner kan hon inte kalla dem, men de är sällskap. Möjligen skulle Heikki sakna henne ett tag, men inte särskilt länge. Två stationer kvar. Sara hade bott i Heikkis hyrestrea i fyra dagar när hon berättade att hennes syster försvunnit spårlöst. <br> »Stanna här. Ta en minut i taget, sen en timme i taget, rätt som det är ringer hon«, hade han sagt. <br> Det är enda gången de pratat om Gabriella. Heikki ställer aldrig frågor eftersom han själv inte vill ha några. De tröttnade ganska snabbt på att ligga med varandra, men konstigt nog inte på varandras sällskap. Heikki dricker litervis av Yogi-te och kan prata länge om ingenting, men också vara tyst en hel kväll när de tittar på Netflix och HBO. Han sover alldeles ljudlöst, en underskattad egenskap. <br> Sara hade antagit att han var högst tjugofem tills hon hittade ett kort på hallmattan där ett av hans ex grattade på trettiotvåårsdagen. Fem år äldre än Sara, ingen av dem i närheten av vuxen. Varför han låtit Sara bo kvar i gästrummet så länge vet hon inte. Antingen för att han inte gillar att vara ensam eller för att någon besvärligare annars skulle försöka flytta in. En man med förstahandskontrakt några kvarter från Tekniska högskolan är högvilt. <br> De första veckorna gick Sara sällan utanför dörren. Hon såg på teveserier, åt pizza, stirrade på mobilen som aldrig ringde och räknade minuter. Dålig metod. Så mycket enklare att låta dagar och nätter flyta in i varandra.

_En enda station kvar._ Vagnshjulens gnisslande blandas med ett annat ljud, ett dovt prasslande som kommer och går i en rytm som inte är tunnelbanans. Samma ljud som brukar komma när hon är trött, ändå är det lika irriterande varje gång. <br>  Knogarna på högerhanden ömmar. Har hon slagit någon? Ingen som inte förtjänat det. Till och med väggar kan jävlas innan man får de små bebisblå.<br>  Vid tretiden i morse följde Sara med en skäggig kille från svartbaren i Hornstull. Bästa sorten: söt och måttligt smart. Med lägenhet på Högalidsgatan, krypavstånd från baren. Han bjöd på ett antal drinkar, fast allt han behövt göra var att fråga om hon ville följa med hem. Hans problem. Alfred? Aron? Aron. Sexet hade varit så bra att det nästan var värt att minnas hans namn. <br> När han somnat låg Sara bredvid honom och stirrade i taket flera timmar. Månen var också full, sken in genom fönstret och blekte hennes nakna kropp, suddade ut gränserna. Ljuset fick sorlet från de små blå pillren att förstärkas till ett brusande i kroppen och inget gjorde ont längre. <br> När taklampan återfick skarpa konturer letade Sara upp yoghurt och flingor i köket och åt stående vid diskbänken. Hon lånade Arons tandborste. Inte för att det hjälpte mer än tillfälligt: när hon äntligen kliver av vid Tekniska högskolan smakar munnen smutsig marsvinsbur igen. <br>

Förmodligen är hon hög fortfarande och på något annat än det vanliga, för omgivningen har en obeveklig skärpa. Verkligheten är verkligare och alla ljud tränger sig på. Rulltrappans knyck och en pust av sur svett gör henne åksjuk. Sara drar ner jackans dragkedja och sniffar i armhålan, skiter i om någon ser. Inte hon som luktar. Måste vara någon av de andra resenärerna i rulltrappan. Lukten blir starkare. Den är skarpare än stress, mer påflugen. Rädsla, nästan panik. <br> Där, han! <br> En mörkhyad kille tre steg ovanför spanar ner mot perrongen de just lämnat. Han är kraftig, klädd i bylsig fotbollsjacka och omaka fingervantar. Ung, mittemellan barn och man. Lite finnig i pannan, halvmustasch och stora mörkbruna ögon med långa ögonfransar. Sara kan ana killens darrningar, hennes hjärtslag snavar över varandra och händerna börjar skaka. Hon hatar det här.<br>  Intrången. <br> Det händer allt oftare sen Gabriella försvann, alltid när Sara är trött, stressad eller hungrig. Alltså: en bakisdag i ett jävla nötskal. Men det här är värre än det brukar. <br> Skräcken strömmar från killen, omöjlig att värja sig mot. Så tumlar hans tankar in i hennes, så hårt att hon tappar andan.  
_– det här är allvar får inte åka fast, inte nu när jag kommit så långt, Reza och Amir väntar, nya stället, vi är trygga där, skickar de tillbaka oss vet jag inte vad jag gör, dör hellre tusen dödar –_  
Sara känner i låren hur killen spänner musklerna i sina. Han tänker springa. Nej, mer än så. Fly. Saras vader, hans vader tar spjärn. Fuck! Hon gör som förra gången någons tankar fastnade i hennes. Sluter ögonen, koncentrerar sig, pressar ut honom så att de är två igen i stället för en enda. Bara en hetta bränner kvar bakom ögonen. Huvudet är lätt, allt är lätt. Färgerna på reklamaffischerna är skarpare, de sjunger i henne. Det gör den nya lukten också. Kaffe! <br> De har kaffe på 7-Eleven, precis utanför spärrarna. Inte gott, men kaffe. Allt hon behöver för att må bättre är en stor kopp med mycket socker. Sen hem 1 5 och sova, en livrädd kille i rulltrappan är verkligen inte hennes ansvar. Killen ser sig om igen. Har han märkt något? Nej, det är inte henne han spanar efter. Två poliser är på väg uppför rulltrappan, de får syn på killen och börjar röra sig snabbare. Inte lägga sig i nu, Sara. Kaffet och frälsningen finns tjugo meter bort. Lugn och fin. Killen kliver av rulltrappan. En liten kö har bildats innanför spärrarna där två väktare står och kräver leg. Uppblåsta av sin egen betydelse, losers som leker polis eftersom de aldrig skulle komma in på polishögskolan. De låtsas titta på vissas leg, andras granskar de noga. Blonda som Sara viftas förbi, så är det alltid.

Nu pulserar ångesten från killen. Stickig svavel, starkare än kaffelukten. Den stackars jäveln har inte en chans, vissa får hur många som helst utan att förtjäna dem. <br> En glöd växer i Sara, strålar ut i armarna, händerna, fingrarna. Pulserar ända ut i fingerspetsarna. Knastrandet och bruset växer sig starkare. Hon tänker knappt, kliver bara av rulltrappan, tar några snabba steg fram och knackar ångestkillen på axeln. Han rycker till, snor runt. <br> »Älskling!« <br> Hon fångar hans ansikte mellan sina händer och drar ner det mot sitt och trycker munnen mot hans. Han smakar metalliskt av rädsla, hon av allt hon gjort under natten. Det kan gå på ett ut. Hon drar ut på kyssen några sekunder innan hon ropar: <br> »Jag är inte med barn! Jag testade nyss.« <br> Flera av de köande vid spärrarna skrattar, en medelålders kille med hästsvans och jeansjacka mest. Den äldre av väktarna, en kraftig rödlätt man i fyrtioårsåldern flinar lite – och så öppnar han sig. Sara kan se honom, se rakt in. <br> Hon vet inte själv vad hon gör, ändå tycks det självklart. Hon glider. In i väktaren. Vet att hon måste få fram något, fort som fan, för ångestkillen måste igenom spärren. Hon rör sig i väktarens tankar. Och de blir hennes:  
_– en stor hund skakar regnvatten från pälsen, en kraftig gråhårig kvinna sjunger, syrener, nyklippt gräsmatta, glas splittras –_

Det räcker inte, Sara behöver något annorlunda, något bräckligt. Polisernas steg närmar sig bakom och hon tvekar. Så kommer en viskning i henne, hon har gjort det här förr, hon kan det här. Sara låter instinkten leda, letar sig längre in. Djupare än hon varit i någon. Faller, handlöst fast ryckvis. Väktarens tankar sluter sig runt hennes och paniken är nära. Hon sliter, det som håller fast henne brister, rivs isär likt tunt tyg. Halvt kvävt under lager av lögner hittar hon det. Blir en del av det:

_– vitglödande sol, dujagvi andas häftigt in tjock luft tång tjära bensin. kysser – hals, nacke och axlar – är så lycklig att det gör ont. allt känns som nu fast det är så länge sen. huvudet spränger när dinaminavåra läppar smakar salt från svettig hud. Paul. så heter han, den du inte kan glömma fast du försökt och försökt. mannen som så mjukt stryker håret från ditt ansikte med pekfingret. tänk på Paul! ingenting annat spelar någon roll –_

Väktaren är vidöppen nu. Sårbar. Vad i helvete håller hon på med? Inte tänka, bara göra. <br> Sara säger milt till honom:<br>  »Paul hälsar.« <br> Hon håller väktarens blick medan hon sakta stryker den darrande killens mörka hår från ansiktet med baksidan av pekfingret. Så nickar hon mot spärren och säger till väktaren: »Vi ska förbi.« Väktarens ansikte veknar. Han trycker upp spärrgrinden, viftar fram Sara och killen medan han vinkar lugnande till poliserna med andra handen. Väktaren kommer att få skit för det här sen. Monumental skit. <br> Med killen i en fast armkrok sveper Sara igenom kontrollen och kastar en slängkyss åt väktaren, som öppnar munnen kanske för att ropa till dem. Så vacklar han till och sätter händerna mot pannan med en grimas. Sara vänder sig inte om, går snabbt, motstår frestelsen att springa. Hon drar med sig killen ut genom stationsdörrarna. En man ropar efter dem när de springer. Snabbt, sida vid sida. <br> Tunneln. Trappan. Över busstorget i regnet, killen snubblar till och hamnar nästan under en buss. Så in på Danderydsgatan, utom synhåll. Insidan av Saras skalle brinner, bränner, dunkar. Hon stannar och stöttar sig med händerna mot de värkande låren. Killen stannar bredvid.<br>  »Spring!« väser Sara.<br>  Killen stirrar på henne. Det rinner varmt över hennes läppar och ner över hakan. Saltsött metalliskt. Näsblod. Killen sträcker ut handen, lägger den på Saras axel. Hon har inte utsatt sig för det här för att han ska åka fast på fem minuter. Hon skakar av sig hans hand, knuffar till honom: <br> »Run like hell, idiot!«<

Killen sätter fart och svänger av på Östermalmsgatan. Sara rätar på sig och försöker få allt att sluta snurra. Varje andetag hugger i sidan, trots att hon sprungit högst hundra meter. Solljuset letar sig fram bakom molnen, skär i ögonen och världen exploderar i vitt ljus och vitt brus när hon faller.

När allt blir stilla igen står hon på alla fyra, till hälften i en vattenpöl, och skakar av köld. Jeansen är genomblöta. Handflator och knän värker. Blod droppar från näsan, ner i spyan med halvsmälta cornflakes och blåbärsyoghurt. De fräna, varma ångorna får henne att börja hulka igen.

När hon tittar upp står en kvinna i röd kappa där. Mörkhårig, blekhyad. Snygg – nej, vacker. Blicken som möter Saras är stadig, gråblå och mycket allvarlig. Sara känner igen henne vagt. Varifrån? Knappast The Hoe. Universitetet kanske. Eller kund från Coop? »Hur är det, Sara?« För jävligt, tackar som frågar, tänker Sara. Sen: hon vet mitt namn. <br> Kvinnan går nära och lägger en hand på Saras rygg. Sara vill slå bort den, hade slagit bort den – om det inte vore för kvinnans parfym: alla barndomsminnen Sara borde ha, invävda i betryggande men sval lavendel. »Jag är Louise«, säger kvinnan.<br>  »Det gäller din syster. Gabriella.« <br>  Rasslandet kommer tillbaka, överröstar alla tankar. <br> Det vita tar över igen.

<figure> <img src="/bilder/288377_10150862827206556_1524096981_o.jpg" alt="tunnelbana" width="494"> <figcaption>Foto: Fredric Dahlgren </figcaption> </figure>

Rummet där Sara vaknar ser ut som ett arbetsrum. En slött svängande värmefläkt hummar i ett hörn och sprider varma pustar. Sara halvligger i en sliten läderfåtölj. Kvinnan som heter Louise står på knä på den svartblåmönstrade mattan och tvättar Saras händer med våtservetter. Högra handflatan blöder på ett par ställen, inget av såren är djupt. <br> Ett minne flimrar förbi: mamma plåstrar om Saras knä medan hon nynnar. Mammas långa ljusa hår faller över ansiktet och kittlar Saras handleder. Sara blundar, håller andan och försöker hålla kvar minnet, men det bleknar bort och nuet tränger sig på. Någonstans långt borta droppar en kran. Utanför det stora fönstret syns en ljus tegelfasad. Den säger henne inget. Hon skulle kunna vara var som helst.

Illamåendet kommer och går i långa dyningar. Sara vill hem till lägenheten, duscha länge och sen sova i ett dygn. <br> Så kommer minnesfragmenten tillbaka, ett efter ett: Louises arm runt axlarna. En evighetslång trottoar. Louise som hjälpte henne upp för en sliten stentrappa. En tung port slog igen bakom dem. Sen genom glasdörrar och en öde korridor med linoleumgolv som luktade nyskurat så där som det gör på sjabbiga institutioner – med ingrodd smutslukt i bakhåll.

»Var är jag?«

»På vårt kontor på Kungstensgatan. Du har sovit drygt en timme, du behövde det.«

Vårt?

Louise reser sig, slänger våtservetterna i en flätad papperskorg och slätar till kjolen på sin ljusgrå dräkt. Hon slår sig ner bakom ett stort skrivbord i mörkblankt trä och iakttar Sara. På väggen ovanför Louises huvud hänger två tavlor i dämpade färger, de föreställer absolut ingenting. Mellan dem sitter en stor klocka där sekundvisaren jagar runt med små skarpa smällar.

»Vi ska bara leka en liten lek, Sara. Efter det pratar vi om Gabriella. Om du vill kan du gå sen, jag lovar.«

Varför lovar Louise att Sara får gå, som om det vore hennes beslut? Louise slår upp vatten från en karaff och skjuter fram glaset mot Sara. Leende. Kallt vinterljus faller över Louises ansikte, det är omöjligt att ens gissa hennes ålder. Drygt fyrtio? Femtio? Inget grått syns i det mörka håret uppsatt i en lös knut. Hyn är så slät att den tycks självlysande, Louise skulle kunna göra reklam för en hudkräm så sanslöst dyr att en massa idioter inbillar sig att den funkar.

»Vet du var hon är?« frågar Sara.

Louise skakar på huvudet.

»Leken först.«

Det är så enkelt att läsa Louise. Hon är lika klar som vattnet i glaset. Hon vill väl. Sara nickar och dricker. Louise ler som om hon fått en present.

»Sten sax påse, kan du den? Jag räknar.«

Herregud, människan är galen.

»Vad vinner jag?«

Louise öppnar skrivbordslådan, tar upp ovikta hundralappar och lägger ut i en solfjäder på skrivbordet. Minst en halv månadshyra. Kanske en hel. Sara är skyldig Heikki tre.

»En för varje vinst.«

Sara nickar. Louises ögon är gråare nu. Ljusare. Hon knyter näven framför Sara och räknar in:

»Ett två…«

Louise är vatten, glas, is. Transparent. _Sten._

»Tre!«

Saras påse vinner över Louises sten.

Klick, klick från klockan. En övervintrad fluga surrar trött mot fönsterrutan. Prasslandet i Saras huvud har mojnat till ett lågt viskande.

»Ett två tre.« _Sax._

Louise gör sax. Sara påse.

»Ett två tre.«

Sara är fruktansvärt trött, men inte så trött att hon är dum. Hon förlorar ibland, vinner lite oftare. De spelar hundra gånger. Louise räknar upp femtiosex sedlar i Saras hand. Helt okej timlön.

»Berätta om min syster!«

Louise ser ut som om hon tycker synd om Sara.

»Hur tycker du att spelet gick, Sara?«

För lätt, det hade gått för lätt. Louise fortsätter:

»Vi spelade hundra gånger. Statistiskt sett är sannolikheten att spela hundra gånger utan att en enda gång få oavgjort…«

»Ungefär en på triljonen«, fyller Sara i. Visserligen har hon varit klantig, men räkna kan hon. Hon försöker skaka av sig Louises blick.

»Du var för enkel att läsa«, muttrar Sara.

Louise skrattar. Ett fint skratt, lite likt Gabriellas. Sara vill säga något som får Louise att skratta igen men hon är för utmattad. Huvudet helvetesvärker.

»Mer vatten?«

»Vad är det du vet om Gabriella?«

»En sak i taget. Vi kommer till det.«

Louise häller upp vatten, går runt skrivbordet, drar fram en stol och sätter sig bredvid Sara. Frågar:

»Kan du pejla alla?«

Pejla. Det finns alltså ett ord för det? Men Louise har inte med det att göra.

»Sara?«

Louise sträcker sig fram, tar Saras svettiga händer i sina torra svala och ser in i hennes ögon. Louise tycker om henne, Sara kan se det tydligt. Louise doftar yllepläd och choklad och Sara vill luta huvudet mot hennes axel. Det är som om de känt varandra länge, kanske alltid.

»Alla?« upprepar Louise.

»Bara vissa. Ibland. Det är enklare när de är arga eller rädda. Jag brukar kunna se små bitar, aldrig så mycket som i dag.«

»Har du alltid kunnat det?«

»Vet inte när det började. Jag visste ofta vad Gabriella tänkte när vi var små, ibland tänkte vi tillsammans. Jag trodde att det var för att vi var tvillingar, att alla tvillingar gjorde det. När vi blev äldre försvann det nästan helt. För snart fyra år sen, när jag flyttade till Malmö, började det igen. Så småningom fattade jag att jag kunde… påverka lite också. Fixa saker. I alla fall vissa nätter.«

Några gratis drinkar. Fritt inträde. Extra piller. Någon med en aning mer ambition hade satsat på världsherravälde.

»Vet fler än jag vad du kan?«

Orden kommer lätt ur Sara nu, hennes men ändå inte:

»Mamma visste. Hon dog när vi var elva.«

Alla minnen av mamma är suddiga. Skratten när Sara och Gabriella var små, den tysta gråten som kom senare. Mamma med det glänsande håret utspillt över mattan i vardagsrummet när hon låg med Sara och Gabriella i famnen. Dofterna: cigarettrök, sorg och den barnsliga parfymen med viol. Ärren på de tunna lena armarna.

»Ingen annan?«

»Inte mormor i alla fall. Kanske andra. Jag var oförsiktig ett par gånger. En fest för några veckor sen…«

Louise halvler som om hon förstår precis. Vad vet Louise med den prydliga dräkten om fester? Saras typ av fester.

»Var ni nära varandra, du och din syster?«

Klumpen i halsen som är där varje gång hon tänker för länge på Gabriella gör det omöjligt att svara. Minnesglimtar igen, så tydliga som om allt vore nyss. Är det något Louise gör? <br> Sara och Gabriella är fem år i kojan under bordet. Åtta år på rygg i stubbigt gräs, hand i hand. Fjorton. Sara cyklar med Gabriella på pakethållaren. Gabriella sitter grensle över Sara och kittlas och kittlas tills Sara kissar på sig.

»När vi var små. Sen… Vi blev mer olika helt enkelt.«

Sara hade kunnat säga mer. När de var bebisar hade de varit i stort sett identiska, sen blev de olika. Lekte egna lekar bredvid varandra. Alltid nära, aldrig tillsammans. När de flyttade till mormor förändrades Gabriella ännu mer. Hon blev smal, lång, snygg. Bäst i klassen, skolans Lucia och sen vuxen. <br> Sara blev Sara.

»Vi sågs väldigt lite de sista åren. Gabriella gifte sig, jag jobbade i Malmö.«

Nästan sant. I alla fall ingen lögn.

»Sen träffades vi igen när jag kom upp till Stockholm, samma natt försvann Gabriella«, fortsätter Sara. Det blir en viskning, ändå är det första gången hon säger det utan att det gör ont, så hon säger det igen:

»Gabriella försvann.«

»Vad tror du hände?«

»Vi var hemma hos henne. Hon gick ut för att hon skulle jobba kväll. Hon kom aldrig tillbaka.«

»Var tror du att din syster är?«

»Ingen aning. Hon lever. Jag skulle veta om hon var död.«

Louise frågar inte ens hur Sara skulle kunna veta, hon nickar bara.

»Letade du efter henne på egen hand? Vi vet att ingen har lämnat in en polisanmälan.«

»Vilka vi?«

»Jag kommer till det. Letade du efter Gabriella?«

»Jag tänkte att hon hade orsaker.«2 3

Drygt tre miljoner orsaker. Juristen som skötte dödsboet hade berättat att Gabriella tömt mormors sparkonto veckan innan hon försvann. Gabriella hade tydligen blivit god man när mormor flyttade in på hemmet. Det är ett mysterium varifrån mormor fått mer än tre miljoner. Man blir inte rik av att vara snål.<br>  Sara bryr sig om pengarna, det gör hon. Inte på så sätt att hon vill ha dem, utan för att Gabriella uppenbarligen brydde sig så mycket om dem att hon lämnade Sara för att slippa dela med sig.

»Hon försvann med pengar, det vet vi. Hade hon träffat någon?«

Ordet vi igen.

»Hon sa att hon inte gjort det.«

»Men du tror det.«

»Hon försvann en gång tidigare. Till Paris, med en fransman, men då ringde hon efter några dagar.«

Killen som gått en sommarkurs på Konstfack kom från en förmögen adelsfamilj. Gabriella hade blivit måttligt nöjd när det visade sig att han inte bodde på familjegodset utan i ett new-age-kollektiv i en förort till Paris. Utan rinnande vatten, med höns i sovrummet. Efter knappt två veckor var Gabriella tillbaka i Sverige.

»Så det var därför du inte letade efter henne?«

Sara tänker på meddelandet Gabriella hade lämnat på telefonsvararen. Orden som bränt i henne så länge.

»Har du hört av Gabriella sen dess?«, fortsätter Louise.

»Inte ett ljud.«

»Vi kan ta mer om det där nästa gång, när du är utvilad«, säger Louise.

Vilken jävla nästa gång? Sara börjar resa sig, men Louise tar ett mjukt tag om hennes armar. Söker Saras blick.

»Vi behöver din hjälp, Sara.«

»Vad vill ni att jag ska göra?«

»Samma sak som din syster gjorde.«

»Jag passar inte som sekreterare.«

Hon kan inte ens ta ansvar för sig själv. Louises leende blir varmare.

»Blunda och slappna av. Jag ska dela en sak med dig.«

Sara blundar.

Louise drar efter andan, kramar hårdare om Saras händer. Något rör vid Saras ansikte, likt långsamma lätta fingertoppar först, sen snabbare: ett sakta ljummet regn. Ögonlocken blir tunga. Illamåendet tonar bort. Sara vill spinna som en katt, rulla ihop sig och somna i stolen. Louise gör det här med henne och Sara tycker så så så mycket om det. <br> Louise lutar sig närmare, drar sakta sin tumme längs insidan av Saras underarm.

Bultandet i huvudet avtar. Ett litet nöjt ljud slipper från Sara, det går inte att hjälpa. En bild kommer till henne och hon vet att det är Louises: ett rum, ett litet åttkantigt rum, gråvita väggar med stängda dörrar i ljust trä. Luften är frisk, golvbrädor skurade med grönsåpa. <br> Gabriella är där. Mitt i rummet. Orörlig i sin turkosa dräkt med stilla allvarsansikte där bara munnen ler. Håret uppsatt i en lös knut. Ansiktet är helt tydligt, resten är liksom skissartat och smälter ut i det blekgrå.<br>  Efter några sekunder försvinner bilden.

»Din syster arbetade för oss på Probonum«, säger Louise sakta.

Saras huvudvärk är tillbaka, blixtrande nu. Det hjälper lite att pressa handflatorna mot tinningarna. Louise gör lugnande ljud, som till ett oroligt djur. Så slår hon upp mer vatten i Saras glas, skakar ut två Treo från ett rör och lägger ner. Tabletterna virvlar pysande runt i glaset. Öronbedövande.

»Många av oss är ljudkänsliga«, säger Louise medan hon iakttar tabletterna. »Vi hör ljud andra inte hör, känner lukter andra inte märker av.«

Sara tömmer glaset i två klunkar.

»Vad gör ni egentligen?«

»Man kan säga att vi utreder vissa typer av försvinnanden, sådana som din systers. Vi gör annat också, för finansieringens skull, men utredningarna är vår kärnverksamhet. Probonum har existerat nästan lika länge som bortföranden.«

Bortföranden? Sara börjar skratta. Louise är alltså någon sorts ufo-galning? Och Sara är halvgalen som ens lyssnar. Okej, hon kan lite cirkuskonster, Louise kan också några och nu är det så jävla dags att gå innan Louise sätter på sig antennerna. Sara formar läpparna till det hon vill säga: Dra åt helvete. Orden kommer inte.

»Jag var där vid spärren, jag vet vad du gjorde. Du vet också«, säger Louise.

Sara försöker tänka tillbaka. Det mesta är suddigt, drömlikt. Kaoset inne i väktaren. Den plötsliga självklarheten när hon gjorde saker som borde vara omöjliga.

»Du hjälpte mig.«

»En smula. Det behövdes knappt. Inte ens våra bästa pejlare hade lyckats med det du gjorde utan träning. Med rätt hjälp kan du gå långt.«

»Hittar ni dem ni utreder?«

»En del får vi höra talas om när det är för sent, andra misslyckas vi med.«

»Som Gabriella.«

»Vi försökte, mer kan jag inte säga utan att bryta mot sekretessen. Våra sekretessregler är omfattande. Det har hänt att utredningar återupptas, Probonums mer seniora medarbetare kan begära ut gamla fallbeskrivningar när de behöver referensmaterial. Det finns en del intressanta detaljer i fallet.«

»Kan tänka mig det.«

»Probonum har en pressad situation och personalbristen är akut. Jag önskar att jag kunde säga att det var ofarligt att arbeta för oss, men jag tänker inte ljuga för dig. Gabriella riskerade sitt liv för att hjälpa andra.«

Sara drar sina händer ur Louises.

»Jag är inte Gabriella.«

»Det räcker med att du är du.«

»Jag måste gå.«

Louise lutar sig tillbaka.

»Du behöver hjälp, Sara. Pejlarförmågan går aldrig att kontrollera helt, förr eller senare kommer du att skada dig själv. Eller andra. Som i dag.«

»Väktaren, han…«

»I bästa fall kommer han att ha lindrig huvudvärk några dagar. Kanske lätta minnesförluster.«

Louise gör en kort paus, lutar sig fram och fångar Saras blick i sin, det är omöjligt att titta bort. Svårt att ens röra huvudet.

»Eller så går han upp tidigt en morgon och utan att veta varför sätter han sig i sin bil och låter den gå på tomgång i garaget tills…«

»Sluta.«

»Det behöver inte fortsätta så. Och det finns mer vi kan göra. Tillsammans.«

Försöker Louise värva henne till en sekt? Eller rädda Gabriella från en? <br> Klockans tickande hörs högre. Ekar. Nedanför på gatan hörs stojande barn och sirener på avstånd. Hon skulle kunna sitta på en parkbänk och röka långsamt, med duggregnet i ansiktet. När Sara reser sig känns benen stumma, som om de tillhörde någon annan.

»Jag måste verkligen gå nu.«

»Jag tänker inte hindra dig. Drick mycket vatten när du kommer hem, det hjälper mot huvudvärken. Kom tillbaka hit på måndag klockan nio.«

»Annars?«

»Det finns inget annars. Vi behöver dig, Sara. Och du behöver oss. Jag skulle gärna ge dig mer tid, men det kan jag verkligen inte. Arvodet är lågt, men tjänstebostad ingår. Vi kommer att ge dig en tremånaders snabbutbildning och placera dig i Fruängen så snart du genomgått de mest grundläggande momenten.«

»Jag hatar Fruängen. Jag har bott i närheten och tänker inte flytta tillbaka.«

Sara är medveten om att hon låter barnslig. Louise reser sig, lägger en grå mapp framför henne och fortsätter:

»Här är kontraktet. Läs igenom noga och signera på alla ställen där jag satt kryss. För övrigt är vi en rökfri organisation. Jag förväntar mig att du har slutat nästa gång vi ses.«

Bra pitch, vi kommer inte att ses fler gånger, tänker Sara. Mappen passar illa med sedlarna på bordet och Louises dräkt. Det finns en sorts enkelhet som är dyr, mappens är av den billiga sorten.

»Det går inte så bra för er, va?«, säger Sara, tar mappen och går innan Louise hinner svara.

Sara blir stående på Kungstensgatan med mappen under armen och Louises visitkort i handen. Krämfärgad papp, så tjock att den tycks porös mellan fingrarna. Probonum. Louise Eke, Head Training Supervisor. Längs kortsidorna löper meanderslingor i relief. <br> En jävla Louise Eke som vet för mycket och pressar fram svar. Som förvandlar Sara till ett mähä, i alla fall för ett tag.

Sara hittar cigarettpaketet längst ner i väskan. Första blosset får rök, kall luft och avgaser att riva i halsen. Efter det andra viker hon sig dubbel i en hostattack. Sen ännu en. Hon hulkar upp en grå, mjuk flaga i handen, större än en tumnagel. <br> Tyg? Mossa? Eller en bit lunga? <br> Louises enda vettiga idé var att Sara skulle sluta röka. Synd att Sara hatar vettiga idéer.

Hon dödar ciggen under kängan och öppnar mappen. <br> Formulär B31 om hälsa och personuppgifter. En fyrsidig ansökningsblankett till diplomutbildning för Junior Investigator. Juniorutredare. En extra ansökningsblankett om dispens för snabbstudier och en om vidareutbildning på distans. I en plastficka ligger en dålig kopia på ytterligare en blankett. F191, sekretessnivå 7. Den är signerad med Louises svepande namnteckning. Kvittens för arkivmaterial gällande fall 36.21 Gabriella Sundén. <br> Budskapet är kristallklart: om Sara samarbetar får hon information om sin syster. Hon knölar ner mappen i väskan, tänder en cigg och börjar gå mot Körsbärsvägen.

Luften i Heikkis lägenhet är stillastående och främmande. Som om Sara varit borta i flera veckor. Eller kommit tillbaka som en annan. Hon drar av sig skor och ytterkläder. Sjunker ner i den fläckiga soffan i vardagsrummet och gräver längst ner i väskan efter halstabletter. Fingrarna fastnar i det trasiga fodret och möter sval metall. Hennes berlock ligger i väskan. Berlocken hon fått av mamma.

Ett vagt minne dyker upp: de sömnlösa timmarna hos Aron. Det ovanligt starka månljuset. Ljuden. <br> Av någon bisarr anledning hade hon tagit av sig berlocken och stoppat den i väskan. Sara kan inte minnas att hon någonsin varit utan den så länge. Gabriella hade inte sin berlock på sig sista gången de sågs.

<figure> <img src="/bilder/43354227_10155546503301556_7636781241919864832_o.jpg" alt="fimp" width="494"> </figure>

KÄRA LÄSARE!  
Det här var första kapitlet i min roman Varken (LB Förlag). En mörk förortssaga som till största delen utspelar sig i Älvsjö. <br>Om du bor i krokarna kan du köpa den till specialpris av mig på Tulpanvägen 4, farligt nära Långsjön. Då kan du dessutom köpa [de tre parfymer ](https://www.facebook.com/lisaforareskriver/posts/372566773436522)som har betydelse för handlingen. Tjugo kronor för tre ljuvliga och eggande doftprover. <br> Jag är nästan alltid hemma. Som bonus får du se Älvsjös mest vanskötta trädgård, vilket kommer att göra underverk för ditt agrara självförtroende.

Annars föreslår jag att du gynnar en bokhandel nära dig. Eller köp Varken på nätet.

[ADLIBRIS](https://www.adlibris.com/se/bok/varken-9789177990864)

[BOKUS](https://www.bokus.com/bok/9789177990864/varken/)

Om du vill punktmarkera mig kan du följa [min författarsida på Facebook.](https://www.facebook.com/lisaforareskriver/?modal=admin_todo_tour)