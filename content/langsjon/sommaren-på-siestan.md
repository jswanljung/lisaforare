+++
avsandare = ""
bilder = []
date = "2019-07-21T00:00:00+02:00"
inlaggsbild = "/67528036_2355616274681729_5502145919866372096_n.jpg"
title = "Siestan"

+++
Med tanke på allt som hände sen var början så … banal.<br> Det var 1984. Jag sommarjobbade på Gröna Lund, som restaurangbiträde på Siestan. <br> Den som hängde på Djurgården på den tiden vet: det där lilla sunkhaket precis vid stora ingången som alltid luktade surt av rök och sötunket av intorkad öl.<!--more--><br>Siestan var ett ställe att dricka på. De som åt ångrade sig. De som drack ångrade sig i och för sig också, men inte förrän dagen efter.

Redan på hösten hade jag ströjobbat några kvällar innan Siestan stängdes för säsongen. Då fick jag mest duka av tunga ölsejdlar från den mörka inneserveringen och torka klistriga bord med en slankig trasa. <br> Första Grönanhelgen på våren plockade vi upp Delicato-bitarna som övervintrat i kylrummet sen förra säsongen – och eftersom jag hunnit fylla arton och fick jag äntligen börja servera öl. Det krävde viss skicklighet och inte så lite charm. <br> Knepet var att distrahera kunden medan man diskret sträckte sig ner bakom kassan. Där stod preppade sejdlar som fyllts till en fjärdedel med avslagen öl. All öl som borde ha kasserats när tankarna tömdes sparades nämligen i stora tillbringare. <br> Det var så det gick till på den tiden. Kaffet i automaten smakade inte ens i närheten av kaffe. Tryffelsåsen var svartprickig av konserverad fårticka.

Jag älskade mitt jobb, trots att det kunde beskrivas som en lång rad småbedrägerier. Eller just därför. Det tar det emot att erkänna det, jag tycker om att tänka på mig själv som en hederlig person. Men den här historien kräver kompromisslöshet i varje detalj. Vid minsta slirande på sanningen kommer jag inte kunna övertyga mig själv – än mindre dig – om att det som hände verkligen hände.

Därför måste jag också berätta det där om männen, hur de såg på mig och vad det gjorde med mig. Ett par år senare skulle jag komma att uppfatta samma blickar som förolämpande, då behövde jag dem. <br> Det var första sommaren jag var söt. Ingen kunde kalla mig nitlott längre, som killarna i högstadiet alltid gjorde när placeringen i klassrummet lottades fram. Äntligen var jag jättekexchokladen i chokladhjulet, inte plastskräpet i ståndet där det var vinst varje gång. <br> Jag hade aldrig lika djup urringning som Soffan och de andra tjejerna på Siestan. Men jag lutade mig fram en aning längre än jag behövde när jag räckte fram ölsejdlarna vid kassan. Precis så mycket att männen skulle kunna se mer, men inte så mycket att någon skulle kunna kalla mig slampig. <br> När man varit hungrig tillräckligt länge smakar vilken skit som helst fantastiskt. Och jag var svältfödd. Så jag tänkte bort mosiga ansikten, prillor, rödbrända håriga bringor under uppknäppta skjortor. Koncentrerade mig på deras blickar och lät dem bränna mot huden.

När den nya ljudteknikern kom för att presentera sig, i början av juni, fastnade han inte med blicken i min urringning. Han fixerade en punkt nära min kind. Söt men bortkommen, hann jag tänka. Så sträckte han fram sin hand, sa sitt namn: Kristian. Och såg mig äntligen och helt lugnt i ögonen. <br> Hans irisar var så ljust blå att de nästan var genomskinliga, men med mörka kransar. Slutade han också att andas? Saker jag avfärdat som klyschor blev plötsligt begripliga. Mer än så. Rimliga. <br> Blixten slog ner. <br> Världen stod stilla. <br> Och i den stillheten fanns bara han och jag och min svettiga ölklibbiga hand i hans svala. Mitt ansikte hettade.

Jag sa ju att det var banalt.

Samma kväll var det utgång efter stängning. Scenkillarna och tjejerna på Siestan festade ihop. Så hade det varit många år. Personal kom och gick, pakten bestod. Vi drog till Hasselbacken – eller Trasselbaken, som stället kallades av gaykillarna som jobbade i servisen på Hacienda. <br> Alla pratade musik, som alltid. Soffan kunde lika mycket, möjligen mer än killarna. Själv försökte jag fejka mig genom långa samtal om Phil Collins och Rory Gallagher. Trots att Kristian pratade minst var det han som visste mest. Han iakttog mig hela tiden. Det syntes på hans minspel att han inte köpte min bullshit för en sekund.

Teo som var från Polen hade obegränsad tillgång till hembränd slivovits. Den påstods vara gjord av plommon men smakade räv. Vi gick ut i omgångar och drack i plastglas utanför Hasselbacken. Drack man mer än ett glas slibb var man lika svår att få omkull som ett paket fil. Kristian drack tre. Och höll sig irriterande och underbart nära mig hela tiden. <br> Visserligen var jag söt, men han spelade i en annan liga, den där alla killar hade button-downkrage och alla tjejer handväskor som kostade som en charterresa. Där ingen någonsin ville vinna kexchoklad.

Som vanligt avslutade vi på partybåten Patricia på Söder Mälarstrand. Och där tidigt på morgonen, när vi stod tysta vid relingen, lade Kristan armen om mig. Jag lutade mig närmare medan hjärtat snubblade runt i bröstkorgen. <br> Han viskade att jag verkligen inte visste något om musik. Jag erkände att jag var tondöv. Då skrattade han lågt och drog mig tätt intill. <br> Först när vi kyssts länge lade jag märke till det där med hans händer. Men det var inget som störde mig.

Det sägs att de som står stadigt med fötterna på jorden är de som faller hårdast när de väl tappar balansen. Och det gjorde jag. Trots att jag haft ett par pojkvänner hade det aldrig hänt mig tidigare. <br> Förnuftiga Lisa. På väg att bli mikrobiolog. Alltid med en bitsk replik.

Varje gång Kristian rörde vid mig drabbades jag av den svindel som inte är rädsla för att falla – utan längtan efter att göra det. Oavsett konsekvenser. <br> Vi hånglade i pariserhjulet när det regnade och var folktomt, i omklädningsrummet, till och med utanför Siestans soprum. <br> Efter två veckor låg vi äntligen med varandra i stjärnlogen bakom Stora scen. Vi halvslumrade nakna med en svensk flagga som täcke. Även om vi hade försökt sova hade det nog inte gått eftersom trollkarlen Ture Trollmans duvor förde ett jävla oväsen hela natten. <br> På förmiddagen blev vi utkörda av Sven Bertil Taube som skulle uppträda senare på dagen. Vi spekulerade i om han lika bakfull som vi, han hade i alla fall mörka solglasögon och ett plågat drag runt munnen.

Efter det följde Kristian med mig hem nästan varje natt – till mina föräldrars sommartomma radhus i Alvik. Vi sov tätt, tätt tillsammans. Något annat var inte möjligt i nittiosängen i mitt flickrum.

Jag är rätt säker på att Kristian aldrig legat med någon innan mig, men det var inget vi pratade om. Det var obegripligt att han var oskuld – han var ju tjugoett och kunde få vem han ville. <br> Kanske berodde det på det där med händerna. <br> Han försökte alltid gömma dem. I fickorna eller bakom ryggen. De tre yttersta fingrarna på varje hand var sammanvuxna till andra leden. Inte tätt ihop, men förenade av tunt skinn. Lite som simhud. Den borde ha gått att avlägsna med en enkel operation, men det var aldrig riktigt läge att fråga. <br> En morgon när vi varit tillsammans ungefär två veckor bad jag honom sära på ena handens fingrar. Jag lät mina fingertoppar löpa över den tunna, bleka huden med nätverket av blodådror. Han låg på rygg med andra armen över ögonen, andades snabbt och ytligt.

Vad brukade vi prata om när vi var ensamma? Ingenting speciellt. Orden var ändå oviktiga, det var hans röst jag lyssnade på. Låg, mörk och mjuk med en antydan av dialekt omöjlig att placera. Någonstans norrifrån, tänkte jag då. <br> Nu vet jag inte vad jag ska tänka.

Det tog ett tag innan jag reflekterade över att han aldrig pratade om sin familj. Inte ett ord. Vid varje fråga gled han undan. Han bodde fortfarande hemma, i en stor rosa villa vid Långsjön i Älvsjö, det hade Teo som kört Kristian hem en kväll berättat. <br> På en av de många festerna frågade jag Manne, veteranen bland ljudteknikerna, om Kristian hade problem hemma. Med familjen, eller så. Manne ryckte på axlarna och sa att han hört att det fanns en syster som ”inte var riktigt som hon skulle”. Mer fick jag inte ur honom ens när jag mutade honom slattöl. <br> Några av scenarbetarna kallade Kristian överklassunge, vilket tydde på att de visste något jag inte visste. Alla andra hade skitiga Converse. Kristian hade kritvita Adidas-skor.

Har jag nämnt att Kristian var vacker? Alltså inte snygg, odiskutabelt vacker. Soffan sa att han såg ut exakt som David Bowie. Hon visste att jag dyrkade Bowie, inte för musiken utan för texterna där varje fras var en gåta att lösa. <br> Gaykillarna som jobbade i servisen på Haciendan retades konstant med mig, sa att Kristian var för tjusig för att ha mig till flickvän. För tjusig för att vara straight också. För fin i kanten.

Soffan brukade ligga med Manne. De hade legat med varandra förra säsongen också och ibland när de gick hem höll de varandra i handen, så de betraktades som praktiskt taget gifta. Men det var Kristian som Soffan inte kunde slita ögonen från när vi var ute och festade, de dansade rätt ofta eftersom jag var usel på att dansa och alltid skyllde på ont i fötterna. <br> Soffan hade nog gärna legat med Kristian, men tjejerna på Siestan hade en hederskodex och Kristian var paxad av mig.

Fast Manne var den som stirrade oftast och mest intensivt på Kristian. Inte så att han såg kåt ut, det var något annat som var svårt att definiera. Något samtidigt beskyddande och avvaktande. Ömsint. Fast jag skvallrade och drygade mig lika mycket som alla andra, nämnde jag inget om det där med Mannes stirrande. Varken till honom eller någon annan. <br> För Manne var ingen man bitchade med. Han var, jag vet inte hur jag ska beskriva det … lite speciell. Trots att han ärligt talat var småtrög _såg_ Manne saker. Han hittade försvunna nycklar, kunde gissa rätt på vilket stjärntecken folk var födda i och kände till varenda svaghet hos andra. <br> När någon av gästerna på Siestan blev mer än salongskanon och tafsig eller hotfull hämtade vi inte ordningsvakterna utan Manne. Han brukade lägga handen på bråkmakarens axel och viska ett tag med honom. Efter ett par minuters lugnt samtal lufsade fyllot iväg. Varenda gång.

Kanske var det på grund av sin egenhet Manne hade sådant tjejtycke. För egentligen var han inte särskilt snygg: kraftig och rödbrusig, med tjock ljus kalufs, ett myller av korkskruvlockar som passat bättre på en kerub. Men han hade en förmåga att säga precis rätt saker vid rätt tillfälle. De nätter Soffan inte var med gick han i regel hem med någon annan. Enda gången Soffan brydde sig var när han var på någon av de andra tjejerna på Siestan.

En natt då i början tog Manne mig åt sidan. Trots att klockan inte ens var tolv, var hans ögon var redan simmiga av slibb. Det fanns en oro över honom.

”Kristian hade med sig en kassett idag. Med _sin_ musik. Det var jävligt …” <br> Mannes ord tog slut, han liksom tuggade i luften. <br> ”Var det bra?” frågade jag. <br> Manne blev tyst. Sen: <br> ”Jag vet inte.” <br> Han rynkade ögonbrynen, trevade efter minnet. <br> ”Jag har aldrig hört något liknande. Har han inte spelat för dig?” <br> Jag kände mig dum när jag svarade nej, jag visste inte ens att Kristian spelade. <br> Manne såg egendomligt lättad ut och gav mig en halv kram, sluddrade att jag var en jävligt fin tjej och att jag skulle vara rädd om mig. Jag trodde att han menade att jag inte skulle bli med barn.

Eftersom jag skulle flytta till Uppsala och plugga till hösten behövde jag pengar. Därför jobbade jag nästan varje dag, ofta långpanna. Först dit, sist hem. Kristian gick på timmar med oregelbundet schema. <br> Att han var borta ett par dygn då och då utan att höra av sig var förstås mer plågsamt än jag ville erkänna – men inte konstigt. Det fanns inget internet, och det var innan mobiltelefonernas tid. Mina föräldrar hade stängt av den fasta telefonen under sommaren. Ja, man gjorde faktiskt så då för att spara pengar. Jag hade inte ens Kristians telefonnummer och var för stolt för att be om det.

”Hjälpt till med lite saker där hemma”, var Kristians enda förklaring när han dök upp igen. När jag försökte fråga mer kysste han mig tyst. Och sen lite mer, tills allt var oviktigt utom att han rörde vid mig. <br> Det fanns en speciell lukt han hade med sig när han varit borta: söta och tunga blommor och något bränt kryddigt, nästan som glögg. Eftersom jag inte kunde erkänna ens för mig själv att jag var svartsjuk frågade jag inte om den. Efter ett par dagar luktade han som min igen.

Jag undvek att prata framtiden, eftersom jag inte ville höra att vi inte hade någon. Kristian skulle fortsätta på familjeföretaget efter sommaren, och han antydde att han troligen inte skulle stanna i Sverige. <br> Inte ens Manne hade någon aning om vad familjeföretaget sysslade med, men han visste att familjen ogillade att Kristian jobbade på Grönan. Kristian var inte där för lönens skull, utan för musiken. Soffan sa att familjen badade i pengar, jag låtsades som om jag inte brydde mig. Det gjorde jag förstås, eftersom det innebar att Kristian var _både_ för vacker och för rik för mig.

Dagar gick. Nätter festades bort. Sommaren var ovanligt sval och regnig, vilket bidrog till en uppgiven känsla av senhöst. Jag motade bort den så gott till gick, plockade disk tills armarna domnade. Hällde upp öl som en gudinna, log mot kunderna tills jag fick träningsvärk i kinderna och drog in galna mängder dricks.

I början av augusti försvann Kristian tre dagar i rad. Sen ännu en, det hade inte hänt förut. På kvällen stod jag inte ut längre. Det vara bara en dryg vecka kvar tills jag skulle flytta till Uppsala och något bara måste hända innan dess. <br> Jag bet ihop om skammen och bad Soffan Kristians nummer av Manne. Det var bråttom, dagen därpå skulle Manne åka till Korfu en vecka. Och jag klarade inte tanken på att någon annan än Manne skulle ens misstänka hur desperat jag var. <br> En timme senare stack Soffan en lapp i min hand. Ett telefonnummer. <br> Jag ringde och ringde under kvällen men ingen svarade utom telefonsvararen: en sval kvinnoröst som meddelade att ingen var hemma och att det gick bra att lämna ett meddelande. Efter att ha ringt många gånger och lagt på talade jag till slut in en hälsning. Försökte låta glad och nonchalant och undrade var Kristian höll hus.

Dagen efter dök Kristian upp på Siestan för att äta lunch med Manne. Kristian hälsade knappt på mig. Han åt sin panerade spätta hopkrupen över tallriken medan han såg sig omkring med hastiga blickar. <br> När han smitit iväg utan ett ord kom Soffan fram och slog armarna om mig i en hård evighetslång kram. Det enda hon sa var: ”Gumman, gumman, lilla gumman.”

Jag körde långpanna som vanligt, blev kvar sent och stängde den kvällen. Halvvägs till busshållplatsen hördes rösterna. Kristians lugna och en kvinnas upprörda. <br>  De stod på trottoaren inte långt från busshållplatsen. Jag tog på mig ett oberört leende och gick fram mot dem. <br> Kvinnan hade en röd blank klänning, långt silverblont hår som lockade sig över ryggen. En blick på henne räckte för att förinta det självförtroende jag byggt upp under sommaren. Jag var söt, hon var ögonbedövande vacker. Ögonen stora och mörka. Läpparna klarröda. Ansiktet så blekt att det var självlysande i dunklet. Hon kunde inte ha varit ute en enda dag i hela sitt liv. <br> Parfymen! <br> Den blomsöta och kryddiga … En tung sten växte i min mage. Sjönk och sjönk.

”Min syster Vera. Jag ska ta hem henne nu”, var allt Kristian sa. <br> Hans ögon var vatten. <br> Vera log. Trots att det gjorde henne ännu vackrare var det inget behagligt leende. Hon gled närmare Kristian. Lade armen om hans rygg, handen på hans höft. Nästan på låret. _I helvete att hon var hans syster!_ <br> ”Är det du som är Lisa?” <br> Vera sträckte fram handen, den var oväntat sval och fuktig i min. <br> Nej, inte fuktig. Våt. <br> Det var som att stoppa ner armen upp till armbågen i ismaskinen på Siestan. Jag såg ner på hennes hand, de tre yttersta fingrarna var sammanväxta till andra leden. Som Kristians. <br> Jag var tyst, för om jag försökt säga något hade jag börjat gråta. Och då hade hon vunnit.

Det var i det ögonblicket Manne dök upp på sin cykel. Först tittade han inte ens åt Vera, han brukade bete sig så mot snygga tjejer. För att få dem ur balans, antagligen. Han började säga något till Kristian. Något om kablar som kommit till rätta. Och om att han skulle göra ett DJ-gig på Korfu. <br> Så vände han sig mot Vera. Och stelnade till. Backade hastigt med så dumt gapande mun att jag började skratta. Men jag slutade tvärt när Manne snubblade och tappade cykeln. Hans ansikte var blekt och han skakade.

En taxi gled fram och Kristian knuffade in Vera i den. Jag hade aldrig sett honom hårdhänt mot någon förut. Manne slet upp cykeln, kastade sig upp på den och gav sig iväg. Fast han för en gångs skull var nykter vinglade han. <br> Jag drog på partybåten Patricia och drack upp nästan en dagslön.

Manne ringde till Siestan nästa förmiddag och frågade efter mig. Han stod i en telefonautomat på Arlanda, det lät som om han var full redan.

”Om jag var du skulle jag sticka. Ta semester eller vad fan som helst. Hon var här på grund av dig. Det fattar du, va?”

Jag var bakis med en gnagande huvudvärk. Fräste åt honom att lägga av. För första gången sa jag det rakt ut: att han var svartsjuk och ville ha Kristian för sig själv. Och att det var fruktansvärt patetiskt. <br> Manne slängde på luren.

Kristian kom inte till jobbet den dagen. Inte dagen efter heller. Tredje dagen skulle Tomas Ledin spela och ljudgänget satt i skiten. Chefen kom till Siestan och frågade Soffan och mig om vi visste något. Tydligen svarade ingen i Kristians familj i telefon. <br> Jag spelade oberörd, jag var bra på det redan då. Bara Soffan märkte att jag var i fritt fall. När vi var ute och festade natten efter, och några av de andra började snacka skit om svikaren som dragit som en avlöning släpade hon med mig därifrån.

Den femte natten när jag gick av mitt pass beställde jag taxi. Fast det var nattaxa och kostade skjortan och sjuttiofem, som Soffan brukade säga. Samma sekund jag klev in i taxin kom Soffan rusande, slängde upp dörren och kastade sig in bredvid mig. <br>”Du ska inte åka ensam dit”, sa hon bara. <br>Hon hade förstått vart jag skulle. _Åh, Soffan!_

Långsjön låg i ett lummigt villaområde, inte långt från Långbroparken. Jag hade för länge sedan pumpat Teo om var huset låg, och även om han inte hade en exakt adress lyckades taxichauffören leta sig fram till det rosa huset. Kristians hus.

Villan var överraskande modern och kantig, som en gigantisk blekrosa tårtkartong. Alla gardiner var fördragna. I ett fönster på nedervåningen sipprade ljus ut genom persiennspjälor. Soffan tog min hand och tryckte den. <br> Ingen stig eller gång ledde till huset. Vi gick genom den vildvuxna trädgården. I träden fanns små vita blommor som lyste i det täta augustimörkret. Marken var täckt av vissnande blomblad som gav ifrån sig en kvalmig dunst. Ju närmare jag kom, desto starkare blev lukten av brända kryddor. Ändå tycktes luften kallare. Rå och fuktig.

Då hördes musiken.

Kanske hade den funnits där hela tiden. Jag är inte ens säker på att det var musik: ljud som steg och sjönk. Ena stunden var den mer som sus i träden, fast det var vindstilla. Sen var den mer som rassel i mycket torra löv sent om hösten. Kanske vågor. Så tog något slags blåsinstrument eller stråkar över. Jag är verkligen fel person att förklara. <br> En gäll flöjtton. <br> Soffan ryckte till. Klämde min hand hårt. Och gav ifrån sig ett gällt läte, lite som en skrämd fågel.

Dörren ovanför den lilla stentrappan var av mörkt, blankt trä. Soffan tvärstannade på nedersta trappsteget. Jag gick upp och ringde på. Flera gånger. Steg hördes inifrån. De stannade innanför dörren.

”Kristian?” ropade jag tyst. Ingen svarade. Ett lågt kvinnoskratt hördes från andra sidan.

”Vera?” Min röst var tunn.

Stegen avlägsnade sig igen. Dansande nu, i takt till musiken. Sen kom ljuden av vatten. Forsande rinnande vatten, nästan ett vattenfall.

Vi satte oss längst ner på stentrappan. Rökte och väntade. På vad vet jag inte. Jag hade en halv flaska slibb i väskan som vi halsade av för att hålla oss varma. Jag grät inte. Inte då. Darrade bara. <br> Soffan lade armen om mig, ändå var hon inte riktigt där. Hon stirrade rakt ut i den mörka trädgården. Spökvisslade entonigt, som om hon letade efter en melodi. Efter ett tag somnade jag med huvudet i Soffans knä medan hon sakta strök mitt hår.

I drömmen var trädgården full av ljud. Viskande, sjungande, stråkar och spröda klanger. Och på gräset dansade Soffan, dansade som man bara kan i drömmar. Flytande och lätt.

Efter vad som måste varit flera timmar, när det ljusnade och fåglarna börjat sjunga, vaknade jag av regn som föll i mitt ansikte. Stora tunga droppar. Jag låg hopkrupen på gräset med stentrappans nedersta steg som kudde, fint grus skavde min kind. Jag vände ansiktet mot himlen och fångade en droppe på tungan, den hade en egendomligt söt och oljig smak. Så vände jag på huvudet.

_Soffan!_ <br> Hon var inte där.

Jag ropade försiktigt. Reste mig vacklande och gick runt huset. Gräset var högre där, obehagligt halkigt mot mina bara ben, nästan sjögrässlemmigt. Men ingen Soffan. Regnet varade bara några minuter.

_Jävla Soffan!_ Hon måste ha fått nog och dragit. Bara dumpat mig ute i förorten, trots att hon visste att jag var sorten som kunde gå vilse i en skokartong. <br> Efter att ha ringt på dörren länge och argt en sista gång började jag gå mot Fruängen. I alla fall trodde jag det.

Eftersom jag inte har något lokalsinne gick jag givetvis åt fel håll. Jag var så van att gå vilse att jag inte brydde mig, så småningom skulle jag stöta på någon att fråga. Efter att ha irrat runt ett tag var jag tillbaka vid huset, den här gången såg jag Långsjön blänka mellan träden. Jag följde en stig som ledde genom en dunge med höga träd. Mot en liten gångbro.

Mitt på bron stod ett par kritvita Adidas-skor. En röd Adidas-overall, en sådan där med vita revärer, låg prydligt hopvikt bredvid. <br> Jag gick fram. Lutade mig mot räcket, som jag en gång lutat mig mot en reling. Såg ut över sjön som var blanksvart och stilla. Ett tunt dis svävade över ytan. Och musiken som inte var musik hördes igen.

Något silvrigt rörde sig i djupet, ett tiotal meter bort. Närmade sig. <br>Ytan krusades. <br>Inte fiskar. Något stort. Kompakt.

Vattenytan bröts.

Jag sprang. Utan att veta varför eller vart sprang jag på värkande fötter. De plaskvåta sandalerna skavde. <br> Våta? Hur hade de blivit så våta? <br> Jag slet av dem i farten. Fortsatte springa barfota på daggfuktiga gator med sandalerna i handen. Snart fick jag håll och gick i stället. Gick och gick i blindo. Jag tror att jag grät då. <br> Ibland hörde jag musiken. Vissa stunder helt nära, andra avlägset. Flera gånger tycktes den rosa villan skymta mellan träden, hela tiden på nya ställen. <br> Hallucinationer? _Aldrig mer slibb!_

Till slut mötte jag en äldre kvinna med krusigt hår, en fet katt i koppel och lugn röst. Hon visade mig vägen mot Fruängen, följde med flera kvarter för att vara säker på att jag kom rätt. När vi skildes åt gav hon mig en kram och stod kvar och såg efter mig. När jag vände mig om för att vinka såg jag att det jag trott var en katt snarare liknade en stor hare.

Jag åkte direkt till Grönan. Halvsov några timmar på en parkbänk innan jag började jobba. <br> Restaurangchefen var skitsur. Tydligen hade hon fått ett meddelande på svararen under natten: Soffan hade sagt upp sig och rest till sin pappa i Frankrike på morgonen.

Tre dagar senare var Manne tillbaka från Korfu. Rödbrusigare än någonsin, håret solblekt och risigt av saltvatten. Jag kastade mig i hans famn, lipade och snorade och berättade att jag varit hemma hos Kristians familj, men att ingen släppt in mig. Jag berättade om skrattet jag hört, om lukten och ljuden av vatten. Om sjön. Och Soffan som bara dragit. <br> Mannes fingrar borrade sig in i mina överarmar när han fick mig att lova att aldrig gå tillbaka.

Den sista veckan på Siestan sniglade sig tiden fram. Minut för minut, timme för timme, dag för dag. Kristian kom inte tillbaka. Soffan var kvar i Frankrike, hon ringde mig inte ens. Manne undvek mig. <br> Jag gick direkt hem efter jobbet, såg på videofilmer till sent på natten för att slippa tänka. Sov oroligt. Drömde om nattgrått stilla vatten och Vera som skrattade och skrattade i sin röda klänning.

<figure> <img src="/bilder/67130627_2302136156540158_9119104898221211648_n.jpg  
" alt="REGN" width="494"> <figcaption>Foto: Fredric Dahlgren </figcaption> </figure>

Jag flyttade till Uppsala den hösten, mitt liv fylldes med annat. Soffans svek gnagde fortfarande, men allt mindre. Jag drack lite för mycket, pluggade och sov alldeles för lite. <br> Jag tänkte allt mer sällan på Kristian. Ibland på fester inbillade jag mig att jag hörde hans röst i sorlet: mörk, mjuk och med den lilla melodin. Det hände att jag anade en parfym som påminde om den där jävla lukten.

Det tog ett år innan jag såg Kristian igen. Eller såg och såg …

I juni 1985 hörde Teo oväntat av sig och bjöd på fest i Stockholm. Han var husvakt över sommaren, en villa rätt nära Fruängen, inte långt från Långsjön. <br> Varningsklockor borde ha ringt, men jag var rastlös och rätt låg. Jag hade sommarjobb som skoförsäljare på Don & Donna på Nybrogatan, under våren i Uppsala hade jag förälskat mig i en juristkille som åkt hem till Göteborg över sommaren. Jag hade skickat två långa brev. Utan att få ens ett vykort tillbaka.

Teos fest var halvlyckad, full av främlingar. Teo hade också blivit en. Efter att han kramat om mig under glädjetjut hade vi nästan ingenting att prata om. Han träffade inte Manne längre, Soffan hade han inte sett röken av sen 1984 och ingen av oss nämnde Kristian. <br> Slibben flödade och tröstade. Jag hamnade i trädgården, hånglade halvhjärtat med en elektriker i en hammock, tills hans pojkvän dök upp och drog iväg honom. <br> När jag vaknade i hammocken – törstig och kissnödig – var trädgården sänkt i mörker.

Månskenet var egendomligt tjockt, liksom rann som trög silvrig vätska över den rosa fasaden på huset bredvid och spillde ner på marken, flöt ut och … <br> Fasaden! Huset! <br> Jag har ett ökänt uselt lokalsinne, men en sak visste jag säkert. Det huset, Kristians hus, borde inte ligga här. Inte ens i närheten. När festen började hade det stått ett lågt vitt tegelhus på samma plats.

Så var lukten där: kvalmig, vidbränd. Och svag musik svävade ut från ett halvöppet fönster. <br> Först Bowies Candidate. Min favoritlåt. Den blandades med den andra musiken, den som nog inte var musik. <br> Runt omkring mig var gräset vitt, vitt, vitt. Först trodde jag att det hade snöat, sen kom en vindil och svepte undan det vita. Blomblad. En fläkt av tung parfym fyllde luften. Kväljande och välbekant. Och ljudet av vatten. Droppande först, sen porlande. Brusande. Dörren öppnades skugga lösgjorde sig inifrån huset. Lång och smal. <br> ”Kristian?” ropade jag. <br> Han vinkade sakta mot mig med en – solfjäder? Nej, handen! Hans ansikte låg i helt skugga, ändå var det så självklart att han log. Han slog ut armen, pekade på dörren bakom honom. <br> Den var annorlunda nu. Den var dörren till stjärnlogen bakom Stora scen på Grönan. Kristian öppnade den på vid gavel. Ut trängde ljuden av Ture Trollmans jävla duvor. <br> Jag började skratta och reste mig vingligt. Bara ett tiotal steg så skulle jag vara i Kristians armar.

En hand slet tag i min axel. Jag försökte skaka mig loss. Teo. Han undrade vad fan jag höll på med. <br> ”Kristian är här.”<br> Jag vände mig om. Där huset stått fanns bara mörker. <br> ”Du borde inte tagit svamp, gå och lägg dig inomhus”, var allt Teo sa.

Livet fortsatte. Saker hände som var bättre och värre än det som hänt sommaren 1984, tiden skrev in nya minnen över de gamla.

När jag stötte på Manne på Kvarnen i slutet av nittiotalet kände jag inte igen honom först. Allt som var kvar av änglahåret var några tunna hårtestar klistrade mot flinten. Ögonen var svullna och rödsprängda. Själv hade jag fått igång någon slags karriär som matskribent och hälsade rätt drygt. Men sen skämdes jag och satte mig ner och pratade. Bjöd på en stor stark, skojade om att jag var skyldig honom en riktig öl efter all fulöl jag lurat på honom den där sommaren på Siestan. <br> Manne var fåordig, berättade att han precis börjat arbeta för en stiftelse. Något slags utredningsjobb. En liten muskel ryckte vid hans öga. Ju mindre han pratade, desto mer pladdrade jag. Efter ett tag gjorde jag rösten lätt och frågade om han stött på Soffan senaste åren. <br> Manne stirrade på mig, undrade om ingen berättat. <br> Jag var tyst. <br> Han fortsatte. <br> ”Soffan hittades drunknad i september 1984. Resterna av henne i alla fall.” <br>”I Frankrike.” <br> ”Nej, i Stockholm.”<br>”Du skämtar”, sa jag.

Manne fiskade fram plånboken och grävde fram en liten sliten papperslapp. En dödsannons: Ann-Sofie Hellberg. Född 27/11 1962. Död 1984. Inget datum. <br> Luften försvann ur rummet. <br> Hjärtat. <br> Allt jag kunde tänka på var Soffans röst som sa ”gumman, gumman”, hennes hand som strök mitt hår. Jag tog två snabba klunkar vin och frågade vad som hänt Kristian. Manne snäste nästan: <br> ”Du vill inte veta.”<br> ”Träffade du honom efter … efter den där sommaren”, frågade jag. <br>”På sätt och vis.” <br> Manne log snett. Mer som en grimas. Hans läppar skälvde när han svepte det sista av sin öl.

Jag sa att jag måste gå, att min fästman väntade på mig på Stureplan. Sa att det var kul att ses, att vi borde träffas igen – för det är sådant man säger och ingen förväntar sig att man ska mena det. <br> Jag var halvvägs genom dörren när han ropade efter mig: <br> ”Du hade en jävla tur, Lisa. Sån jävla tur.” <br> Ute blåste snöblandat regn i mitt ansikte, men det var inte därför jag darrade.

För nio år sedan, när jag och min man började leta hus i förorten, var Kristian inte mycket mer än ett vagt minne. En sommarfling bland andra. Jag hade googlat honom någon gång på fyllan. Minnet av Soffan hade en besk smak av skam, jag sköt alltid bort det.

I oktober snubblade vi över ett hus i Herrängen som inte var galet dyrt. Efter visningen promenerade vi runt i området för att känna in det. Tillät oss drömma. Visst skulle vi trivas här? Husen i området var lustigt omaka. Stora trävillor, sextiotalshus i rött tegel, nybyggda rymliga kuber i vitt trä. <br> ”Inget rosa hus, inget rosa hus”, mumlade jag. <br> ”Vad säger du?” frågade min man. <br>”Jag vet inte.” Och det var helt sant, jag visste inte. Jag hade glömt.

Till sist gick vi en sväng ner till sjön. Stannade på den lilla gångbron. Jag mindes den inte då, tänkte bara att den kändes hemtam. Vattnet skimrade framför oss. Kallt och grått. Med ens ville jag därifrån, snabbt. Fragment av festnatten kom tillbaka: mörkret i trädgården, en rosa husfasad. Månens smälta silver. <br> Sen slog jag bort tankarna. Insåg att det var löjligt att låta husköp påverkas av att jag hade haft en snedtändning i närheten mer än tjugo år tidigare. Chansen var ändå mikroskopisk att vi skulle få vårt drömhus.

Det blev knappt budgivning på huset. Kanske var det tur, kanske något annat, men vi fick det till ett förbluffande lågt pris. I december flyttade vi in på Tulpanvägen med min mans söner. En familj på fyra, som blev fem till sommaren.

Jag undvek Långsjön under våren. När vår son kom i juli var det rekordvarmt. Snart började jag gå med barnvagnen till den lilla strandpromenaden för att få svalka. Alltid dåsig av sömnbrist, de första gångerna med något tungt molande i magen. Sjön var harmlös och sprakade av solglitter. Det värsta som hände var att jag brände mig på nässlor och blev biten av en miljon knott.

När man just fått ett barn, och dessutom fått det sent i livet efter mycket sorger, finns det inte plats för så mycket annan oro. <br> Länge var allt som det skulle och lite till, mina bekymmer var av samma slag som andra småbarnsmammors.Till den där sommaren för några år sedan. <br> På midsommardagens morgon vaknade jag av musik. Dunkande musik av något slag. Några av grannarna som festade och inte haft vett att sluta i anständig tid. Efter ett tag började de spela Bowie. Klar förbättring.

Jag gick upp för att dricka vatten. Spolade länge i kranen för att få vattnet kallt, ljudet blandades med musiken, förvrängde den till något annat. Jag tittade ut på den ljusnande himlen genom köksfönstret. <br> Grannhuset stod för nära. Och hade fel färg. <br> Blekt rosa. <br> Vita blomblad täckte gräset, trots att vårt körsbärsträd blommat över för länge sen. En gestalt lösgjorde sig ur skuggorna. En smal ung man med månblekt hår.

Kristian.

Jag knep ihop ögonen. Hårt, hårt. Räknade till tio. <br> Och öppnade ögonen. <br> Gräsmattan var tom. Grannhuset stod på sin vanliga plats och såg ut som det brukade. Var det bara soluppgångens glöd som hade reflekterats på den vita fasaden? <br> Det var då jag kände lukten. <br>  Och började minnas igen.

\*******

I augusti utkommer min bok Varken som utspelar sig runt Långsjön.  
Du kan[ förbeställa den här](https://www.adlibris.com/se/bok/varken-9789177990864) på Adlibris.  
[Du kan förbeställa den här](https://www.bokus.com/bok/9789177990864/varken/) på Bokus.  
Pilska nornor, schlagerälskande maror och små smutsiga älvor. Köper du min bok har jag råd att skriva en uppföljare. Det ska jag väl?

Berättelsen ovan är min egen och jag tänker fortsätta på den här på hemsidan, i min "roman" är det däremot helt andra individer som fått ordet. Personer, om man nu kan kalla det personer, som vistas runt Långsjön. Främst Sara som jag stötte på under egendomliga omständigheter.

Varför citationstecken runt ordet roman? Tja …Till min familj och mina vänner har jag sagt att jag skrivit en roman. <br> Det är det inte. <br> Snarare ett pussel av minnesskärvor. Egna och andras. Plockade från snirkliga förortsgator och framvaskade vid Långsjöns stränder. Visst har jag skarvat en hel del och fyllt i gapande luckor, men någon författare är jag inte. <br> Min förläggare, Louise, tycker att jag har fantastisk fantasi. Jag önskar att hon hade rätt.

\**GILLADE DU HISTORIEN? Dela den till alla du gillar. INTE? Dela den till alla du ogillar, så slösar de sin dyrbara tid på goja.

HÄR FINNS [ALLA BERÄTTELSER FRÅN LÅNGSJÖN](https://lisaforare.se/langsjon/ "KLABBET!").

VILL DU LÄSA FLER BERÄTTELSER HÄR? SPONSRA MITT SKRIVSNUS – OCH ETT LEGOBYGGE TILL MIN SON SÅ ATT JAG FÅR LUGN OCH RO.  
0709-733 331**